package controller;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

import model.db.Spiel;
import model.db.Zug;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;

/**
 * 
 * ControllerGameFieldHistory dient der Anzeige von bereits gespielten Spielen.
 * 
 * Der User kann in einem Dropdown-Menü ein Spiel auswählen. Zu diesem Spiel
 * wird in einer Übersicht die beiden Spieler, das Endergebnis, der Modus, das
 * Schwierigkeitslevel, eine Tabelle mit allen Daten zu den Sätzen(Satznummer,
 * Satzgewinner, Startspieler & Zwischenergebnis), die Gesamtanzahl der
 * gespielten Züge und den Gewinner des Spiels angezeigt. Zu dem kann der User
 * den Gewinner im Nachhinein noch ändern. Im Weiteren kann der User auf einen
 * Simulationsscreen gelangen, auf dem er eine Simulation des Spiels starten
 * kann. Er kann zu dem durch einen Vorwärts- und Rückwärtsbutton die Züge
 * einzeln durchklicken. Der User kann ebenso in einem Dropbdown Menü einen
 * Spielzug auswählen, der direkt angezeigt werden soll.
 * 
 * @author MTGames
 * @Mitglieder Timo Strauch, Tim Holm, Meike Helwig, Marcel Schäfer
 * 
 */

public class ControllerGameFieldHistory implements Initializable {

	/**
	 * allGames enthält alle bereits gespielten Spiele. Es wird mit Daten aus
	 * der Datenbank gefüllt.
	 */
	private ArrayList<Spiel> allGames;

	/**
	 * game dient als Spiel-Instanz, in der ein bereits gespieltes Spiel
	 * abgelegt wird.
	 */
	private Spiel game;

	/**
	 * Die Variable wird bei dem Farbwechsel benötigt. Sie entscheidet welcher
	 * Spieler welche Farbe hat.
	 */
	private int selectedStartplayer = 0;

	/**
	 * Counter dient als HilfsVariable für den Farbwechsel bei dem Setzen von
	 * Spielsteinen.
	 */
	private int counter = 0;

	/**
	 * Die Variable hilft bei der Bedienung des Dropdownmenüs. Ist sie auf
	 * False, wird der Spielzug ausgelesen. Ist sie auf true, wurde ein neues
	 * Spiel gestartet und der WErt des Dropdown-Menüs soll nicht ausgelesen
	 * werden.
	 */
	boolean newGame = false;

	/**
	 * manageRows managed das Spielfeld. Es gibt 7 Spalten und jede Spalte kann
	 * 6 Spielsteine aufnehmen. Sobald ein Spielstein in einer Spalte gesetzt
	 * wird, wird der dazugehörige Wert um eins verringert.
	 */
	private int manageRows[] = { 5, 5, 5, 5, 5, 5, 5 };

	/**
	 * Innerhalb von threadUI wird ein JavaFX Thread ausgeführt, der die
	 * Interaktion mit dem UI ermöglicht. Der JavaFX Thread wird bei der
	 * Spiel-Simulation eines Spiels benötigt.
	 */
	Thread threadUI = null;

	/**
	 * Variable zum Managen aller Panes in Spalte 1.
	 */
	private Pane[] col1;

	/**
	 * Variable zum Managen aller Panes in Spalte 2.
	 */
	private Pane[] col2;

	/**
	 * Variable zum Managen aller Panes in Spalte 3.
	 */
	private Pane[] col3;

	/**
	 * Variable zum Managen aller Panes in Spalte 4.
	 */
	private Pane[] col4;

	/**
	 * Variable zum Managen aller Panes in Spalte 5.
	 */
	private Pane[] col5;

	/**
	 * Variable zum Managen aller Panes in Spalte 6.
	 */
	private Pane[] col6;

	/**
	 * Variable zum Managen aller Panes in Spalte 7.
	 */
	private Pane[] col7;

	/**
	 * IsColInit dient der Überprüfung der Initialisierung der Spielstein-Panes.
	 */
	private boolean isColInit = false;

	/**
	 * In listDraw werden alle Spielzüge in einer doppelt-verketteten Liste
	 * abgespeichert.
	 */
	private LinkedListDraw listDraw = null;

	/**
	 * position wird bei der Simulation eines Spiel benötigt. In position wird
	 * der vom User gewünschte Spielzug abgespeichert. Die Variable wird mit der
	 * Variable currentPosition abgeglichen, um das Spiel zu dem gewünschten
	 * Spielzug vorzuspulen.
	 */
	private Position position;

	/**
	 * In currentPosition wird der momentan angezeigte Zug gespeichert.
	 */
	private Position currentPosition;

	/**
	 * In listDraw werden alle Spielzüge zu einem Spiel abgespeichert.
	 */
	private ObservableList<String> draws = null;

	/**
	 * Anzeigelabel für Spieler 1
	 */
	@FXML
	private Label label_PlayerOne;

	/**
	 * Anzeigelabel für Spieler 2
	 */
	@FXML
	private Label label_PlayerTwo;

	/**
	 * Anzeige des Zwischenstands auf dem Spielfeld UI.
	 */
	@FXML
	private Label label_Score;

	/**
	 * 
	 * Spielfeld Aufbau: Pane für Position 1/1
	 */
	@FXML
	private Pane col1_1;

	/**
	 * Spielfeld Aufbau: Pane für Position 1/2
	 */
	@FXML
	private Pane col1_2;

	/**
	 * Spielfeld Aufbau: Pane für Position 1/3
	 */
	@FXML
	private Pane col1_3;

	/**
	 * Spielfeld Aufbau: Pane für Position 1/4
	 */
	@FXML
	private Pane col1_4;

	/**
	 * Spielfeld Aufbau: Pane für Position 1/5
	 */
	@FXML
	private Pane col1_5;

	/**
	 * Spielfeld Aufbau: Pane für Position 1/6
	 */
	@FXML
	private Pane col1_6;

	/**
	 * Spielfeld Aufbau: Pane für Position 2/1
	 */
	@FXML
	private Pane col2_1;

	/**
	 * Spielfeld Aufbau: Pane für Position 2/2
	 */
	@FXML
	private Pane col2_2;

	/**
	 * Spielfeld Aufbau: Pane für Position 2/3
	 */
	@FXML
	private Pane col2_3;

	/**
	 * Spielfeld Aufbau: Pane für Position 2/4
	 */
	@FXML
	private Pane col2_4;

	/**
	 * Spielfeld Aufbau: Pane für Position 2/5
	 */
	@FXML
	private Pane col2_5;

	/**
	 * Spielfeld Aufbau: Pane für Position 2/6
	 */
	@FXML
	private Pane col2_6;

	/**
	 * Spielfeld Aufbau: Pane für Position 3/1
	 */
	@FXML
	private Pane col3_1;

	/**
	 * Spielfeld Aufbau: Pane für Position 3/2
	 */
	@FXML
	private Pane col3_2;

	/**
	 * Spielfeld Aufbau: Pane für Position 3/3
	 */
	@FXML
	private Pane col3_3;

	/**
	 * Spielfeld Aufbau: Pane für Position 3/4
	 */
	@FXML
	private Pane col3_4;

	/**
	 * Spielfeld Aufbau: Pane für Position 3/5
	 */
	@FXML
	private Pane col3_5;

	/**
	 * Spielfeld Aufbau: Pane für Position 3/6
	 */
	@FXML
	private Pane col3_6;

	/**
	 * Spielfeld Aufbau: Pane für Position 4/1
	 */
	@FXML
	private Pane col4_1;

	/**
	 * Spielfeld Aufbau: Pane für Position 4/2
	 */
	@FXML
	private Pane col4_2;

	/**
	 * Spielfeld Aufbau: Pane für Position 4/3
	 */
	@FXML
	private Pane col4_3;

	/**
	 * Spielfeld Aufbau: Pane für Position 4/4
	 */
	@FXML
	private Pane col4_4;

	/**
	 * Spielfeld Aufbau: Pane für Position 4/5
	 */
	@FXML
	private Pane col4_5;

	/**
	 * Spielfeld Aufbau: Pane für Position 4/6
	 */
	@FXML
	private Pane col4_6;

	/**
	 * Spielfeld Aufbau: Pane für Position 5/1
	 */
	@FXML
	private Pane col5_1;

	/**
	 * Spielfeld Aufbau: Pane für Position 5/2
	 */
	@FXML
	private Pane col5_2;

	/**
	 * Spielfeld Aufbau: Pane für Position 5/3
	 */
	@FXML
	private Pane col5_3;

	/**
	 * Spielfeld Aufbau: Pane für Position 5/4
	 */
	@FXML
	private Pane col5_4;

	/**
	 * Spielfeld Aufbau: Pane für Position 5/5
	 */
	@FXML
	private Pane col5_5;

	/**
	 * Spielfeld Aufbau: Pane für Position 5/6
	 */
	@FXML
	private Pane col5_6;

	/**
	 * Spielfeld Aufbau: Pane für Position 6/1
	 */
	@FXML
	private Pane col6_1;

	/**
	 * Spielfeld Aufbau: Pane für Position 6/2
	 */
	@FXML
	private Pane col6_2;

	/**
	 * Spielfeld Aufbau: Pane für Position 6/3
	 */
	@FXML
	private Pane col6_3;

	/**
	 * Spielfeld Aufbau: Pane für Position 6/4
	 */
	@FXML
	private Pane col6_4;

	/**
	 * Spielfeld Aufbau: Pane für Position 6/5
	 */
	@FXML
	private Pane col6_5;

	/**
	 * Spielfeld Aufbau: Pane für Position 6/6
	 */
	@FXML
	private Pane col6_6;

	/**
	 * Spielfeld Aufbau: Pane für Position 7/1
	 */
	@FXML
	private Pane col7_1;

	/**
	 * Spielfeld Aufbau: Pane für Position 7/2
	 */
	@FXML
	private Pane col7_2;

	/**
	 * Spielfeld Aufbau: Pane für Position 7/3
	 */
	@FXML
	private Pane col7_3;

	/**
	 * Spielfeld Aufbau: Pane für Position 7/4
	 */
	@FXML
	private Pane col7_4;

	/**
	 * Spielfeld Aufbau: Pane für Position 7/5
	 */
	@FXML
	private Pane col7_5;

	/**
	 * Spielfeld Aufbau: Pane für Position 7/6
	 */
	@FXML
	private Pane col7_6;

	/**
	 * Button für das Ausfahren des Einstellungsfensters. Löst Methode
	 * openSettings() aus.
	 */
	@FXML
	private Button btnSettingsHistory;
	/**
	 * Button zum Starten des Simulationsdurchlaufs. Löst Methode startsim()
	 * aus.
	 */
	@FXML
	private Button btnStartSimulation;

	/**
	 * Button zum Stoppen des Simulationsdurchlaufs. Löst Methode
	 * pauseSimulation() aus.
	 */
	@FXML
	private Button btnPauseSimulation;

	/**
	 * Button um den nächsten Spielzug anzuzeigen. Löst Methode next() aus.
	 */
	@FXML
	private Button btnForward;

	/**
	 * Button um einen Spielzug zurück zu gehen. Löst Methode backsim() aus;
	 */
	@FXML
	private Button btnPrevious;

	/**
	 * Button um einen geänderten Gewinner zu speichern. Löst Methode save()
	 * aus.
	 */
	@FXML
	private Button btnSaveHistory;

	/**
	 * Button um auf den Simulationscreen zu kommen. Löst Methode start() aus.
	 */
	@FXML
	private Button btnStart;

	/**
	 * Button um das Einstellungsfenster einzufahren, um zurueck auf den
	 * Simulationsbildschirm zu kommen.
	 */
	@FXML
	private Button btnBack;
	/**
	 * Button um die Bearbeitung des Gewinners freizuschalten. Löst Methode
	 * edit() aus.
	 */
	@FXML
	private Button btnEdit;

	/**
	 * AnchorPane für das HistoryUI, auf dem die Simulation stattfindet.
	 */
	@FXML
	private AnchorPane GameFieldHistory;

	/**
	 * AnchorPane für den Screen mit allen Details zu einem Spiel.
	 */
	@FXML
	private AnchorPane Gamedetails;

	/**
	 * Gridpane, dass sich hinter dem Spielfeld befindet.
	 */
	@FXML
	private GridPane grid;

	/**
	 * Dropdown-Menü für alle bereits gespielten Spiele. Löst Methode
	 * selectgame() aus, wenn ein Wert ausgewahlt wurde.
	 */
	@FXML
	private ComboBox<String> gamechoice;

	/**
	 * Dropdown-Menü für alle Züge, die zu dem ausgewählten Spiel gehören. Löst
	 * Methode selectDraw() aus, wenn ein Wert ausgewählt wurde.
	 */
	@FXML
	private ComboBox<String> CbSelectDraw;

	/**
	 * Anzeige für den Gewinner des ausgewählten Spiels.
	 */
	@FXML
	private TextField txtfld_winner;

	/**
	 * Anzeige für das Endergebnis des ausgewählten Spiels.
	 */
	@FXML
	private TextField txtfld_result;

	/**
	 * Anzeige für den Spieler 1 des ausgewählten Spiels.
	 */
	@FXML
	private TextField txtfld_PlayerOne;

	/**
	 * Anzeige für den Spieler 2 des ausgewählten Spiels.
	 */
	@FXML
	private TextField txtfld_PlayerTwo;

	/**
	 * Anzeige für den Modus des ausgewählten Spiels.
	 */
	@FXML
	private TextField txtfld_modus;

	/**
	 * Anzeige für den Schwierigkeitsgrad des ausgewählten Spiels.
	 */
	@FXML
	private TextField txtfld_level;

	/**
	 * Anzeige für die Gesamtzüge des ausgewählten Spiels.
	 */
	@FXML
	private TextField txtfld_draw;

	/**
	 * Anzeige der Tabelle mit den Satzdetails des ausgewählten Spiels.
	 */
	@FXML
	private TableView<Table_setDetails> table_details;

	/**
	 * Tabellenspalte Satznummer
	 */
	@FXML
	private TableColumn<Table_setDetails, String> setNr_table;

	/**
	 * Tabellenspalte Startspieler des Satzes
	 */
	@FXML
	private TableColumn<Table_setDetails, String> startPlayer;

	/**
	 * Tabellenspalte Gewinner des Satzes
	 */
	@FXML
	private TableColumn<Table_setDetails, String> winner;

	/**
	 * Tabellenspalte Zwischenergebnis des Satzes
	 */
	@FXML
	private TableColumn<Table_setDetails, String> result;

	/**
	 * Die Methode initializiert das Dropdown-Menü mit allen bereits gespielten
	 * Spielen.
	 */
	@Override
	public void initialize(URL location, ResourceBundle resources) {

		ArrayList<String> allGamesString = new ArrayList<String>();

		allGames = Spiel.getallgames();
		for (int i = 0; i < allGames.size(); i++) {
			allGamesString.add("Spiel " + allGames.get(i).getId() + ": "
					+ allGames.get(i).getSpielerA() + " gegen "
					+ allGames.get(i).getSpielerB());
		}

		ObservableList<String> options = FXCollections
				.observableArrayList(allGamesString);
		gamechoice.setItems(options);

		Gamedetails.setVisible(true);
		Gamedetails.setOpacity(1);
		GameFieldHistory.setOpacity(0.8);
		btnPrevious.setVisible(false);

	}

	/**
	 * Die Methode gibt alle bereits gespielten Spiele aus der Datenbank zurück.
	 * 
	 * @return allGames
	 */
	@SuppressWarnings("rawtypes")
	public ArrayList getAllGames() {
		ArrayList<Spiel> allGames = Spiel.getallgames();
		return allGames;
	}

	/**
	 * Die Methode simuliert ein bereits gespieltes Spiel.
	 * 
	 * @param event
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public void startSim(ActionEvent event) throws IOException,
	InterruptedException {

		gamechoice.setDisable(true);
		// Es wird ein JavaFx-Thread angelegt, um mit dem UI interagieren zu
		// können.

		Runnable b = new Runnable() {
			@Override
			public void run() {

				while (position != listDraw.getLastElem()) {
					position = position.getNextElem();
					Platform.runLater(new Runnable() {
						@Override
						public void run() {
							@SuppressWarnings("unused")
							Zug draw = null;

							CbSelectDraw.setValue("Satz "
									+ (game.saetze.get(position.getSet())
											.getSatznr())
											+ ", Zug "
											+ game.saetze.get(position.getSet()).zuege
											.get((position.getDrawNr() - 1))
											.getZug_nr());
						}
					});
					try {
						Thread.sleep(800);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

				Platform.runLater(new Runnable() {
					@Override
					public void run() {
						{
							check();
							CbSelectDraw.setDisable(false);
							btnPauseSimulation.setVisible(false);
							gamechoice.setDisable(false);
						}
					}
				});
			}
		};

		btnPrevious.setVisible(false);
		btnForward.setVisible(false);
		btnStartSimulation.setVisible(false);
		CbSelectDraw.setDisable(true);
		CbSelectDraw.setOpacity(1);
		btnPauseSimulation.setVisible(true);

		threadUI = new Thread(b);
		threadUI.start();
	}

	/**
	 * Die Methode spielt den nächsten Zug im Spiel.
	 * 
	 * @param event
	 * @throws IOException
	 */
	public void next(ActionEvent event) throws IOException {
		position = position.getNextElem();

		CbSelectDraw.setValue("Satz "
				+ (game.saetze.get(position.getSet()).getSatznr())
				+ ", Zug "
				+ game.saetze.get(position.getSet()).zuege.get(
						(position.getDrawNr() - 1)).getZug_nr());
	}

	/**
	 * Die Methode setzt das Spiel einen Zug zurück.
	 * 
	 * @param event
	 * @throws IOException
	 */
	public void backSim(ActionEvent event) throws IOException {
		position = position.getPrevElem();

		CbSelectDraw.setValue("Satz "
				+ (game.saetze.get(position.getSet()).getSatznr())
				+ ", Zug "
				+ game.saetze.get(position.getSet()).zuege.get(
						(position.getDrawNr() - 1)).getZug_nr());
	}

	/**
	 * Die Methode bringt den User zurück auf das Hauptmenü.
	 * 
	 * @param event
	 * @throws IOException
	 */
	public void startMenu(ActionEvent event) throws IOException {
		MainController.changeScene(event, "/view/Menu.fxml");

	}

	/**
	 * Die Methode setzt das Spielfeld auf den gewünschten Spielzug. Sie wird
	 * ausgelöst, entweder wenn der User einen Zug aus dem Dropdown-Menü oder
	 * wenn der User den Vorwärts/Rückwärts-Button drückt oder wenn der User die
	 * Simulation startet.
	 * 
	 * @param event
	 * @throws IOException
	 */
	public void selectDraw(ActionEvent event) throws IOException {

		if (newGame == false) {
			// Combobox auslesen
			String SetDraw = CbSelectDraw.valueProperty().get();

			// Satz & Zug auslesen
			String splitSetDraw[] = SetDraw.split(", ");
			String splitSet[] = splitSetDraw[0].split("Satz ");
			String splitDraw[] = splitSetDraw[1].split("Zug ");

			int set = Integer.parseInt(splitSet[1]) - 1;
			int drawNr = Integer.parseInt(splitDraw[1]);

			// Züge bis Soll-Zug setzen
			position = listDraw.find((drawNr), set);

			// um zum gewünschten Zug zugelangen, wird das Spiel zurück gespult
			if (position.getId() < currentPosition.getId()) {
				while (currentPosition != position) {

					deleteCircle(currentPosition.getColumn());

					if (currentPosition.getSet() > currentPosition
							.getPrevElem().getSet()) {
						reset();
						if (game.saetze
								.get(currentPosition.getPrevElem().getSet())
								.getStartspieler()
								.equalsIgnoreCase(game.getSpielerA()))
							selectedStartplayer = 1;
						else
							selectedStartplayer = 2;

						buildSet();
					}
					currentPosition = currentPosition.getPrevElem();
				}

				// um zum gewünschten Zug zugelangen, wird das Spiel nach vorne
				// gespult
			} else if (position.getId() > currentPosition.getId()) {
				while (currentPosition != position) {

					if (currentPosition.getSet() < currentPosition
							.getNextElem().getSet()) {
						reset();
						if (game.saetze
								.get((currentPosition.getNextElem().getSet()))
								.getStartspieler()
								.equalsIgnoreCase(game.getSpielerA())) {
							selectedStartplayer = 1;
						} else {
							selectedStartplayer = 2;
						}
					}

					currentPosition = currentPosition.getNextElem();
					setGrid(currentPosition.getColumn());
				}
			}
			check();
		} else {
			newGame = false;
		}
	}

	/**
	 * Die Methode setzt den Spielstand auf dem Simulationsscreen abhängig von
	 * dem momentanten Satz.
	 * 
	 * @param set
	 */
	private void setScore(int set) {

		int resultA = 0;
		int resultB = 0;
		for (int i = 0; i < set; i++) {
			resultA = resultA + game.saetze.get(i).getSpielerAPunkte();
			resultB = resultB + game.saetze.get(i).getSpielerBPunkte();
		}

		label_Score.setText(resultA + ":" + resultB);

	}

	/**
	 * Die Methode öffnet die Übersicht mit allen Details über das Spiel.
	 * 
	 * 
	 * @param event
	 * @throws IOException
	 */
	public void openSettings(ActionEvent event) throws IOException {
		Gamedetails.setVisible(true);
		GameFieldHistory.setOpacity(0.8);

		// Buttons sperren
		btnForward.setDisable(true);
		btnPrevious.setDisable(true);
		btnStartSimulation.setDisable(true);
		CbSelectDraw.setDisable(true);
	}

	/**
	 * Die MEthode bringt den User von dem Screen mit den Spieldetails auf den
	 * Simulationscreen. Dabei wird das Dropbown-Menü mit allen möglichen Zügen
	 * des ausgewählten Spiels gefüllt. Zu dem wird die Anfangsposition gesetzt.
	 * 
	 * @param event
	 * @throws IOException
	 */
	public void start(ActionEvent event) throws IOException {

		Gamedetails.setVisible(false);
		GameFieldHistory.setOpacity(1);
		label_PlayerOne.setText(txtfld_PlayerOne.getText());
		label_PlayerTwo.setText(game.getSpielerB());
		btnForward.setDisable(false);
		btnPrevious.setDisable(false);
		btnStartSimulation.setDisable(false);
		CbSelectDraw.setDisable(false);
		btnStart.setVisible(false);
		btnBack.setVisible(true);
		label_Score.setText("0 : 0");

		ArrayList<String> allDrawsString = new ArrayList<String>();
		listDraw = new LinkedListDraw();
		Position pos = null;

		int id = 0;
		for (int i = 0; i < game.saetze.size(); i++) {
			for (int j = 0; j < game.saetze.get(i).zuege.size(); j++) {
				Zug draw = game.saetze.get(i).zuege.get(j);
				// Spielzug in die verkettete Liste hinzufügen
				pos = new Position(draw, i, id);
				id++;
				listDraw.addLast(pos);

				// Spielzug in das Dropdown-Menü hinzufügen
				allDrawsString.add("Satz " + (game.saetze.get(i).getSatznr())
						+ ", Zug "
						+ game.saetze.get(i).zuege.get(j).getZug_nr());
			}
		}
		// Anfangsposition setzen
		currentPosition = listDraw.getFirstElem();
		position = listDraw.getFirstElem();

		draws = FXCollections.observableArrayList(allDrawsString);
		CbSelectDraw.setItems(draws);

		if (game.saetze.get(position.getNextElem().getSet()).getStartspieler()
				.equalsIgnoreCase(game.getSpielerB())) {
			selectedStartplayer = 2;
		} else {
			selectedStartplayer = 1;
		}
		newGame = true;
		CbSelectDraw.setValue("Bitte einen Spielzug auswählen!");
	}

	/**
	 * Die Methode erlaubt dem User die Bearbeitungs des Spielgewinners.
	 * 
	 * @param event
	 */
	public void edit(ActionEvent event) {
		btnEdit.setVisible(false);
		txtfld_winner.setDisable(false);
		txtfld_winner.setEditable(true);
		btnSaveHistory.setVisible(true);
	}

	/**
	 * Die Methode speichert den geänderten Gewinner in die Datenbank ab.
	 * 
	 * @param event
	 */
	public void save(ActionEvent event) {
		btnEdit.setVisible(true);
		txtfld_winner.setDisable(true);
		txtfld_winner.setEditable(false);
		btnSaveHistory.setVisible(false);

		game.SetWinner(txtfld_winner.getText());
		try {
			game.perstistWinner();
		} catch (Exception e) {
			System.out.println("nicht abgespeichert");
		}
	}

	/**
	 * Die Methode setzt die Spieldetails zu einem im Dropdown-Menü ausgewählten
	 * Spiels. Die Methode füllt die Textfelder für die Spieler, das
	 * Endergebnis, den Modus, der Schwierigkeitslevel, die Tabelle mit den
	 * Details über einen Satz (Satznummer, Startspieler, Satzgewinner,
	 * Zwischenergebnis), die Gesamtanzahl der Züge und den Gewinner des Spiels.
	 * 
	 * @param event
	 * @throws IOException
	 */
	public void selectGame(ActionEvent event) throws IOException {

		reset();
		btnStart.setVisible(true);
		btnStart.setDisable(false);
		btnEdit.setDisable(false);
		btnBack.setVisible(false);

		// Buttons auf dem Simulations UI wieder auf Default zuruecksetzen
		btnPrevious.setVisible(false);
		btnStartSimulation.setVisible(true);
		btnForward.setVisible(true);
		// Spiel Id auslesen & Spielinstanz holen
		String item = gamechoice.getValue();
		String split[] = item.split(":");
		String splitId[] = split[0].split("Spiel ");
		int Id = Integer.parseInt(splitId[1]);

		for (int i = 0; i < allGames.size(); i++) {
			if (Id == allGames.get(i).getId()) {
				game = allGames.get(i);
			}
		}

		// Spiel Instanz auslesen
		ArrayList<Table_setDetails> details = new ArrayList<Table_setDetails>();
		Table_setDetails data = null;
		String winner_name;
		int playerOne;
		int playerTwo;
		int draws = 0;

		String playerOneName = game.getSpielerA();
		String playerTwoName = game.getSpielerB();

		// Textfelder für Spieler 1 und Spieler 2 füllen
		txtfld_PlayerOne.setText(playerOneName);
		txtfld_PlayerTwo.setText(playerTwoName);

		// Befüllen einer Tabelle mit den jeweiligen Daten eines Satzes
		for (int i = 0; i < game.saetze.size(); i++) {
			// Gesamtanzahl der Züge: Addition der Züge aus den einzelnen Sätzen
			draws = draws + game.saetze.get(i).zuege.size();

			// Punkte für Spieler 1 und Spieler 2
			playerOne = game.saetze.get(i).getSpielerAPunkte();
			playerTwo = game.saetze.get(i).getSpielerBPunkte();

			// Gewinner des Satzes festlegen
			if (playerOne > playerTwo)
				winner_name = playerOneName;
			else if (playerTwo > playerOne)
				winner_name = playerTwoName;
			else
				winner_name = "Unentschieden";

			// Satznummer festlegen
			String setNr_table = null;
			setNr_table = String.valueOf(game.saetze.get(i).getSatznr());

			// Datensatz anlegen
			data = new Table_setDetails(setNr_table, game.saetze.get(i)
					.getStartspieler(), winner_name, game.saetze.get(i)
					.getSpielerAPunkte()
					+ ":"
					+ game.saetze.get(i).getSpielerBPunkte());

			details.add(data);
		}

		// Tabelle anlegen mit Spalten Satznummer, Satzgewinner, Startspieler
		// und Ergebnis
		setNr_table
		.setCellValueFactory(new PropertyValueFactory<Table_setDetails, String>(
				"setNr_table"));
		winner.setCellValueFactory(new PropertyValueFactory<Table_setDetails, String>(
				"winner"));
		startPlayer
		.setCellValueFactory(new PropertyValueFactory<Table_setDetails, String>(
				"startPlayer"));
		result.setCellValueFactory(new PropertyValueFactory<Table_setDetails, String>(
				"result"));

		// Tabelle füllen
		ObservableList<Table_setDetails> table_de = FXCollections
				.observableArrayList(details);
		table_details.setItems(table_de);

		// Textfeld Gewinner füllen
		txtfld_winner.setText(game.getWinner());

		// Textfeld Gesamtzüge,Endergebnis, Modus, Level & Gewinner füllen
		String drawAll = null;
		drawAll = String.valueOf(draws);

		txtfld_draw.setText(drawAll);
		txtfld_result.setText(game.getResultPlayerA() + ":"
				+ game.getResultPlayerB());
		txtfld_modus.setText(game.getModus());
		txtfld_level.setText(game.getLevel());
		txtfld_winner.setText(game.getWinner());

		newGame = true;
	}

	/**
	 * Die Methode setzt einen Spielstein auf das Spielfeld.
	 * 
	 * @param column
	 */
	public void setGrid(int column) {

		// Verringern des Wertes des Spielfeld-Arrays in der Spalte um eins
		int row = manageRows[column];
		manageRows[column] = manageRows[column] - 1;

		if (isColInit == false) {
			col1 = initManageCols1();
			col2 = initManageCols2();
			col3 = initManageCols3();
			col4 = initManageCols4();
			col5 = initManageCols5();
			col6 = initManageCols6();
			col7 = initManageCols7();
			isColInit = true;
		}
		// Auswahl der Farben (Rot <-> Geld)
		// setzen eines Spielsteins in Spalte & Reihe
		// Wechsel der Spielstein Farbe durch Erhöhung des Counters um eins

		if (selectedStartplayer == 1) {
			// Auswahl der Farben (Rot <-> Geld)
			if (counter % 2 != 0) {

				switch (column) {
				case 0:
					col1[row].setId("red");
					break;
				case 1:
					col2[row].setId("red");
					break;
				case 2:
					col3[row].setId("red");
					break;
				case 3:
					col4[row].setId("red");
					break;
				case 4:
					col5[row].setId("red");
					break;
				case 5:
					col6[row].setId("red");
					break;
				case 6:
					col7[row].setId("red");
					break;
				default:
					System.out.println("Fehler");
				}
				counter++;
			} else {

				switch (column) {
				case 0:
					col1[row].setId("yellow");
					break;
				case 1:
					col2[row].setId("yellow");
					break;
				case 2:
					col3[row].setId("yellow");
					break;
				case 3:
					col4[row].setId("yellow");
					break;
				case 4:
					col5[row].setId("yellow");
					break;
				case 5:
					col6[row].setId("yellow");
					break;
				case 6:
					col7[row].setId("yellow");
					break;
				default:
					System.out.println("Fehler");
				}
				counter++;
			}
		} else {
			if (counter % 2 == 0) {

				switch (column) {
				case 0:
					col1[row].setId("red");
					break;
				case 1:
					col2[row].setId("red");
					break;
				case 2:
					col3[row].setId("red");
					break;
				case 3:
					col4[row].setId("red");
					break;
				case 4:
					col5[row].setId("red");
					break;
				case 5:
					col6[row].setId("red");
					break;
				case 6:
					col7[row].setId("red");
					break;
				default:
					System.out.println("Fehler");
				}
				counter++;
			} else {

				switch (column) {
				case 0:
					col1[row].setId("yellow");
					break;
				case 1:
					col2[row].setId("yellow");
					break;
				case 2:
					col3[row].setId("yellow");
					break;
				case 3:
					col4[row].setId("yellow");
					break;
				case 4:
					col5[row].setId("yellow");
					break;
				case 5:
					col6[row].setId("yellow");
					break;
				case 6:
					col7[row].setId("yellow");
					break;
				default:
					System.out.println("Fehler");
				}
				counter++;
			}
		}
	}

	/**
	 * Die Methode löscht einen bereits gesetzten Zug.
	 * 
	 * @param column
	 */
	public void deleteCircle(int column) {

		// Spalte wieder um eins erhöhen
		manageRows[column] = manageRows[column] + 1;
		int row = manageRows[column];

		// Spielstein aus der Spalte & Reihe entfernen
		switch (column) {
		case 0:
			col1[row].setId("empty");
			break;
		case 1:
			col2[row].setId("empty");
			break;
		case 2:
			col3[row].setId("empty");
			break;
		case 3:
			col4[row].setId("empty");
			break;
		case 4:
			col5[row].setId("empty");
			break;
		case 5:
			col6[row].setId("empty");
			break;
		case 6:
			col7[row].setId("empty");
			break;
		default:
			System.out.println("Fehler");
		}
		// Farbwechsel-Variable um eins verringern -> Farbe wechselt
		counter--;

	}

	/**
	 * Die Methode initialisiert Spalte 1.
	 * 
	 * @return manageCols
	 */
	private Pane[] initManageCols1() {
		Pane[] manageCols = { col1_6, col1_5, col1_4, col1_3, col1_2, col1_1 };
		return manageCols;
	}

	/**
	 * Die Methode initialisiert Spalte 2.
	 * 
	 * @return manageCols
	 */
	private Pane[] initManageCols2() {
		Pane[] manageCols = { col2_6, col2_5, col2_4, col2_3, col2_2, col2_1 };
		return manageCols;
	}

	/**
	 * Die Methode initialisiert Spalte 3.
	 * 
	 * @return manageCols
	 */
	private Pane[] initManageCols3() {
		Pane[] manageCols = { col3_6, col3_5, col3_4, col3_3, col3_2, col3_1 };
		return manageCols;
	}

	/**
	 * Die Methode initialisiert Spalte 4.
	 * 
	 * @return manageCols
	 */
	private Pane[] initManageCols4() {
		Pane[] manageCols = { col4_6, col4_5, col4_4, col4_3, col4_2, col4_1 };
		return manageCols;
	}

	/**
	 * Die Methode initialisiert Spalte 5.
	 * 
	 * @return manageCols
	 */
	private Pane[] initManageCols5() {
		Pane[] manageCols = { col5_6, col5_5, col5_4, col5_3, col5_2, col5_1 };
		return manageCols;
	}

	/**
	 * Die Methode initialisiert Spalte 6.
	 * 
	 * @return manageCols
	 */
	private Pane[] initManageCols6() {
		Pane[] manageCols = { col6_6, col6_5, col6_4, col6_3, col6_2, col6_1 };
		return manageCols;
	}

	/**
	 * Die Methode initialisiert Spalte 7.
	 * 
	 * @return manageCols
	 */
	private Pane[] initManageCols7() {
		Pane[] manageCols = { col7_6, col7_5, col7_4, col7_3, col7_2, col7_1 };
		return manageCols;
	}

	/**
	 * Die Methode setzt das Spielfeld auf ihren Anfangszustand zurück und
	 * resettet alle gebrauchten Variablen.
	 */
	private void reset() {

		// Spielsteine löschen
		if (col1 != null) {
			for (int i = 0; i <= 5; i++) {
				col1[i].setId("empty");
				col2[i].setId("empty");
				col3[i].setId("empty");
				col4[i].setId("empty");
				col5[i].setId("empty");
				col6[i].setId("empty");
				col7[i].setId("empty");
			}
		}

		// Variablen auf Anfangswert setzen
		int resetManageRows[] = { 5, 5, 5, 5, 5, 5, 5 };
		manageRows = resetManageRows;
		counter = 0;

	}

	/**
	 * Die Methode baut einen vorherigen Satz auf, wenn der User z.B. einen
	 * Schritt zurück geht und dabei den Satz wechselt.
	 * 
	 * @param back
	 */
	private void buildSet() {

		// finden des Satzanfangs
		Position resetPosition = listDraw.find(1, currentPosition.getPrevElem()
				.getSet());

		// setzen der Spielsteine vom Satzanfang bis zum Satzende
		while (resetPosition != currentPosition) {
			setGrid(game.saetze.get(resetPosition.getSet()).zuege.get(
					resetPosition.getDrawNr() - 1).getSpalte());
			resetPosition = resetPosition.getNextElem();
		}
	}

	/**
	 * Die Methode überprüft ob der ausgewählte Zug der erste oder letzte Zug
	 * ist. Sollte es der letzte Zug sein, wird die Vorwärts-und Start
	 * Simulations-Taste ausgeblendet. Sollte es der erste Zug sein, wird der
	 * Rückwärtsbutton ausgeblendet.
	 */
	private void check() {
		// Überprüfung ob der ausgewählte Zug der letzte Zug ist
		if (position != listDraw.getLastElem()) {
			btnForward.setVisible(true);
			btnStartSimulation.setVisible(true);
			setScore(position.getSet());
		} else {
			btnForward.setVisible(false);
			btnStartSimulation.setVisible(false);
			label_Score.setText(game.getResultPlayerA() + ":"
					+ game.getResultPlayerB());
		}

		// Überprüfung ob der ausgewählte Zug der erste Zug ist
		if (position != listDraw.getFirstElem().getNextElem())
			btnPrevious.setVisible(true);
		else
			btnPrevious.setVisible(false);
	}

	/**
	 * Die Methode pausiert den Simulationslauf.
	 * 
	 * @param event
	 * @throws IOException
	 */
	@SuppressWarnings("deprecation")
	public void pauseSimulation(ActionEvent event) throws IOException {

		gamechoice.setDisable(false);
		threadUI.stop();
		check();
		CbSelectDraw.setDisable(false);
		btnStartSimulation.setVisible(true);
		btnPauseSimulation.setVisible(false);

	}

	/**
	 * Die Methode schließt das Fenster mit den Einstellungen für die
	 * Simulation. Sie bezieht sich auf den Zurueck Button.
	 */
	public void closeSetting(ActionEvent event) {
		Gamedetails.setVisible(false);
		GameFieldHistory.setOpacity(1);

		// Buttons aktivieren
		btnForward.setDisable(false);
		btnPrevious.setDisable(false);
		btnStartSimulation.setDisable(false);
		CbSelectDraw.setDisable(false);
	}
}
