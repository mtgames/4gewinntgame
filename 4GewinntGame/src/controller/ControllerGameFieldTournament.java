/**
 * 
 */
package controller;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Observable;
import java.util.ResourceBundle;


import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.stage.DirectoryChooser;
import model.db.Spiel;
import supportFiles.Constants;
import supportFiles.GameplayCommunicator;
import supportFiles.GameplayDataPackage;
import view.Main;

/**
 * 
 * Die Klasse kümmert sich um die Steuerung des Turniermodus. Die Steuerung
 * betrifft die des Einstellungscreens, auf dem der User alle Einstellungen für
 * das Spiel tätigt, die der Steuerung der Spielwürfe der KI und des Gegners und
 * die der Steuerung der Gesamtübersicht, auf der alle Daten eines Spiel nochmal
 * angezeigt werden.
 * 
 * @author MTGames
 * @Mitglieder Timo Strauch, Tim Holm, Meike Helwig, Marcel Schäfer
 */
public class ControllerGameFieldTournament extends GameplayCommunicator
implements Initializable {

	/**
	 * manageRows managed das Spielfeld. Es gibt 7 Spalten und jede Spalte kann
	 * 6 Spielsteine aufnehmen. Sobald ein Spielstein in einer Spalte gesetzt
	 * wird, wird der dazugehörige Wert um eins verringert.
	 */
	private int manageRows[] = { 5, 5, 5, 5, 5, 5, 5 };

	/**
	 * Counter dient als HilfsVariable für den Farbwechsel bei dem Setzen von
	 * Spielsteinen.
	 */
	private int counter = 0;
	
	/**
	 * Die Variable setzt fest, ob man Spieler X oder Spieler O ist.
	 */
	private int selectedPlayer = 1;

	/**
	 * Variable zum Managen aller Panes in Spalte 1.
	 */
	private Pane[] col1;

	/**
	 * Variable zum Managen aller Panes in Spalte 2.
	 */
	private Pane[] col2;

	/**
	 * Variable zum Managen aller Panes in Spalte 3.
	 */
	private Pane[] col3;

	/**
	 * Variable zum Managen aller Panes in Spalte 4.
	 */
	private Pane[] col4;

	/**
	 * Variable zum Managen aller Panes in Spalte 5.
	 */
	private Pane[] col5;

	/**
	 * Variable zum Managen aller Panes in Spalte 6.
	 */
	private Pane[] col6;

	/**
	 * Variable zum Managen aller Panes in Spalte 7.
	 */
	private Pane[] col7;

	/**
	 * IsColInit dient der Überprüfung der Initialisierung der Spielstein-Panes.
	 */
	private boolean isColInit = false;

	/**
	 * game dient als Spiel-Instanz, in der die Daten zu einem Spiel gespeichert
	 * wird. Sie wird in die DB persistiert, wenn der User
	 */
	private Spiel game = null;

	/**
	 * Die Variable speichert die momentane Satznummer ab.
	 */
	private int setNr = 0;

	/**
	 * Die Variable wird zur Prüfung der Farbe des Startspielers verwendet. 
	 */
	private boolean isStartColorYellow;

	/**
	 * Die Variable speichert den Modus ab.
	 */
	private String modus;

	/**
	 * Die Variable speichert den Startspieler ab.
	 */
	private String startPlayer;

	/**
	 * Die Variable speichert den Gegner ab. Gegener bedeutet in dem
	 * Zusammenhang, dass dieser Spieler nicht der Startspieler ist.
	 */
	private String opponentPlayer;

	/**
	 * AnchorPane für das SpielfeldUI, auf dem das Spiel stattfindet.
	 */
	@FXML
	private AnchorPane GameField;

	/**
	 * AnchorPane für den Screen mit allen Details zu einem Spiel.
	 */
	@FXML
	private AnchorPane Gamedetails;

	/**
	 * AnchorPane auf dem der User alle Einstellungen für das Spiel eingeben
	 * muss.
	 */
	@FXML
	private AnchorPane settingScreen;

	/**
	 * 
	 * Spielfeld Aufbau: Pane für Position 1/1
	 */
	@FXML
	private Pane col1_1;

	/**
	 * Spielfeld Aufbau: Pane für Position 1/2
	 */
	@FXML
	private Pane col1_2;

	/**
	 * Spielfeld Aufbau: Pane für Position 1/3
	 */
	@FXML
	private Pane col1_3;

	/**
	 * Spielfeld Aufbau: Pane für Position 1/4
	 */
	@FXML
	private Pane col1_4;

	/**
	 * Spielfeld Aufbau: Pane für Position 1/5
	 */
	@FXML
	private Pane col1_5;

	/**
	 * Spielfeld Aufbau: Pane für Position 1/6
	 */
	@FXML
	private Pane col1_6;

	/**
	 * Spielfeld Aufbau: Pane für Position 2/1
	 */
	@FXML
	private Pane col2_1;

	/**
	 * Spielfeld Aufbau: Pane für Position 2/2
	 */
	@FXML
	private Pane col2_2;

	/**
	 * Spielfeld Aufbau: Pane für Position 2/3
	 */
	@FXML
	private Pane col2_3;

	/**
	 * Spielfeld Aufbau: Pane für Position 2/4
	 */
	@FXML
	private Pane col2_4;

	/**
	 * Spielfeld Aufbau: Pane für Position 2/5
	 */
	@FXML
	private Pane col2_5;

	/**
	 * Spielfeld Aufbau: Pane für Position 2/6
	 */
	@FXML
	private Pane col2_6;

	/**
	 * Spielfeld Aufbau: Pane für Position 3/1
	 */
	@FXML
	private Pane col3_1;

	/**
	 * Spielfeld Aufbau: Pane für Position 3/2
	 */
	@FXML
	private Pane col3_2;

	/**
	 * Spielfeld Aufbau: Pane für Position 3/3
	 */
	@FXML
	private Pane col3_3;

	/**
	 * Spielfeld Aufbau: Pane für Position 3/4
	 */
	@FXML
	private Pane col3_4;

	/**
	 * Spielfeld Aufbau: Pane für Position 3/5
	 */
	@FXML
	private Pane col3_5;

	/**
	 * Spielfeld Aufbau: Pane für Position 3/6
	 */
	@FXML
	private Pane col3_6;

	/**
	 * Spielfeld Aufbau: Pane für Position 4/1
	 */
	@FXML
	private Pane col4_1;

	/**
	 * Spielfeld Aufbau: Pane für Position 4/2
	 */
	@FXML
	private Pane col4_2;

	/**
	 * Spielfeld Aufbau: Pane für Position 4/3
	 */
	@FXML
	private Pane col4_3;

	/**
	 * Spielfeld Aufbau: Pane für Position 4/4
	 */
	@FXML
	private Pane col4_4;

	/**
	 * Spielfeld Aufbau: Pane für Position 4/5
	 */
	@FXML
	private Pane col4_5;

	/**
	 * Spielfeld Aufbau: Pane für Position 4/6
	 */
	@FXML
	private Pane col4_6;

	/**
	 * Spielfeld Aufbau: Pane für Position 5/1
	 */
	@FXML
	private Pane col5_1;

	/**
	 * Spielfeld Aufbau: Pane für Position 5/2
	 */
	@FXML
	private Pane col5_2;

	/**
	 * Spielfeld Aufbau: Pane für Position 5/3
	 */
	@FXML
	private Pane col5_3;

	/**
	 * Spielfeld Aufbau: Pane für Position 5/4
	 */
	@FXML
	private Pane col5_4;

	/**
	 * Spielfeld Aufbau: Pane für Position 5/5
	 */
	@FXML
	private Pane col5_5;

	/**
	 * Spielfeld Aufbau: Pane für Position 5/6
	 */
	@FXML
	private Pane col5_6;

	/**
	 * Spielfeld Aufbau: Pane für Position 6/1
	 */
	@FXML
	private Pane col6_1;

	/**
	 * Spielfeld Aufbau: Pane für Position 6/2
	 */
	@FXML
	private Pane col6_2;

	/**
	 * Spielfeld Aufbau: Pane für Position 6/3
	 */
	@FXML
	private Pane col6_3;

	/**
	 * Spielfeld Aufbau: Pane für Position 6/4
	 */
	@FXML
	private Pane col6_4;

	/**
	 * Spielfeld Aufbau: Pane für Position 6/5
	 */
	@FXML
	private Pane col6_5;

	/**
	 * Spielfeld Aufbau: Pane für Position 6/6
	 */
	@FXML
	private Pane col6_6;

	/**
	 * Spielfeld Aufbau: Pane für Position 7/1
	 */
	@FXML
	private Pane col7_1;

	/**
	 * Spielfeld Aufbau: Pane für Position 7/2
	 */
	@FXML
	private Pane col7_2;

	/**
	 * Spielfeld Aufbau: Pane für Position 7/3
	 */
	@FXML
	private Pane col7_3;

	/**
	 * Spielfeld Aufbau: Pane für Position 7/4
	 */
	@FXML
	private Pane col7_4;

	/**
	 * Spielfeld Aufbau: Pane für Position 7/5
	 */
	@FXML
	private Pane col7_5;

	/**
	 * Spielfeld Aufbau: Pane für Position 7/6
	 */
	@FXML
	private Pane col7_6;

	/**
	 * Der Button ruft das Menu auf. Löst die Methode startMenu() aus.
	 */
	@FXML
	private Button btnMenu;

	/**
	 * Der Button startet das Turnier. Löst die Methode startTournament() aus.
	 */
	@FXML
	private Button btnStartTournament;

	/**
	 * Der Radiobutton dient zur Auswahl des Spielers. Ist dieser aktiviert, ist
	 * man Spieler O.
	 */
	@FXML
	private RadioButton player1RBtn;

	/**
	 * Der Radiobutton dient zur Auswahl des Spielers. Ist dieser aktiviert, ist
	 * man Spieler X.
	 */
	@FXML
	private RadioButton player2RBtn;

	/**
	 * In das Feld wird der Kontaktpfad von dem User eingetragen.
	 */
	@FXML
	private TextField txtfld_contactPath;

	/**
	 * In das Feld wird die Zugzeit von dem User eingetragen.
	 */
	@FXML
	private TextField txtfld_zugZeit;

	/**
	 * In das Textfeld trägt der User den Namen des Spieler O ein.
	 */
	@FXML
	private TextField txtfld_PlayerOne;

	/**
	 * In das Textfeld trägt der User den Namen des Spieler X ein.
	 */
	@FXML
	private TextField txtfld_PlayerTwo;

	/**
	 * Anzeige für die Gesamtanzahl an benötigten Zügen.
	 */
	@FXML
	private TextField txtfld_sum_draw;
	/**
	 * Anzeige für den Spieler 1 auf der Gesamtübersicht.
	 */
	@FXML
	private TextField txtfld_sum_p1;

	/**
	 * Anzeige für den Spieler 2 auf der Gesamtübersicht.
	 */
	@FXML
	private TextField txtfld_sum_p2;

	/**
	 * Anzeige für das Endergebnis auf der Gesamtübersicht.
	 */
	@FXML
	private TextField txtfld_sum_score;

	/**
	 * Anzeige für den Modus auf der Gesamtübersicht.
	 */
	@FXML
	private TextField txtfld_sum_mode;

	/**
	 * Anzeige für den Schwierigkeitsgrad auf der Gesamtübersicht.
	 */
	@FXML
	private TextField txtfld_sum_level;

	/**
	 * Anzeige für den Gewinner auf der Gesamtübersicht.
	 */
	@FXML
	private TextField txtfld_winner;

	/**
	 * Anzeige des Spieler 1 auf dem Spielfeld UI.
	 */
	@FXML
	private Label label_PlayerOne;

	/**
	 * Anzeige des Spieler 2 auf dem Spielfeld UI.
	 */
	@FXML
	private Label label_PlayerTwo;

	/**
	 * Anzeige einer Nachricht, die den Sieger beinhaltet, auf dem Spielfeld UI.
	 */
	@FXML
	private Label labelWinnerMsg;

	/**
	 * Anzeige einer Nachricht, die über den Startspieler des aktuellen Satzes
	 * informier und auf dem Spielfeld UI angezeigt wird.
	 */
	@FXML
	private Label label_startplayer;

	/**
	 * Anzeige des Zwischenstands auf dem Spielfeld UI.
	 */
	@FXML
	private Label label_Score;

	/**
	 * Button zur Auswahldes Kontaktpfades mit Hilfe eines DirectoryChoosers.
	 * Löst die Methode chooseDirectory() aus. 
	 */
	@FXML
	private Button btnDirectoryChooser;

	/**
	 * Button für den Spielwurf in Spalte 1. Löst Methode addCol1() aus.
	 */
	@FXML
	private Button col1Btn;

	/**
	 * Button für den Spielwurf in Spalte 2. Löst Methode addCol2() aus.
	 */
	@FXML
	private Button col2Btn;

	/**
	 * Button für den Spielwurf in Spalte 3. Löst Methode addCol3() aus.
	 */
	@FXML
	private Button col3Btn;

	/**
	 * Button für den Spielwurf in Spalte 4. Löst Methode addCol4() aus.
	 */
	@FXML
	private Button col4Btn;

	/**
	 * Button für den Spielwurf in Spalte 5. Löst Methode addCol5() aus.
	 */
	@FXML
	private Button col5Btn;

	/**
	 * Button für den Spielwurf in Spalte 6. Löst Methode addCol6() aus.
	 */
	@FXML
	private Button col6Btn;

	/**
	 * Button für den Spielwurf in Spalte 7. Löst Methode addCol7() aus.
	 */
	@FXML
	private Button col7Btn;

	/**
	 * Durch den Button gelangt der User in den Modus, in dem er den Gewinner
	 * eines Spiels ändern kann. Löst Methode edit() aus.
	 */
	@FXML
	private Button btnEdit;

	/**
	 * Gridpane, dass sich hinter dem Spielfeld befindet.
	 */
	@FXML
	private GridPane grid;

	/**
	 * Mit dem Button kann der User den Einstellungsscreen schließen. Löst
	 * Methode closeSettingScreen() aus.
	 */
	@FXML
	private Button btnBack;

	/**
	 * Der Button löst einen neuen Satz aus. Das Spiel läuft weiter. Löst
	 * Methode startnewSet() aus.
	 */
	
	@FXML
	private Button btnEnd;

	/**
	 * Der Button löst einen neuen Satz aus. Das Spiel läuft weiter. Löst
	 * Methode startnewSetTour() aus.
	 */
	@FXML
	private Button btnNewSetTour;

	/**
	 * Anzeige der Tabelle mit den Satzdetails des ausgewählten Spiels.
	 */
	@FXML
	private TableView<Table_setDetails> table_details;

	/**
	 * Tabellenspalte Satznummer
	 */
	@FXML
	private TableColumn<Table_setDetails, String> setNr_table;

	/**
	 * Tabellenspalte Startspieler des Satzes
	 */
	@FXML
	private TableColumn<Table_setDetails, String> startPlayer_table;

	/**
	 * Tabellenspalte Gewinner des Satzes
	 */
	@FXML
	private TableColumn<Table_setDetails, String> winner;

	/**
	 * Tabellenspalte Zwischenergebnis des Satzes
	 */
	@FXML
	private TableColumn<Table_setDetails, String> result;

	/**
	 * Diese Methode ist für die Kommunikation mit dem KI Backend (Model) zuständig.
	 * Sobald das Model/ KI die Methode "notifyObservers" ausführt, wird diese Methode
	 * im UI Controller aufgerufen. Hier werden Informationen zum Spiel übermittelt.
	 * Dazu zählen der Wurf der KI, der Zeitpunkt des Spielendes sowie der Satzsieger.
	 */
	@Override
	public void update(Observable o, Object arg) {

		final GameplayDataPackage newData = (GameplayDataPackage) arg;

		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				int changingContext;
				changingContext = GameplayDataPackage.CONTEXT_UI_TOURNAMENT;

				if (newData.context == changingContext
						|| newData.context == GameplayDataPackage.CONTEXT_UI_SERVER) {
					if (newData.contentInfo == GameplayDataPackage.CONTENT_START_PLAYER) {

						setStartplayer(newData.value);
					} else if (newData.contentInfo == GameplayDataPackage.CONTENT_THROWINFO) {
						setGrid(newData.value);

					} else if (newData.contentInfo == GameplayDataPackage.CONTENT_MSG) {

						endSet(newData.msg);
						btnNewSetTour.setVisible(true);
					}
				}

			}

		});
	}

	/**
	 * Dieser Eventhändler prüft die Eingabe der Tastatur. Sie ist mit den Eingabefeldern
	 * für die Spielernamen verknüpft und stellt sicher, dass ein Spielername 
	 * eingetragen ist.
	 */
	EventHandler<KeyEvent> mouseHandler = new EventHandler<KeyEvent>() {
		@Override
		public void handle(KeyEvent arg0) {
			checkEntries();
			try {
				Integer.parseInt(arg0.getText());
			} catch (Exception e) {
			}

		}

	};

	/**
	 * Die Methode initialisiert das TurnierUI.
	 */
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		GameField.setOpacity(0.8);
		txtfld_PlayerOne.setOnKeyReleased(mouseHandler);
		txtfld_PlayerTwo.setOnKeyReleased(mouseHandler);
		txtfld_contactPath.setOnKeyReleased(mouseHandler);
		txtfld_zugZeit.setOnKeyReleased(mouseHandler);

	}

	/**
	 * wird im Turniermodus nicht verwendet! Muss jedoch implementiert werden, da von {@link GameplayCommunicator}
	 * geerbt wird und diese Schnittstelle zur Kommunikation verwendet werden soll.
	 */
	@Override
	public synchronized int getPlayersThrow() {

		return 0;
	}

	/**
	 * wird im Turniermodus nicht verwendet! Muss jedoch implementiert werden, da von {@link GameplayCommunicator}
	 * geerbt wird und diese Klasse zur Kommunikation verwendet werden soll.
	 */
	@Override
	public synchronized void setPlayersThrow(int throwColumn) {

	}

	/**
	 * Die Methode öffnet den Einstellungsscreen.
	 * 
	 * @param event
	 * @throws IOException
	 */
	public void openSettings(ActionEvent event) throws IOException {
		settingScreen.setVisible(true);
		GameField.setOpacity(0.5);
		txtfld_PlayerOne.setDisable(true);
		txtfld_PlayerTwo.setDisable(true);
		btnNewSetTour.setDisable(true);

	}

	/**
	 * Die Methode setzt alle Variablen auf den Anfangswert zurück.
	 */
	private void resetGamefield() {
		if (col1 != null) {
			for (int i = 0; i <= 5; i++) {
				col1[i].setId("empty");
				col2[i].setId("empty");
				col3[i].setId("empty");
				col4[i].setId("empty");
				col5[i].setId("empty");
				col6[i].setId("empty");
				col7[i].setId("empty");
			}
		}
		label_startplayer.setText("");
		int resetManageRows[] = { 5, 5, 5, 5, 5, 5, 5 };
		manageRows = resetManageRows;

	}

	/**
	 * Die Methode startet einen neuen Satz innerhalb eines Spiels. Der
	 * Einstellungsscreen wird geöffnet, um die Einstellungen ändern zu können.
	 * 
	 * @param event
	 * @throws IOException
	 */
	public void startNewSetTour(ActionEvent event) throws IOException {
		// open Setting Screen
		settingScreen.setVisible(true);
		GameField.setOpacity(0.8);

		disableSettingElements(false);
		
		label_startplayer.setText("");
		btnBack.setVisible(false);
		btnStartTournament.setVisible(true);
		btnStartTournament.setDisable(true);

		checkEntries();

		resetGamefield();
		btnNewSetTour.setVisible(false);
		btnBack.setVisible(false);

		counter = 0;
		setNr++;
		MainController.deinitializeTournamentClasses();
		btnStartTournament.setVisible(true);

	}

	/**
	 * Die Methode navigiert den User ins Hauptmenü.
	 * 
	 * @param event
	 * @throws IOException
	 */
	public void startMenu(ActionEvent event) throws IOException {
		if (this.gameMaster != null) {
			MainController.deinitializeTournamentClasses();
		}
		MainController.changeScene(event, "/view/Menu.fxml");
	}

	/**
	 * Die Methode beendet ein Spiel, öffnet die Gesamtübersicht eines Spiels
	 * und setzt alle Daten für die Gesamtübersicht.
	 */
	private void gameHasEnded() {
		settingScreen.setVisible(false);
		Gamedetails.setVisible(true);

		txtfld_sum_mode.setDisable(true);
		txtfld_sum_p1.setDisable(true);
		txtfld_sum_p2.setDisable(true);
		txtfld_sum_score.setDisable(true);

		int draws = 0;
		ArrayList<Table_setDetails> details = new ArrayList<Table_setDetails>();

		Table_setDetails data = null;
		String winner_name = null;
		int playerOne;
		int playerTwo;

		String playerOneName = game.getSpielerA();
		String playerTwoName = game.getSpielerB();
		int resultA = 0;
		int resultB = 0;
		String nr_Set = null;

		// Gesamtzüge addieren -> draws, Endresult von A & B zusammenrechnen
		// ->resultA/resultB,
		// Satzgewinner-> winner_name => Tabelle schreiben
		for (int i = 0; i < game.saetze.size(); i++) {
			draws = draws + game.saetze.get(i).zuege.size();

			playerOne = game.saetze.get(i).getSpielerAPunkte();
			playerTwo = game.saetze.get(i).getSpielerBPunkte();

			resultA = resultA + game.getResultPlayerA();
			resultB = resultB + game.getResultPlayerB();

			if (playerOne > playerTwo)
				winner_name = playerOneName;
			else if (playerTwo > playerOne)
				winner_name = playerTwoName;
			else
				winner_name = "Unentschieden";

			nr_Set = String.valueOf(game.saetze.get(i).getSatznr());

			data = new Table_setDetails(nr_Set, game.saetze.get(i)
					.getStartspieler(), winner_name, game.saetze.get(i)
					.getSpielerAPunkte()
					+ ":"
					+ game.saetze.get(i).getSpielerBPunkte());

			details.add(data);
		}

		// Tabelle anlegen & setzen
		setNr_table
		.setCellValueFactory(new PropertyValueFactory<Table_setDetails, String>(
				"setNr_table"));
		winner.setCellValueFactory(new PropertyValueFactory<Table_setDetails, String>(
				"winner"));
		startPlayer_table
		.setCellValueFactory(new PropertyValueFactory<Table_setDetails, String>(
				"startPlayer"));
		result.setCellValueFactory(new PropertyValueFactory<Table_setDetails, String>(
				"result"));

		ObservableList<Table_setDetails> table_de = FXCollections
				.observableArrayList(details);

		table_details.setItems(table_de);

		// Felder befüllen
		String drawAll = null;

		drawAll = String.valueOf(draws);
		txtfld_sum_draw.setText(drawAll);

		txtfld_sum_mode.setText(game.getModus());
		txtfld_sum_p1.setText(playerOneName);
		txtfld_sum_p2.setText(playerTwoName);
		txtfld_sum_score.setText(game.getResultPlayerA() + ":"
				+ game.getResultPlayerB());
		txtfld_PlayerOne.setText(game.getSpielerA());
		txtfld_PlayerTwo.setText(game.getSpielerB());

		// gewinner setzen
		game.SetWinner(winner_name);

		// Endsieger
		if (resultA > resultB)
			game.SetWinner(game.getSpielerA());
		else if (resultA < resultB)
			game.SetWinner(game.getSpielerB());
		else
			game.SetWinner("Unentschieden");

		txtfld_winner.setText(game.getWinner());

	}

	/**
	 * Die Methode beendet ein Spiel und löst Methode hameHasEnded() aus.
	 * 
	 * @param event
	 * @throws IOException
	 */
	public void startGameEnd(ActionEvent event) throws IOException {
		btnEnd.setDisable(true);
		MainController.deinitializeTournamentClasses();
		gameHasEnded();
	}

	/**
	 * Die Methode startet das Turnier.
	 * 
	 * @param event
	 * @throws IOException
	 */
	public void startTournament(ActionEvent event) throws IOException {
		btnEnd.setDisable(false);
		btnStartTournament.setVisible(false);
		btnBack.setVisible(true);

		disableSettingElements(true);

		label_PlayerOne.setText(txtfld_PlayerOne.getText());
		label_PlayerTwo.setText(txtfld_PlayerTwo.getText());
		labelWinnerMsg.setVisible(false);

		settingScreen.setVisible(false);
		GameField.setOpacity(1);

		txtfld_sum_mode.setText("Turniermodus");
		modus = "Turniermodus";

		startTournamentM();
	}

	/**
	 * Die Methode startet die KI im Turniermodus und legt eine Spielinstanz an,
	 * in der alle Daten zu dem Spiel gespeichert werden.
	 */
	public void startTournamentM() {
		// neues Spiel in der DB anlegen
		if (game == null) {
			game = new Spiel(txtfld_PlayerOne.getText(),
					txtfld_PlayerTwo.getText(), modus, "Kein Level");
		}
		game.addSatz(0, 0, "");

		MainController.initTournamentClasses(this);
		label_Score.setText(game.getResultPlayerA() + " : "
				+ game.getResultPlayerB());

		if (selectedPlayer == 1) {
			MainController.serverComm.start(Constants.SPIELERO,
					txtfld_contactPath.getText(),
					Integer.parseInt(txtfld_zugZeit.getText()));
		} else {
			MainController.serverComm.start(Constants.SPIELERX,
					txtfld_contactPath.getText(),
					Integer.parseInt(txtfld_zugZeit.getText()));
		}

	}

	/**
	 * Diese Methode ist für die zentrale Steuerung der Buttons und Eingabefelder
	 * auf dem Einstellungsfenster(SettingScreen) zuständig. Während einem laufenden
	 * Spiel wird hiermit verhindert, dass die Einstellungen geändert werden können. 
	 * 
	 * @param disableValue
	 */
	private void disableSettingElements(boolean disableValue) {
		player1RBtn.setDisable(disableValue);
		player2RBtn.setDisable(disableValue);
		txtfld_PlayerOne.setDisable(disableValue);
		txtfld_PlayerTwo.setDisable(disableValue);
		txtfld_contactPath.setDisable(disableValue);
		btnDirectoryChooser.setDisable(disableValue);
		txtfld_zugZeit.setDisable(disableValue);
	}

	/**
	 * Wenn diese Methode ausgelöst wird, hat der User festgelegt, dass er
	 * Spieler X ist.
	 * 
	 * @param event
	 * @throws IOException
	 */
	public void setPlayerX(ActionEvent event) throws IOException {
		selectedPlayer = 2;
		player1RBtn.setSelected(false);
		player2RBtn.setSelected(true);
	}

	/**
	 * Wenn diese Methode ausgelöst wird, hat der User festgelegt, dass er
	 * Spieler O ist.
	 * 
	 * @param event
	 * @throws IOException
	 */
	public void setPlayerO(ActionEvent event) throws IOException {
		selectedPlayer = 1;
		player1RBtn.setSelected(true);
		player2RBtn.setSelected(false);
	}

	/**
	 * Die Methode setzt die Button auf dem Einstellungscreen zurück.
	 */
	public void resetGameSetting() {
		// Spielfeld UI Handling (SettingScreen sichtbar)
		settingScreen.setVisible(true);
		GameField.setOpacity(0.8);
		Gamedetails.setVisible(false);
		// Reset UI im Setting Screen -> Eingaben für neues Spiel möglich
		// Buttons
		btnEdit.setDisable(false);
		btnEnd.setDisable(true);
		btnBack.setVisible(false);
		btnNewSetTour.setVisible(false);
		btnStartTournament.setVisible(true);
		btnStartTournament.setDisable(true);
		disableSettingElements(false);
		// Label + Textfelder
		label_PlayerOne.setText("Spieler 1");
		label_PlayerTwo.setText("Spieler 2");
		label_Score.setText("");
		txtfld_winner.setDisable(true);
		txtfld_PlayerOne.setText("");
		txtfld_PlayerTwo.setText("");
		txtfld_contactPath.setText("");
		txtfld_PlayerOne.setText("");
		txtfld_zugZeit.setText("");
		txtfld_contactPath.setText("");
		labelWinnerMsg.setText("");
		label_startplayer.setText("");
		// Variablen für den Eingabecheck

		// Reset des Spielfeldes sowie Variablen für den Spielverlauf
		resetGamefield();
		game = null;
		setNr = 0;
		counter = 0;
	}

	/**
	 * Die Methode verwirft ein Spiel. Der User löscht das grad gespielte Spiel
	 * und gelangt wieder auf den Einstellungsscreen.
	 * 
	 * @param event
	 * @throws IOException
	 */
	public void delete(ActionEvent event) throws IOException {
		resetGameSetting();
	}

	/**
	 * Die Methode speichert ein Spiel in die Datenbank ab.
	 * 
	 * @param event
	 * @throws IOException
	 */
	public void save(ActionEvent event) throws IOException {

		try {
			game.persist();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("Speichern fehlgeschlagen!");
		}
		resetGameSetting();
	}

	/**
	 * Die Methode löst den Editier-Modus aus. Der User kann den Gewinner nun
	 * ändern.
	 * 
	 * @param event
	 * @throws IOException
	 */
	public void edit(ActionEvent event) throws IOException {
		btnEdit.setDisable(true);
		txtfld_winner.setEditable(true);
		txtfld_winner.setDisable(false);
	}

	/**
	 * Die Methode initialisiert Spalte 1.
	 * 
	 * @return manageCols
	 */
	private Pane[] initManageCols1() {
		Pane[] manageCols = { col1_6, col1_5, col1_4, col1_3, col1_2, col1_1 };
		return manageCols;
	}

	/**
	 * Die Methode initialisiert Spalte 2.
	 * 
	 * @return manageCols
	 */
	private Pane[] initManageCols2() {
		Pane[] manageCols = { col2_6, col2_5, col2_4, col2_3, col2_2, col2_1 };
		return manageCols;
	}

	/**
	 * Die Methode initialisiert Spalte 3.
	 * 
	 * @return manageCols
	 */
	private Pane[] initManageCols3() {
		Pane[] manageCols = { col3_6, col3_5, col3_4, col3_3, col3_2, col3_1 };
		return manageCols;
	}

	/**
	 * Die Methode initialisiert Spalte 4.
	 * 
	 * @return manageCols
	 */
	private Pane[] initManageCols4() {
		Pane[] manageCols = { col4_6, col4_5, col4_4, col4_3, col4_2, col4_1 };
		return manageCols;
	}

	/**
	 * Die Methode initialisiert Spalte 5.
	 * 
	 * @return manageCols
	 */
	private Pane[] initManageCols5() {
		Pane[] manageCols = { col5_6, col5_5, col5_4, col5_3, col5_2, col5_1 };
		return manageCols;
	}

	/**
	 * Die Methode initialisiert Spalte 6.
	 * 
	 * @return manageCols
	 */
	private Pane[] initManageCols6() {
		Pane[] manageCols = { col6_6, col6_5, col6_4, col6_3, col6_2, col6_1 };
		return manageCols;
	}

	/**
	 * Die Methode initialisiert Spalte 7.
	 * 
	 * @return manageCols
	 */
	private Pane[] initManageCols7() {
		Pane[] manageCols = { col7_6, col7_5, col7_4, col7_3, col7_2, col7_1 };
		return manageCols;
	}

	/**
	 * Die Methode setzt einen Spielstein auf das Spielfeld.
	 * 
	 * @param column
	 */
	public void setGrid(int column) {

		int row = manageRows[column];
		manageRows[column] = manageRows[column] - 1;

		if (isColInit == false) {
			col1 = initManageCols1();
			col2 = initManageCols2();
			col3 = initManageCols3();
			col4 = initManageCols4();
			col5 = initManageCols5();
			col6 = initManageCols6();
			col7 = initManageCols7();
			isColInit = true;
		}
		// Auswahl der Farben (Gelb <-> Rot)
		if (isStartColorYellow == true) {
			// Auswahl der Farben (Rot <-> Geld)
			if (counter % 2 != 0) {
				game.saetze.get(setNr).addZug(startPlayer, column);
				switch (column) {
				case 0:
					col1[row].setId("red");
					break;
				case 1:
					col2[row].setId("red");
					break;
				case 2:
					col3[row].setId("red");
					break;
				case 3:
					col4[row].setId("red");
					break;
				case 4:
					col5[row].setId("red");
					break;
				case 5:
					col6[row].setId("red");
					break;
				case 6:
					col7[row].setId("red");
					break;
				default:
					System.out.println("Fehler");
				}
				counter++;
			} else {
				game.saetze.get(setNr).addZug(opponentPlayer, column);
				switch (column) {
				case 0:
					col1[row].setId("yellow");
					break;
				case 1:
					col2[row].setId("yellow");
					break;
				case 2:
					col3[row].setId("yellow");
					break;
				case 3:
					col4[row].setId("yellow");
					break;
				case 4:
					col5[row].setId("yellow");
					break;
				case 5:
					col6[row].setId("yellow");
					break;
				case 6:
					col7[row].setId("yellow");
					break;
				default:
					System.out.println("Fehler");
				}
				counter++;
			}
		} else {
			if (counter % 2 == 0) {
				game.saetze.get(setNr).addZug(startPlayer, column);

				switch (column) {
				case 0:
					col1[row].setId("red");
					break;
				case 1:
					col2[row].setId("red");
					break;
				case 2:
					col3[row].setId("red");
					break;
				case 3:
					col4[row].setId("red");
					break;
				case 4:
					col5[row].setId("red");
					break;
				case 5:
					col6[row].setId("red");
					break;
				case 6:
					col7[row].setId("red");
					break;
				default:
					System.out.println("Fehler");
				}
				counter++;
			} else {
				game.saetze.get(setNr).addZug(opponentPlayer, column);
				switch (column) {
				case 0:
					col1[row].setId("yellow");
					break;
				case 1:
					col2[row].setId("yellow");
					break;
				case 2:
					col3[row].setId("yellow");
					break;
				case 3:
					col4[row].setId("yellow");
					break;
				case 4:
					col5[row].setId("yellow");
					break;
				case 5:
					col6[row].setId("yellow");
					break;
				case 6:
					col7[row].setId("yellow");
					break;
				default:
					System.out.println("Fehler");
				}
				counter++;
			}
		}
	}

	/**
	 * Die Methode setzt die Punktevergabe im Punktemodus. Punkte: Gewinner = 2
	 * Unentschieden = 1, keine feste Spiellaenge
	 * 
	 * @param winner
	 */
	public void setPointsPunktemodus(int winner) {
		int pointsA = 0;
		int pointsB = 0;

		if (winner == 1) {
			pointsA = 2;
			pointsB = 0;
		} else if (winner == 2) {
			pointsA = 0;
			pointsB = 2;
		} else if (winner == 0) {
			pointsA = 1;
			pointsB = 1;
		}
		game.saetze.get(setNr).changePoints(pointsA, pointsB);
	}

	/**
	 * Die Methode überprüft ob der User alle nötigen Einstellungen getroffen
	 * hat.
	 */

	private void checkEntries() {
		try {
			Integer.parseInt(txtfld_zugZeit.getText());
			if (!(txtfld_PlayerOne.getText().equals(""))
					&& !(txtfld_PlayerTwo.getText().equals(""))
					&& !(txtfld_contactPath.getText().equals(""))
					&& !(txtfld_zugZeit.getText().equals(""))
					&& (player1RBtn.isSelected() == true || player2RBtn
					.isSelected() == true)) {
				btnStartTournament.setDisable(false);
			} else {
				btnStartTournament.setDisable(true);
			}
		} catch (Exception e) {
			btnStartTournament.setDisable(true);
		}
	}

	/**
	 * Die Methode wird aufgerufen, wenn ein Satz zu Ende ist. Sie löst die
	 * Punktevergabe je nach Gewinner und Modus aus.
	 * 
	 * @param message
	 */
	public void endSet(String message) {
		counter = 0;
		btnNewSetTour.setDisable(false);
		labelWinnerMsg.setVisible(true);

		if (message == GameplayDataPackage.MSG_KI_GEWINNT) {
			setPointsPunktemodus(selectedPlayer);
			if (selectedPlayer == 1) {
				labelWinnerMsg.setText(txtfld_PlayerOne.getText()
						+ " hat gewonnen!");
			} else if (selectedPlayer == 2) {
				labelWinnerMsg.setText(label_PlayerTwo.getText()
						+ " hat gewonnen!");
			}

		} else if (message == GameplayDataPackage.MSG_SPIELER_GEWINNT) {
			if (selectedPlayer == 1) {
				setPointsPunktemodus(2);
				labelWinnerMsg.setText(label_PlayerTwo.getText()
						+ " hat gewonnen!");
			} else if (selectedPlayer == 2) {
				setPointsPunktemodus(1);
				labelWinnerMsg.setText(txtfld_PlayerOne.getText()
						+ " hat gewonnen!");
			}
		}
		label_Score.setText(game.getResultPlayerA() + " : "
				+ game.getResultPlayerB());
	}

	/**
	 * Die Methode schließt den Einstellungsscreen.
	 * 
	 * @param event
	 * @throws IOException
	 */
	public void closeSettingScreen(ActionEvent event) throws IOException {
		settingScreen.setVisible(false);
		GameField.setOpacity(1);
		btnNewSetTour.setDisable(false);
	}

	/**
	 * Diese Methode legt den Startspieler fest. Der Startspieler wird vom
	 * Server festgelegt.
	 * 
	 * @param startplayer
	 */
	private void setStartplayer(int startplayer) {

		if (startplayer == Constants.ME) {
			if (selectedPlayer == 1) {
				game.saetze.get(setNr).setStartspieler(
						txtfld_PlayerOne.getText());
				isStartColorYellow = true;
				label_startplayer.setText(txtfld_PlayerOne.getText()
						+ " beginnt diesen Satz!");	
			} else if (selectedPlayer == 2) {
				game.saetze.get(setNr).setStartspieler(
						txtfld_PlayerTwo.getText());
				label_startplayer.setText(txtfld_PlayerTwo.getText()
						+ " beginnt diesen Satz!");
				isStartColorYellow = false;
			}
		} else if (startplayer == Constants.OPPONENT) {
			if (selectedPlayer == 1) {
				game.saetze.get(setNr).setStartspieler(
						txtfld_PlayerTwo.getText());
				isStartColorYellow = false;
				label_startplayer.setText(txtfld_PlayerTwo.getText()
						+ " beginnt diesen Satz!");	
			} else if (selectedPlayer == 2) {
				game.saetze.get(setNr).setStartspieler(
						txtfld_PlayerOne.getText());
				isStartColorYellow = true;
				label_startplayer.setText(txtfld_PlayerOne.getText()
						+ " beginnt diesen Satz!");	
			}
		}
	}

	/**
	 * Diese öffnet den FileChooser zur Auswahl des Kontaktpfades. 
	 * Zusätzlich findet sich hier die Logik des FileChooser.
	 */
	public void chooseDirectory (ActionEvent event){
		try {			
			// FileChooser-Objekt erstellen
			DirectoryChooser chooser = new DirectoryChooser();
			//	Dialog zum Oeffnen von Dateien anzeigen
			disableSettingElements(true);
			btnMenu.setDisable(true);
			File file = chooser.showDialog(Main.primaryStage);
			
			txtfld_contactPath.setText(file.getPath() + "/");

			checkEntries();
		} catch (Exception e) {
			// TODO: handle exception
		}
		disableSettingElements(false);
		btnMenu.setDisable(false);
	}
}
