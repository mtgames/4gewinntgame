package controller;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Random;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import model.db.Spiel;
import model.ki.GameField;
import model.ki.ThreatManager;

/**
 * Die Klasse kümmert sich um die Steuerung des TwoPlayer Modus. Die Steuerung
 * betrifft die des Einstellungscreens, auf dem der User alle Einstellungen für
 * das Spiel tätigt, die der Steuerung der Spieler und die der Steuerung der
 * Gesamtübersicht, auf der alle Daten eines Spiel nochmal angezeigt werden.
 * 
 * @author MTGames
 * @Mitglieder Timo Strauch, Tim Holm, Meike Helwig, Marcel Schäfer
 *
 */
public class ControllerGameFieldTwoPlayer implements Initializable {

	/**
	 * manageRows managed das Spielfeld. Es gibt 7 Spalten und jede Spalte kann
	 * 6 Spielsteine aufnehmen. Sobald ein Spielstein in einer Spalte gesetzt
	 * wird, wird der dazugehörige Wert um eins verringert.
	 */
	private int manageRows[] = { 5, 5, 5, 5, 5, 5, 5 };

	/**
	 * Variable überprüft ob der User einen Startspieler ausgewählt hat.
	 */
	boolean isPlayerSet = false;

	/**
	 * Variable überprüft ob der User einen Modus ausgewählt hat.
	 */
	boolean isModusSet = false;

	/**
	 * Counter dient als HilfsVariable für den Farbwechsel bei dem Setzen von
	 * Spielsteinen.
	 */
	private int counter = 0;

	/**
	 * Die Variable wird bei dem Farbwechsel benötigt. Sie entscheidet welcher
	 * Spieler welche Farbe hat.
	 */
	private int selectedStartPlayer = 1;

	/**
	 * Die Variable wird bei dem Farbwechsel benötigt. Sie entscheidet welcher
	 * Spieler welche Farbe hat.
	 */
	public int selectedOpponentPlayer = 1;

	/**
	 * Die Variable speichert den ausgewählten Schwierigskeitsgrad ab.
	 */
	public int selectedLevel = 0;

	/**
	 * Die Variable speichert den ausgewählten Modus ab.
	 */
	public int selectedModus = 0;

	/**
	 * IsColInit dient der Überprüfung der Initialisierung der Spielstein-Panes.
	 */
	private boolean isColInit = false;

	/**
	 * Variable zum Managen aller Panes in Spalte 1.
	 */
	private Pane[] col1;

	/**
	 * Variable zum Managen aller Panes in Spalte 2.
	 */
	private Pane[] col2;

	/**
	 * Variable zum Managen aller Panes in Spalte 3.
	 */
	private Pane[] col3;

	/**
	 * Variable zum Managen aller Panes in Spalte 4.
	 */
	private Pane[] col4;

	/**
	 * Variable zum Managen aller Panes in Spalte 5.
	 */
	private Pane[] col5;

	/**
	 * Variable zum Managen aller Panes in Spalte 6.
	 */
	private Pane[] col6;

	/**
	 * Variable zum Managen aller Panes in Spalte 7.
	 */
	private Pane[] col7;

	/**
	 * game dient als Spiel-Instanz, in der die Daten zu einem Spiel gespeichert
	 * wird. Sie wird in die DB persistiert, wenn der User
	 */
	private Spiel game = null;

	/**
	 * Die Variable speichert die momentane Satznummer ab.
	 */
	private int setNr = 0;

	/**
	 * Die Variable speichert den Modus ab.
	 */
	private String modus;

	/**
	 * Die Variable speichert den Schwierigskeitsgrad ab.
	 */
	private String level;

	/**
	 * Die Variable speichert den Startspieler ab.
	 */
	private String startPlayer;

	/**
	 * Die Variable speichert den Gegner ab. Gegener bedeutet in dem
	 * Zusammenhang, dass dieser Spieler nicht der Startspieler ist.
	 */
	private String opponentPlayer;

	/**
	 * Die Variable beinhaltet eine Instanz der Klasse GameField, die das Spiel
	 * repräsentiert.
	 */
	private GameField gameField = null;

	/**
	 * Die Variable beinhaltet eine Instanz der Klasse ThreatManager, in der die
	 * Gefahren auf dem Spielfeld aufgedeckt werden.
	 */
	private ThreatManager threatManager = null;

	/**
	 * AnchorPane für das SpielfeldUI, auf dem das Spiel stattfindet.
	 */
	@FXML
	private AnchorPane GameField;

	/**
	 * AnchorPane für den Screen mit allen Details zu einem Spiel.
	 */
	@FXML
	private AnchorPane Gamedetails;

	/**
	 * AnchorPane auf dem der User alle Einstellungen für das Spiel eingeben
	 * muss.
	 */
	@FXML
	private AnchorPane settingScreen;

	/**
	 * 
	 * Spielfeld Aufbau: Pane für Position 1/1
	 */
	@FXML
	private Pane col1_1;

	/**
	 * Spielfeld Aufbau: Pane für Position 1/2
	 */
	@FXML
	private Pane col1_2;

	/**
	 * Spielfeld Aufbau: Pane für Position 1/3
	 */
	@FXML
	private Pane col1_3;

	/**
	 * Spielfeld Aufbau: Pane für Position 1/4
	 */
	@FXML
	private Pane col1_4;

	/**
	 * Spielfeld Aufbau: Pane für Position 1/5
	 */
	@FXML
	private Pane col1_5;

	/**
	 * Spielfeld Aufbau: Pane für Position 1/6
	 */
	@FXML
	private Pane col1_6;

	/**
	 * Spielfeld Aufbau: Pane für Position 2/1
	 */
	@FXML
	private Pane col2_1;

	/**
	 * Spielfeld Aufbau: Pane für Position 2/2
	 */
	@FXML
	private Pane col2_2;

	/**
	 * Spielfeld Aufbau: Pane für Position 2/3
	 */
	@FXML
	private Pane col2_3;

	/**
	 * Spielfeld Aufbau: Pane für Position 2/4
	 */
	@FXML
	private Pane col2_4;

	/**
	 * Spielfeld Aufbau: Pane für Position 2/5
	 */
	@FXML
	private Pane col2_5;

	/**
	 * Spielfeld Aufbau: Pane für Position 2/6
	 */
	@FXML
	private Pane col2_6;

	/**
	 * Spielfeld Aufbau: Pane für Position 3/1
	 */
	@FXML
	private Pane col3_1;

	/**
	 * Spielfeld Aufbau: Pane für Position 3/2
	 */
	@FXML
	private Pane col3_2;

	/**
	 * Spielfeld Aufbau: Pane für Position 3/3
	 */
	@FXML
	private Pane col3_3;

	/**
	 * Spielfeld Aufbau: Pane für Position 3/4
	 */
	@FXML
	private Pane col3_4;

	/**
	 * Spielfeld Aufbau: Pane für Position 3/5
	 */
	@FXML
	private Pane col3_5;

	/**
	 * Spielfeld Aufbau: Pane für Position 3/6
	 */
	@FXML
	private Pane col3_6;

	/**
	 * Spielfeld Aufbau: Pane für Position 4/1
	 */
	@FXML
	private Pane col4_1;

	/**
	 * Spielfeld Aufbau: Pane für Position 4/2
	 */
	@FXML
	private Pane col4_2;

	/**
	 * Spielfeld Aufbau: Pane für Position 4/3
	 */
	@FXML
	private Pane col4_3;

	/**
	 * Spielfeld Aufbau: Pane für Position 4/4
	 */
	@FXML
	private Pane col4_4;

	/**
	 * Spielfeld Aufbau: Pane für Position 4/5
	 */
	@FXML
	private Pane col4_5;

	/**
	 * Spielfeld Aufbau: Pane für Position 4/6
	 */
	@FXML
	private Pane col4_6;

	/**
	 * Spielfeld Aufbau: Pane für Position 5/1
	 */
	@FXML
	private Pane col5_1;

	/**
	 * Spielfeld Aufbau: Pane für Position 5/2
	 */
	@FXML
	private Pane col5_2;

	/**
	 * Spielfeld Aufbau: Pane für Position 5/3
	 */
	@FXML
	private Pane col5_3;

	/**
	 * Spielfeld Aufbau: Pane für Position 5/4
	 */
	@FXML
	private Pane col5_4;

	/**
	 * Spielfeld Aufbau: Pane für Position 5/5
	 */
	@FXML
	private Pane col5_5;

	/**
	 * Spielfeld Aufbau: Pane für Position 5/6
	 */
	@FXML
	private Pane col5_6;

	/**
	 * Spielfeld Aufbau: Pane für Position 6/1
	 */
	@FXML
	private Pane col6_1;

	/**
	 * Spielfeld Aufbau: Pane für Position 6/2
	 */
	@FXML
	private Pane col6_2;

	/**
	 * Spielfeld Aufbau: Pane für Position 6/3
	 */
	@FXML
	private Pane col6_3;

	/**
	 * Spielfeld Aufbau: Pane für Position 6/4
	 */
	@FXML
	private Pane col6_4;

	/**
	 * Spielfeld Aufbau: Pane für Position 6/5
	 */
	@FXML
	private Pane col6_5;

	/**
	 * Spielfeld Aufbau: Pane für Position 6/6
	 */
	@FXML
	private Pane col6_6;

	/**
	 * Spielfeld Aufbau: Pane für Position 7/1
	 */
	@FXML
	private Pane col7_1;

	/**
	 * Spielfeld Aufbau: Pane für Position 7/2
	 */
	@FXML
	private Pane col7_2;

	/**
	 * Spielfeld Aufbau: Pane für Position 7/3
	 */
	@FXML
	private Pane col7_3;

	/**
	 * Spielfeld Aufbau: Pane für Position 7/4
	 */
	@FXML
	private Pane col7_4;

	/**
	 * Spielfeld Aufbau: Pane für Position 7/5
	 */
	@FXML
	private Pane col7_5;

	/**
	 * Spielfeld Aufbau: Pane für Position 7/6
	 */
	@FXML
	private Pane col7_6;

	/**
	 * Der Button beendet ein Spiel und der user gelangt auf die Gesamtübersicht
	 * des Spiels. Löst Methode startGameEnd() aus.
	 */
	@FXML
	private Button btnEnd;

	/**
	 * Der Button löst den Start eines neuen Spiels aus. Die Methode
	 * startTwoPlayer() aus.
	 */
	@FXML
	private Button btnStartTwoPlayer;

	/**
	 * In das Textfeld trägt der User den Namen des Spieler 1 ein.
	 */
	@FXML
	private TextField txtfld_PlayerOne;

	/**
	 * In das Textfeld trägt der User den Namen des Spieler X ein.
	 */
	@FXML
	private TextField txtfld_PlayerTwo;

	/**
	 * Anzeige für die Gesamtanzahl an benötigten Zügen.
	 */
	@FXML
	private TextField txtfld_sum_draw;

	/**
	 * Anzeige für den Spieler 1 auf der Gesamtübersicht.
	 */
	@FXML
	private TextField txtfld_sum_p1;

	/**
	 * Anzeige für den Spieler 2 auf der Gesamtübersicht.
	 */
	@FXML
	private TextField txtfld_sum_p2;

	/**
	 * Anzeige für das Endergebnis auf der Gesamtübersicht.
	 */
	@FXML
	private TextField txtfld_sum_score;

	/**
	 * Anzeige für den Modus auf der Gesamtübersicht.
	 */
	@FXML
	private TextField txtfld_sum_mode;

	/**
	 * Anzeige für den Schwierigkeitsgrad auf der Gesamtübersicht.
	 */
	@FXML
	private TextField txtfld_sum_level;

	/**
	 * Anzeige für den Gewinner auf der Gesamtübersicht.
	 */
	@FXML
	private TextField txtfld_winner;

	/**
	 * Anzeige des Spieler 1 auf dem Spielfeld UI.
	 */
	@FXML
	private Label label_PlayerOne;

	/**
	 * Anzeige des Spieler 2 auf dem Spielfeld UI.
	 */
	@FXML
	private Label label_PlayerTwo;

	/**
	 * Anzeige einer Nachricht, die den Sieger beinhaltet, auf dem Spielfeld UI.
	 */
	@FXML
	private Label labelWinnerMsg;

	/**
	 * Anzeige des Zwischenstands auf dem Spielfeld UI.
	 */
	@FXML
	private Label label_Score;

	/**
	 * Anzeige einer Nachricht, die über den Startspieler des aktuellen Satzes
	 * informier und auf dem Spielfeld UI angezeigt wird.
	 */
	@FXML
	private Label label_startplayer;

	/**
	 * Button für den Spielwurf in Spalte 1. Löst Methode addCol1() aus.
	 */
	@FXML
	private Button col1Btn;

	/**
	 * Button für den Spielwurf in Spalte 2. Löst Methode addCol2() aus.
	 */
	@FXML
	private Button col2Btn;

	/**
	 * Button für den Spielwurf in Spalte 3. Löst Methode addCol3() aus.
	 */
	@FXML
	private Button col3Btn;

	/**
	 * Button für den Spielwurf in Spalte 4. Löst Methode addCol4() aus.
	 */
	@FXML
	private Button col4Btn;

	/**
	 * Button für den Spielwurf in Spalte 5. Löst Methode addCol5() aus.
	 */
	@FXML
	private Button col5Btn;

	/**
	 * Button für den Spielwurf in Spalte 6. Löst Methode addCol6() aus.
	 */
	@FXML
	private Button col6Btn;

	/**
	 * Button für den Spielwurf in Spalte 7. Löst Methode addCol7() aus.
	 */
	@FXML
	private Button col7Btn;

	/**
	 * Durch den Button gelangt der User in den Modus, in dem er den Gewinner
	 * eines Spiels ändern kann. Löst Methode edit() aus.
	 */
	@FXML
	private Button btnEdit;

	/**
	 * Gridpane, dass sich hinter dem Spielfeld befindet.
	 */
	@FXML
	private GridPane grid;

	/**
	 * Setzt den Modus auf den Standardmodus. Löst Methode setStandardmodus()
	 * aus.
	 */
	@FXML
	private Button btnStandardmodus;

	/**
	 * Setzt den Modus auf den Punktemodus. Löst Methode setPunktemodus() aus.
	 */
	@FXML
	private Button btnPunktemodus;

	/**
	 * Setzt den Modus auf den Vorsprungsmodus. Löst Methode
	 * setVorsprungsmodus() aus.
	 */
	@FXML
	private Button btnVorsprungsmodus;

	/**
	 * Setzt den Startspieler auf Spieler 1. Löst Methode setPlayerOne() aus.
	 */
	@FXML
	private Button btnPlayerOne;

	/**
	 * Setzt den Startspieler auf Spieler 2. Löst Methode setPlayerTwo() aus.
	 */
	@FXML
	private Button btnPlayerTwo;

	/**
	 * Setzt den Startspieler anhand einer Zufallszahl. Löst Methode setRandom()
	 * aus.
	 */
	@FXML
	private Button btnRandom;

	/**
	 * Mit dem Button kann der User den Einstellungsscreen schließen. Löst
	 * Methode closeSettingScreen() aus.
	 */
	@FXML
	private Button btnBack;

	/**
	 * Der Button löst einen neuen Satz aus. Das Spiel läuft weiter. Löst
	 * Methode startnewSet() aus.
	 */
	@FXML
	private Button btnNewSet;

	/**
	 * Anzeige der Tabelle mit den Satzdetails des ausgewählten Spiels.
	 */
	@FXML
	private TableView<Table_setDetails> table_details;

	/**
	 * Tabellenspalte Satznummer
	 */
	@FXML
	private TableColumn<Table_setDetails, String> setNr_table;

	/**
	 * Tabellenspalte Startspieler des Satzes
	 */
	@FXML
	private TableColumn<Table_setDetails, String> startPlayer_table;

	/**
	 * Tabellenspalte Gewinner des Satzes
	 */
	@FXML
	private TableColumn<Table_setDetails, String> winner;

	/**
	 * Tabellenspalte Zwischenergebnis des Satzes
	 */
	@FXML
	private TableColumn<Table_setDetails, String> result;

	/**
	 * Variable, die zusätzlich benötigt wird, um den Farbwechsel zu
	 * realisieren. Hiermit wechselt Methode changeColor() die Farbe. Getrennt
	 * wird damit der Farbwechsel der Buttonreihe für den Steineinwurf und der
	 * Farbwechsel auf dem Spielfeld.
	 */
	int colorStartplayer = 0; // 1 = yellow, 2 = red

	/**
	 * Dieser Eventhändler prüft die Eingabe der Tastatur. Sie ist mit den
	 * Eingabefeldern für die Spielernamen verknüpft und stellt sicher, dass ein
	 * Spielername eingetragen ist.
	 */
	EventHandler<KeyEvent> mouseHandler = new EventHandler<KeyEvent>() {
		@Override
		public void handle(KeyEvent arg0) {

			checkEntries();
		}
	};

	/**
	 * Die Methode initialisiert das KiVs.Player UI. Dabei werden die Tooltips
	 * gesetzt.
	 */
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		GameField.setOpacity(0.8);
		txtfld_PlayerOne.setOnKeyReleased(mouseHandler);
		txtfld_PlayerTwo.setOnKeyReleased(mouseHandler);
		final Tooltip tooltipPunkte = new Tooltip();
		final Tooltip tooltipStandard = new Tooltip();
		final Tooltip tooltipVorsprung = new Tooltip();
		tooltipStandard.setText("Ein Spiel besteht aus 3 Sätzen.\n"
				+ "Man erhält für einen Satzsieg 1 Punkt.\n"
				+ "Unentschieden und Niederlage geben 0 Punkte.\n"
				+ "Im ersten Satz beginnt der Startspieler.\n"
				+ "Im zweiten Satz beginnt der andere Spieler.\n"
				+ "Im dritten Satz beginnt ein zufälliger Spieler.\n");
		btnStandardmodus.setTooltip(tooltipStandard);
		tooltipVorsprung.setText("Ein Spiel besteht aus max. 5 Sätzen.\n"
				+ "Man erhält für einen Satzsieg 1 Punkt.\n"
				+ "Unentschieden und Niederlage geben 0 Punkte.\n"
				+ "Das Spiel ist vorbei, sobald ein Spieler 2\n"
				+ "Punkte Vorpsrung hat oder der 5.Satz zu Ende\n"
				+ "gespielt wurde.\n"
				+ "Der Startspieler ist jeweils routiert jeden Satz.");
		btnVorsprungsmodus.setTooltip(tooltipVorsprung);
		tooltipPunkte.setText("Ein Spiel kann eine unbegrenzte Anzahl.\n"
				+ "an Sätzen haben. Pro Sieg gibt es 2 Punkte.\n"
				+ "Niederlagen geben 1 Punkt und Unentschieden geben 0 Punkte.\n"
				+ "Das Spiel wird über den 'Spiel beenden' Button beendet.\n"
				+ "Der Startspieler ist jeweils routiert jeden Satz.");
		btnPunktemodus.setTooltip(tooltipPunkte);
	}

	/**
	 * Die Methode öffnet den Einstellungsscreen.
	 * 
	 * @param event
	 * @throws IOException
	 */
	public void openSettings(ActionEvent event) throws IOException {
		settingScreen.setVisible(true);
		GameField.setOpacity(0.5);
		btnNewSet.setDisable(true);
		

	}

	/**
	 * Die Methode setzt alle Variablen auf den Anfangswert zurück.
	 */
	private void resetGamefield() {
		if (col1 != null) {
			for (int i = 0; i <= 5; i++) {
				col1[i].setId("empty");
				col2[i].setId("empty");
				col3[i].setId("empty");
				col4[i].setId("empty");
				col5[i].setId("empty");
				col6[i].setId("empty");
				col7[i].setId("empty");
			}
		}
		enablePlayButtons(true);
		int resetManageRows[] = { 5, 5, 5, 5, 5, 5, 5 };
		manageRows = resetManageRows;
		counter = 0;
	}

	/**
	 * Die Methode startet einen neuen Satz innerhalb eines Spiels. Dabei wird
	 * der Startspieler gewechselt oder im Standardmodus im 3. Satz zufällig
	 * ausgewechselt.
	 * 
	 * @param event
	 * @throws IOException
	 */
	public void startNewSet(ActionEvent event) throws IOException {
		resetGamefield();
		setNr++;
		String name;

		counter = 0;

		if (modus == "Standardmodus" && setNr == 2) {
			Random rand = new Random();
			int randomNum = rand.nextInt((2 - 1) + 1) + 1;

			if (selectedStartPlayer == 1 && randomNum == 2) {
				if (colorStartplayer == 2) {
					changeColor();
				}
				selectedStartPlayer = randomNum;
				name = startPlayer;
				startPlayer = opponentPlayer;
				opponentPlayer = name;
				selectedStartPlayer = 2;
			} else if (selectedStartPlayer == 2 && randomNum == 1) {
				if (colorStartplayer == 1) {
					changeColor();
				}
				selectedStartPlayer = randomNum;
				name = startPlayer;
				startPlayer = opponentPlayer;
				opponentPlayer = name;
				selectedStartPlayer = 1;
			}
		} else {
			name = startPlayer;
			startPlayer = opponentPlayer;
			if (selectedStartPlayer == 1) {
				if (colorStartplayer == 2) {
					changeColor();
				}
				selectedStartPlayer = 2;
			} else {
				if (colorStartplayer == 1) {
					changeColor();
				}
				selectedStartPlayer = 1;
			}
			opponentPlayer = name;

		}
		setStartplayerLabel();
		startTwoPlayerM();
	}

	/**
	 * Die Methode setzt den Modus auf den Punktemodus.
	 * 
	 * @param event
	 * @throws IOException
	 */
	public void setPunktemodus(ActionEvent event) throws IOException {
		selectedModus = 1;
		btnStandardmodus.setOpacity(0.5);
		btnVorsprungsmodus.setOpacity(0.5);
		btnPunktemodus.setOpacity(1);
		modus = "Punktemodus";
		isModusSet = true;
	}

	/**
	 * Die Methode setzt den Modus auf den Standardmodus.
	 * 
	 * @param event
	 * @throws IOException
	 */
	public void setStandardmodus(ActionEvent event) throws IOException {
		selectedModus = 2;
		btnStandardmodus.setOpacity(1);
		btnVorsprungsmodus.setOpacity(0.5);
		btnPunktemodus.setOpacity(0.5);
		modus = "Standardmodus";
		isModusSet = true;
		checkEntries();
	}

	/**
	 * Die Methode setzt den Modus auf den Vorsprungsmodus.
	 * 
	 * @param event
	 * @throws IOException
	 */
	public void setVorsprungsmodus(ActionEvent event) throws IOException {
		selectedModus = 3;
		btnStandardmodus.setOpacity(0.5);
		btnVorsprungsmodus.setOpacity(1);
		btnPunktemodus.setOpacity(0.5);
		modus = "Vorsprungsmodus";
		isModusSet = true;
		checkEntries();
	}

	/**
	 * Die Methode setzt den Spieler 1 als Startspieler.
	 * 
	 * @param event
	 * @throws IOException
	 */
	public void setPlayerOne(ActionEvent event) throws IOException {
		colorStartplayer = 1;
		selectedStartPlayer = 1;
		selectedOpponentPlayer = 1;
		btnPlayerTwo.setOpacity(0.5);
		btnRandom.setOpacity(0.5);
		btnPlayerOne.setOpacity(1);
		opponentPlayer = txtfld_PlayerTwo.getText();
		startPlayer = txtfld_PlayerOne.getText();
		isPlayerSet = true;
		checkEntries();

	}

	/**
	 * Die Methode setzt den Spieler 2 als Startspieler.
	 * 
	 * @param event
	 * @throws IOException
	 */
	public void setPlayerTwo(ActionEvent event) throws IOException {
		colorStartplayer = 2;
		selectedStartPlayer = 2;
		selectedOpponentPlayer = 0;
		btnPlayerTwo.setOpacity(1);
		btnRandom.setOpacity(0.5);
		btnPlayerOne.setOpacity(0.5);
		opponentPlayer = txtfld_PlayerOne.getText();
		startPlayer = txtfld_PlayerTwo.getText();
		isPlayerSet = true;
		checkEntries();

	}

	/**
	 * Die Methode setzt einen Spieler anhand einer Zufallszahl als
	 * Startspieler.
	 * 
	 * @param event
	 * @throws IOException
	 */
	public void setRandom(ActionEvent event) throws IOException {
		Random rand = new Random();
		int randomNum = rand.nextInt((2 - 1) + 1) + 1;

		btnPlayerTwo.setOpacity(0.5);
		btnRandom.setOpacity(1);
		btnPlayerOne.setOpacity(0.5);
		isPlayerSet = true;
		checkEntries();

		if (randomNum == 2) {
			opponentPlayer = txtfld_PlayerOne.getText();
			startPlayer = txtfld_PlayerTwo.getText();
			colorStartplayer = 2;
			selectedOpponentPlayer = 0;
			selectedStartPlayer = 2;
		} else {
			startPlayer = txtfld_PlayerOne.getText();
			opponentPlayer = txtfld_PlayerOne.getText();
			colorStartplayer = 1;
			selectedOpponentPlayer = 1;
			selectedStartPlayer = 1;
		}
	}

	/**
	 * Die Methode navigiert den User ins Hauptmenü.
	 * 
	 * @param event
	 * @throws IOException
	 */
	public void startMenu(ActionEvent event) throws IOException {

		MainController.changeScene(event, "/view/Menu.fxml");
	}

	/**
	 * Die Methode beendet ein Spiel, öffnet die Gesamtübersicht eines Spiels
	 * und setzt alle Daten für die Gesamtübersicht.
	 */
	private void gameHasEnded() {
		settingScreen.setVisible(false);
		Gamedetails.setVisible(true);
		btnNewSet.setVisible(false);

		txtfld_sum_mode.setDisable(true);
		txtfld_sum_p1.setDisable(true);
		txtfld_sum_p2.setDisable(true);
		txtfld_sum_score.setDisable(true);

		int draws = 0;
		ArrayList<Table_setDetails> details = new ArrayList<Table_setDetails>();

		Table_setDetails data = null;
		String winner_name;
		int playerOne;
		int playerTwo;

		String playerOneName = game.getSpielerA();
		String playerTwoName = game.getSpielerB();
		int resultA = 0;
		int resultB = 0;
		String nr_Set = null;

		// Gesamtzüge addieren -> draws, Endresult von A & B zusammenrechnen
		// ->resultA/resultB,
		// Satzgewinner-> winner_name => Tabelle schreiben
		for (int i = 0; i < game.saetze.size(); i++) {
			draws = draws + game.saetze.get(i).zuege.size();

			playerOne = game.saetze.get(i).getSpielerAPunkte();
			playerTwo = game.saetze.get(i).getSpielerBPunkte();

			if (playerOne > playerTwo)
				winner_name = playerOneName;
			else if (playerTwo > playerOne)
				winner_name = playerTwoName;
			else
				winner_name = "Unentschieden";

			nr_Set = String.valueOf(game.saetze.get(i).getSatznr());

			data = new Table_setDetails(nr_Set, game.saetze.get(i)
					.getStartspieler(), winner_name, game.saetze.get(i)
					.getSpielerAPunkte()
					+ ":"
					+ game.saetze.get(i).getSpielerBPunkte());

			details.add(data);
		}

		// Tabelle anlegen & setzen
		setNr_table
				.setCellValueFactory(new PropertyValueFactory<Table_setDetails, String>(
						"setNr_table"));
		winner.setCellValueFactory(new PropertyValueFactory<Table_setDetails, String>(
				"winner"));
		startPlayer_table
				.setCellValueFactory(new PropertyValueFactory<Table_setDetails, String>(
						"startPlayer"));
		result.setCellValueFactory(new PropertyValueFactory<Table_setDetails, String>(
				"result"));

		ObservableList<Table_setDetails> table_de = FXCollections
				.observableArrayList(details);

		table_details.setItems(table_de);

		// Felder befüllen
		String drawAll = null;

		drawAll = String.valueOf(draws);
		txtfld_sum_draw.setText(drawAll);

		txtfld_sum_mode.setText(game.getModus());
		txtfld_sum_p1.setText(playerOneName);
		txtfld_sum_p2.setText(playerTwoName);
		txtfld_sum_score.setText(game.getResultPlayerA() + ":"
				+ game.getResultPlayerB());

		// Endsieger
		resultA = resultA + game.getResultPlayerA();
		resultB = resultB + game.getResultPlayerB();

		if (resultA > resultB)
			game.SetWinner(game.getSpielerA());
		else if (resultA < resultB)
			game.SetWinner(game.getSpielerB());
		else
			game.SetWinner("Unentschieden");

		txtfld_winner.setText(game.getWinner());

	}

	/**
	 * Die Methode beendet ein Spiel und löst Methode hameHasEnded() aus.
	 * 
	 * @param event
	 * @throws IOException
	 */
	public void startGameEnd(ActionEvent event) throws IOException {
		gameHasEnded();
		btnStartTwoPlayer.setDisable(true);
	}

	/**
	 * Die Methode startet ein Spiel.
	 * 
	 * @param event
	 * @throws IOException
	 */
	public void startTwoPlayer(ActionEvent event) throws IOException {
		// UI Handling beim Spielstart
		settingScreen.setVisible(false);
		GameField.setOpacity(1);
		disableSettingElements(true);
		label_PlayerOne.setText(txtfld_PlayerOne.getText());
		label_PlayerTwo.setText(txtfld_PlayerTwo.getText());
		btnEnd.setDisable(false);
		btnRandom.setVisible(false);
		btnStartTwoPlayer.setVisible(false);
		btnBack.setVisible(true);
		if (selectedStartPlayer == 1) {
			btnPlayerOne.setOpacity(1);
		} else {
			btnPlayerTwo.setOpacity(1);
		}
		setStartplayerLabel();

		changeColor();
		startTwoPlayerM();

	}

	/**
	 * Diese Methode wechselt den Text zur Anzeige des Startspielers.
	 */
	private void setStartplayerLabel() {
		if (selectedStartPlayer == 1) {
			label_startplayer.setText(txtfld_PlayerOne.getText()
					+ " beginnt diesen Satz!");
		} else {
			label_startplayer.setText(txtfld_PlayerTwo.getText()
					+ " beginnt diesen Satz!");
		}
	}

	/**
	 * Die Methode legt eine neue Spielinstanz an und startet den TwoPlayer
	 * Modus.
	 */
	public void startTwoPlayerM() {
		labelWinnerMsg.setVisible(false);
		btnNewSet.setVisible(false);
		btnNewSet.setDisable(true);


		if (game == null) {
			game = new Spiel(txtfld_PlayerOne.getText(),
					txtfld_PlayerTwo.getText(), modus, level);
		}

		game.addSatz(0, 0, startPlayer);

		label_Score.setText(game.getResultPlayerA() + " : "
				+ game.getResultPlayerB());

		gameField = new GameField();
		this.threatManager = new ThreatManager(gameField);

	}

	/**
	 * Diese Methode ist für die zentrale Steuerung der Buttons und
	 * Eingabefelder auf dem Einstellungsfenster(SettingScreen) zuständig.
	 * Während einem laufenden Spiel wird hiermit verhindert, dass die
	 * Einstellungen geändert werden können.
	 * 
	 * @param disableValue
	 */
	private void disableSettingElements(boolean disableValue) {

		btnPlayerOne.setDisable(disableValue);
		btnPlayerTwo.setDisable(disableValue);
		btnRandom.setDisable(disableValue);
		btnVorsprungsmodus.setDisable(disableValue);
		btnStandardmodus.setDisable(disableValue);
		btnPunktemodus.setDisable(disableValue);
		txtfld_PlayerOne.setDisable(disableValue);
		txtfld_PlayerTwo.setDisable(disableValue);

		if (disableValue == false) {
			btnPlayerOne.setOpacity(1);
			btnPlayerTwo.setOpacity(1);
			btnRandom.setOpacity(1);
			btnVorsprungsmodus.setOpacity(1);
			btnStandardmodus.setOpacity(1);
			btnPunktemodus.setOpacity(1);
		}
	}

	/**
	 * Die Methode setzt die Button auf dem Einstellungscreen zurück.
	 */
	public void resetGameSetting() {
		// Spielfeld UI Handling (SettingScreen sichtbar)
		settingScreen.setVisible(true);
		GameField.setOpacity(0.8);
		Gamedetails.setVisible(false);
		// Reset UI im Setting Screen -> Eingaben für neues Spiel möglich
		// Buttons
		btnEdit.setDisable(false);
		btnEnd.setDisable(true);
		btnBack.setVisible(false);
		btnRandom.setVisible(true);
		btnStartTwoPlayer.setVisible(true);
		btnStartTwoPlayer.setVisible(true);
		btnNewSet.setVisible(false);
		disableSettingElements(false);
		// Label + Textfelder
		label_PlayerOne.setText("Spieler 1");
		label_PlayerTwo.setText("Spieler 2");
		label_Score.setText("");
		txtfld_winner.setDisable(true);
		txtfld_PlayerOne.setText("");
		txtfld_PlayerTwo.setText("");
		labelWinnerMsg.setText("");
		label_startplayer.setText("");
		// Variablen für den Eingabecheck
		isModusSet = false;
		isPlayerSet = false;

		// Reset des Spielfeldes sowie Variablen für den Spielverlauf
		resetGamefield();
		game = null;
		setNr = 0;
	}

	/**
	 * Die Methode verwirft ein Spiel. Der User löscht das grad gespielte Spiel
	 * und gelangt wieder auf den Einstellungsscreen.
	 * 
	 * @param event
	 * @throws IOException
	 */
	public void delete(ActionEvent event) throws IOException {
		resetGameSetting();
	}

	/**
	 * Die Methode speichert ein Spiel in die Datenbank ab.
	 * 
	 * @param event
	 * @throws IOException
	 */
	public void save(ActionEvent event) throws IOException {
		// Gewinner zu der Spielinstanz hinzufügen->notwendig, weil der Nutzer
		// den Gewinner nachträglich noch ändern kann
		game.SetWinner(txtfld_winner.getText());
		try {
			game.persist();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("Speichern fehlgeschlagen!");
		}
		resetGameSetting();
	}

	/**
	 * Die Methode löst den Editier-Modus aus. Der User kann den Gewinner nun
	 * ändern.
	 * 
	 * @param event
	 * @throws IOException
	 */
	public void edit(ActionEvent event) throws IOException {
		txtfld_winner.setDisable(false);
		btnEdit.setDisable(true);
	}

	/**
	 * Die Methode setzt einen Spielwurf durch den User in Spalte 1.
	 * 
	 * @param event
	 */
	public void addCol1(ActionEvent event) {
		setGrid(0);
	}

	/**
	 * Die Methode setzt einen Spielwurf durch den User in Spalte 2.
	 * 
	 * @param event
	 */
	public void addCol2(ActionEvent event) {
		setGrid(1);
	}

	/**
	 * Die Methode setzt einen Spielwurf durch den User in Spalte 3.
	 * 
	 * @param event
	 */
	public void addCol3(ActionEvent event) {
		setGrid(2);
	}

	/**
	 * Die Methode setzt einen Spielwurf durch den User in Spalte 4.
	 * 
	 * @param event
	 */
	public void addCol4(ActionEvent event) {
		setGrid(3);
	}

	/**
	 * Die Methode setzt einen Spielwurf durch den User in Spalte 5.
	 * 
	 * @param event
	 */
	public void addCol5(ActionEvent event) {
		setGrid(4);
	}

	/**
	 * Die Methode setzt einen Spielwurf durch den User in Spalte 6.
	 * 
	 * @param event
	 */
	public void addCol6(ActionEvent event) {
		setGrid(5);
	}

	/**
	 * Die Methode setzt einen Spielwurf durch den User in Spalte 7.
	 * 
	 * @param event
	 */
	public void addCol7(ActionEvent event) {
		setGrid(6);
	}

	/**
	 * Diese Methode macht die Buttons für einen Steinwurf durch den Spieler für
	 * jede Reihe wieder klickbar. Durch eventuell volle Reihen im vorherigen
	 * Spiel können Buttons für bestimmte Spalten ausgegraut sein. Hiermit
	 * werden diese wieder auf den Grundzustand zurückgesetzt. Zusätzlich wird
	 * hier die Eingabe gesperrt, solange der User noch nicht alle Einstellungen
	 * für das Spiel getätigt hat.
	 * 
	 * @param value
	 */
	public void enablePlayButtons(boolean value) {
		if (col1Btn != null) {
			if (value == true) {
				col1Btn.setDisable(false);
				col2Btn.setDisable(false);
				col3Btn.setDisable(false);
				col4Btn.setDisable(false);
				col5Btn.setDisable(false);
				col6Btn.setDisable(false);
				col7Btn.setDisable(false);
			} else {
				col1Btn.setDisable(true);
				col2Btn.setDisable(true);
				col3Btn.setDisable(true);
				col4Btn.setDisable(true);
				col5Btn.setDisable(true);
				col6Btn.setDisable(true);
				col7Btn.setDisable(true);
			}
		}
	}

	/**
	 * Diese Methode ändert die Farbe der Buttons für den Steineinwurf. Sobald
	 * ein Spieler seinen Zug getätigt hat, wird diese Methode aufgerufen und
	 * wechselt die Farben.
	 */
	private void changeColor() {
		Button[] manageButtons = initPlayButtons();
		if (colorStartplayer == 2) {
			for (int i = 0; i <= 6; i++) {
				manageButtons[i].setId("red_throw");
			}
			colorStartplayer = 1;
		} else if (colorStartplayer == 1) {
			for (int i = 0; i <= 6; i++) {
				manageButtons[i].setId("yellow_throw");
			}
			colorStartplayer = 2;
		}
	}

	/**
	 * Die Methode initialisiert Spalte 1.
	 * 
	 * @return manageCols
	 */
	private Pane[] initManageCols1() {
		Pane[] manageCols = { col1_6, col1_5, col1_4, col1_3, col1_2, col1_1 };
		return manageCols;
	}

	/**
	 * Die Methode initialisiert Spalte 2.
	 * 
	 * @return manageCols
	 */
	private Pane[] initManageCols2() {
		Pane[] manageCols = { col2_6, col2_5, col2_4, col2_3, col2_2, col2_1 };
		return manageCols;
	}

	/**
	 * Die Methode initialisiert Spalte 3.
	 * 
	 * @return manageCols
	 */
	private Pane[] initManageCols3() {
		Pane[] manageCols = { col3_6, col3_5, col3_4, col3_3, col3_2, col3_1 };
		return manageCols;
	}

	/**
	 * Die Methode initialisiert Spalte 4.
	 * 
	 * @return manageCols
	 */
	private Pane[] initManageCols4() {
		Pane[] manageCols = { col4_6, col4_5, col4_4, col4_3, col4_2, col4_1 };
		return manageCols;
	}

	/**
	 * Die Methode initialisiert Spalte 5.
	 * 
	 * @return manageCols
	 */
	private Pane[] initManageCols5() {
		Pane[] manageCols = { col5_6, col5_5, col5_4, col5_3, col5_2, col5_1 };
		return manageCols;
	}

	/**
	 * Die Methode initialisiert Spalte 6.
	 * 
	 * @return manageCols
	 */
	private Pane[] initManageCols6() {
		Pane[] manageCols = { col6_6, col6_5, col6_4, col6_3, col6_2, col6_1 };
		return manageCols;
	}

	/**
	 * Die Methode initialisiert Spalte 7.
	 * 
	 * @return manageCols
	 */
	private Pane[] initManageCols7() {
		Pane[] manageCols = { col7_6, col7_5, col7_4, col7_3, col7_2, col7_1 };
		return manageCols;
	}

	/**
	 * Die Methode initialisiert die Spielfeldbuttons,so dass der User anhand
	 * dieser Buttons einen Wurf setzen kann.
	 * 
	 * @return
	 */
	private Button[] initPlayButtons() {
		Button[] manageButtons = { col1Btn, col2Btn, col3Btn, col4Btn, col5Btn,
				col6Btn, col7Btn };
		return manageButtons;
	}

	/**
	 * Die Methode setzt einen Spielstein auf das Spielfeld.
	 * 
	 * @param column
	 */
	public void setGrid(int column) {
		int row = manageRows[column];
		manageRows[column] = manageRows[column] - 1;

		changeColor();

		if (isColInit == false) {
			col1 = initManageCols1();
			col2 = initManageCols2();
			col3 = initManageCols3();
			col4 = initManageCols4();
			col5 = initManageCols5();
			col6 = initManageCols6();
			col7 = initManageCols7();
			isColInit = true;
		}
		if (selectedStartPlayer == 1) {
			// Auswahl der Farben (Rot <-> Geld)
			if (counter % 2 == 0) {
				game.saetze.get(setNr).addZug(startPlayer, column);
				gameField.throwChip(column, 0);

				switch (column) {
				case 0:
					col1[row].setId("yellow");
					break;
				case 1:
					col2[row].setId("yellow");
					break;
				case 2:
					col3[row].setId("yellow");
					break;
				case 3:
					col4[row].setId("yellow");
					break;
				case 4:
					col5[row].setId("yellow");
					break;
				case 5:
					col6[row].setId("yellow");
					break;
				case 6:
					col7[row].setId("yellow");
					break;
				default:
					System.out.println("Fehler");
				}
				// String status = gameField.hasGameEnded(0, column);
				String status = gameField.hasGameEnded(threatManager);

				if (status == "K" || status == "O")
					endSet(0);
				else if (status == "F")
					endSet(-1);
				else
					counter++;
			} else {
				gameField.throwChip(column, 1);
				game.saetze.get(setNr).addZug(opponentPlayer, column);
				switch (column) {
				case 0:
					col1[row].setId("red");
					break;
				case 1:
					col2[row].setId("red");
					break;
				case 2:
					col3[row].setId("red");
					break;
				case 3:
					col4[row].setId("red");
					break;
				case 4:
					col5[row].setId("red");
					break;
				case 5:
					col6[row].setId("red");
					break;
				case 6:
					col7[row].setId("red");
					break;
				default:
					System.out.println("Fehler");
				}
				String status = gameField.hasGameEnded(threatManager);

				if (status == "K" || status == "O")
					endSet(1);
				else if (status == "F")
					endSet(-1);
				else
					counter++;
			}
			if (manageRows[column] == -1) {
				setFullRow(column);
			}
		} else {
			if (counter % 2 != 0) {
				game.saetze.get(setNr).addZug(startPlayer, column);
				gameField.throwChip(column, 0);

				switch (column) {
				case 0:
					col1[row].setId("yellow");
					break;
				case 1:
					col2[row].setId("yellow");
					break;
				case 2:
					col3[row].setId("yellow");
					break;
				case 3:
					col4[row].setId("yellow");
					break;
				case 4:
					col5[row].setId("yellow");
					break;
				case 5:
					col6[row].setId("yellow");
					break;
				case 6:
					col7[row].setId("yellow");
					break;
				default:
					System.out.println("Fehler");
				}

				String status = gameField.hasGameEnded(threatManager);

				if (status == "K" || status == "O")
					endSet(0);
				else if (status == "F")
					endSet(-1);
				else
					counter++;
			} else {
				gameField.throwChip(column, 1);

				game.saetze.get(setNr).addZug(opponentPlayer, column);
				switch (column) {
				case 0:
					col1[row].setId("red");
					break;
				case 1:
					col2[row].setId("red");
					break;
				case 2:
					col3[row].setId("red");
					break;
				case 3:
					col4[row].setId("red");
					break;
				case 4:
					col5[row].setId("red");
					break;
				case 5:
					col6[row].setId("red");
					break;
				case 6:
					col7[row].setId("red");
					break;
				default:
					System.out.println("Fehler");
				}

				String status = gameField.hasGameEnded(threatManager);

				if (status == "K" || status == "O")
					endSet(1);
				else if (status == "F")
					endSet(-1);
				else
					counter++;
			}
			if (manageRows[column] == -1) {
				setFullRow(column);
			}
		}
	}

	/**
	 * Die Methode setzt den Button einer Spalte für den Wurf eines Spielers auf
	 * Disable, wenn die Spalte voll Ist.
	 * 
	 * @param column
	 */
	private void setFullRow(int column) {
		switch (column) {
		case 0:
			col1Btn.setDisable(true);
			break;
		case 1:
			col2Btn.setDisable(true);
			break;
		case 2:
			col3Btn.setDisable(true);
			break;
		case 3:
			col4Btn.setDisable(true);
			break;
		case 4:
			col5Btn.setDisable(true);
			break;
		case 5:
			col6Btn.setDisable(true);
			break;
		case 6:
			col7Btn.setDisable(true);
			break;
		default:
			System.out.println("Fehler");
		}
	}

	/**
	 * Die Methode regelt die Punktevergabe in den Standardmodus. Standardmodus:
	 * Gewinner bekommt einen Punkt, bei Unentschieden = Zufallsentscheidung,
	 * wer der Gewinner ist, Spiellaenge: 3 Saetze
	 * 
	 * @param winner
	 */
	public void setPointsStandardmodus(int winner) {
		int pointsA = 0;
		int pointsB = 0;

		if (winner == 0) {
			Random rand = new Random();
			int randomNum = rand.nextInt((2 - 1) + 1) + 1;

			winner = randomNum;
		} else if (winner == 1) {
			pointsA = 1;
			pointsB = 0;
		} else if (winner == 2) {
			pointsA = 0;
			pointsB = 1;
		}

		game.saetze.get(setNr).changePoints(pointsA, pointsB);

		if (setNr == 2)
			gameHasEnded();
	}

	/**
	 * Die Methode machte die Punktevergabe im Vorsprungsmodus. Punkte: Gewinner
	 * = 1, Remis = 0, ab Satz 4 Remis = Zufallsentscheidung wer Gewinner ist,
	 * Spiellaenge: wenn einer der Spieler 2 Punkte Vorsprung hat
	 * 
	 * @param winner
	 */
	public void setPointsVorsprungsmodus(int winner) {
		int pointsA = 0;
		int pointsB = 0;
		
		if (setNr >= 4) {
			if (winner == 0) {	

				if (winner == 1) {
					pointsA = 1;
					pointsB = 0;

				} else if (winner == 2) {
					pointsA = 0;
					pointsB = 1;
				}
			} else if (winner == 1) {
				pointsA = 1;
				pointsB = 0;
			} else if (winner == 2) {
				pointsA = 0;
				pointsB = 1;
			}
		} else {
			if (winner == 1) {
				pointsA = 1;
				pointsB = 0;
			} else if (winner == 2) {
				pointsA = 0;
				pointsB = 1;
			} else if (winner == 0) {
				pointsA = 0;
				pointsB = 0;
			}
		}
		game.saetze.get(setNr).changePoints(pointsA, pointsB);

		int resultA = 0;
		int resultB = 0;

		for (int i = 0; i < game.saetze.size(); i++) {
			resultA = resultA + game.saetze.get(i).getSpielerAPunkte();
			resultB = resultB + game.saetze.get(i).getSpielerBPunkte();
		}

		if (resultA == resultB + 2) {
			gameHasEnded();
			game.SetWinner(game.getSpielerA());
		}

		if ((resultA + 2) == resultB) {
			gameHasEnded();
			game.SetWinner(game.getSpielerB());
		}

		if (setNr == 4) {
			if (resultA > resultB) {
				game.SetWinner(game.getSpielerA());
			} else {
				game.SetWinner(game.getSpielerB());
			}
			gameHasEnded();
		}
	}

	/**
	 * Die Methode setzt die Punktevergabe im Punktemodus. Punkte: Gewinner = 2
	 * Unentschieden = 1, keine feste Spiellaenge
	 * 
	 * @param winner
	 */
	public void setPointsPunktemodus(int winner) {
		int pointsA = 0;
		int pointsB = 0;

		if (winner == 1) {
			pointsA = 2;
			pointsB = 0;
		} else if (winner == 2) {
			pointsA = 0;
			pointsB = 2;
		} else if (winner == 0) {
			pointsA = 1;
			pointsB = 1;
		}
		game.saetze.get(setNr).changePoints(pointsA, pointsB);
	}

	/**
	 * Die Methode überprüft ob der User alle nötigen Einstellungen getroffen
	 * hat.
	 */
	private void checkEntries() {

		if (isPlayerSet == true && isModusSet == true
				&& !(txtfld_PlayerOne.getText().equals(""))
				&& !(txtfld_PlayerTwo.getText().equals(""))) {
			btnStartTwoPlayer.setDisable(false);
		} else {
			btnStartTwoPlayer.setDisable(true);
		}

	}

	/**
	 * Die Methode wird aufgerufen, wenn ein Satz zu Ende ist. Sie löst die
	 * Punktevergabe je nach Gewinner und Modus aus.
	 * 
	 * @param message
	 */
	public void endSet(int team) {
		enablePlayButtons(false);
		labelWinnerMsg.setVisible(false);

		btnNewSet.setDisable(false);
		btnNewSet.setVisible(true);
	
		if (team == 0) {
			labelWinnerMsg.setText(txtfld_PlayerOne.getText()
					+ " hat gewonnen!");
			labelWinnerMsg.setVisible(true);

			
				if (modus == "Standardmodus")
					setPointsStandardmodus(1);
				else if (modus == "Punktemodus")
					setPointsPunktemodus(1);
				else if (modus == "Vorsprungsmodus")
					setPointsVorsprungsmodus(1);
			
			
		} else if (team == 1) {
			labelWinnerMsg.setText(txtfld_PlayerTwo.getText()
					+ " hat gewonnen!");
			labelWinnerMsg.setVisible(true);

				if (modus == "Standardmodus")
					setPointsStandardmodus(2);
				else if (modus == "Punktemodus")
					setPointsPunktemodus(2);
				else if (modus == "Vorsprungsmodus")
					setPointsVorsprungsmodus(2);
		} else if (team == -1) {
			labelWinnerMsg.setText("Unentschieden!");
			labelWinnerMsg.setVisible(true);
			if (modus == "Standardmodus")
				setPointsStandardmodus(0);
			else if (modus == "Punktemodus")
				setPointsPunktemodus(0);
			else if (modus == "Vorsprungsmodus")
				setPointsVorsprungsmodus(0);
		}

		label_Score.setText(game.getResultPlayerA() + " : "
				+ game.getResultPlayerB());
	}

	/**
	 * Die Methode schließt den Einstellungsscreen.
	 * 
	 * @param event
	 * @throws IOException
	 */
	public void closeSettingScreen(ActionEvent event) throws IOException {
		settingScreen.setVisible(false);
		GameField.setOpacity(1);
		btnNewSet.setDisable(false);
	}
}
