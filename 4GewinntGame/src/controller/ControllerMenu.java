/**
 * 
 */
package controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;

/**
 * 
 * Die Klasse kümmert sich um die Steuerung des Hauptmenüs. Der User hat vier
 * Möglichkeiten. Er kann in den Turnier, TwoPlayer oder KiVsPlayer Modus gehen
 * oder kann sich in Spielstände, die bereits gespielten Spiele ansehen.
 * 
 * @author MTGames
 * @Mitglieder Timo Strauch, Tim Holm, Meike Helwig, Marcel Schäfer
 * 
 * 
 *
 */
public class ControllerMenu implements Initializable {

	/**
	 * Der Button löst startTournament() aus. Der User gelangt so in den
	 * Turniermodus.
	 */
	@FXML
	Button btnTourn;

	/**
	 * Der Button löst startTwoPlayer() aus. Der User gelangt so in den
	 * TwoPlayermodus.
	 */
	@FXML
	Button btnTwoPlayer;

	/**
	 * Der Button löst startKI() aus. Der User gelangt so in den
	 * KiVsPlayermodus.
	 */
	@FXML
	Button btnKI;

	/**
	 * Der Button löst startHistory() aus. Der User gelangt so in den
	 * Historymodus.
	 */
	@FXML
	Button btnHistory;

	/**
	 * Die Methode initialisiert das Hauptmenü.
	 */
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {

	}

	/**
	 * Die Methode ruft die Navigation zum Turniermodus auf.
	 * 
	 * @param event
	 * @throws IOException
	 */
	public void startTournament(ActionEvent event) throws IOException {
		MainController.changeScene(event, "/view/TournamentGamefield.fxml");

	}

	/**
	 * Die Methode ruft die Navigation zum TwoPlayermodus auf.
	 * 
	 * @param event
	 * @throws IOException
	 */
	public void startTwoPlayer(ActionEvent event) throws IOException {
		MainController.changeScene(event, "/view/TwoPlayerGamefield.fxml");
	}

	/**
	 * Die Methode ruft die Navigation zum KiVsPlayermodus auf.
	 * 
	 * @param event
	 * @throws IOException
	 */
	public void startKI(ActionEvent event) throws IOException {
		MainController.changeScene(event, "/view/KIGamefield.fxml");
	}

	/**
	 * Die Methode ruft die Navigation zu den bereits gespielten Spielen auf.
	 * @param event
	 * @throws IOException
	 */
	public void startHistory(ActionEvent event) throws IOException {
		MainController.changeScene(event, "/view/HistoryGamefield.fxml");
	}

}
