package controller;

/**
 * 
 * 
 * Die Klasse dient zur Organisation einer doppelt-verkettetet Liste dar. In der
 * Liste werden alle  Züge zu einem ausgewählten Spiel gespeichert. Zu der Liste
 * können Elemente hinzugefügt werden und Elemente anhand der Zugnummer und
 * Satznummer gefunden werden.
 * 
 * @author MTGames
 * @Mitglieder Timo Strauch, Tim Holm, Meike Helwig, Marcel Schäfer
 * 
 * 
 *
 */
public class LinkedListDraw {

	/**
	 * StartElement der doppelt-verketteten Liste
	 */
	Position startElem = new Position(null, 0, -1);

	/**
	 * Die Methode fügt ein Objekt an die Liste hinzu.
	 * 
	 * @param o
	 */
	public void addLast(Object o) {

		Position newElem = new Position(o);
		Position lastElem = getLastElem();
		lastElem.setNextElem(newElem);
		newElem.setPrevElem(lastElem);
	}

	/**
	 * Die Methode liefert ein Objekt zurück, dass anhand der Zugnummer und der
	 * Satznummer identifiziert wird.
	 * 
	 * @param drawNr
	 * @param setNr
	 * @return gesuchtes Objekt
	 */
	public Position find(int drawNr, int setNr) {
		Position le = startElem.getNextElem();
		boolean find = false;

		while (find == false) {
			if (le.getDrawNr() == drawNr && le.getSet() == setNr) {
				find = true;
			} else {
				le = le.getNextElem();
			}
		}
		if (find == true)
			return le;
		else
			return null;
	}

	/**
	 * Die Methode liefert das erste Element der Liste zurück.
	 * 
	 * @return startElem
	 */
	public Position getFirstElem() {
		return startElem;
	}

	/**
	 * Die Methode liefert das letzte Element der Liste zurück.
	 * 
	 * @return letztes Element
	 */
	public Position getLastElem() {
		Position le = startElem;
		while (le.getNextElem() != null) {
			le = le.getNextElem();
		}
		return le;
	}
}
