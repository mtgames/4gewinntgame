package controller;

import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import model.ki.KIGameMaster;
import model.server.ServerCommunicatorMonitor;

/**
 * 
 * Die Klasse beinhaltet Methoden, die von mehreren Controllern benutzt werden.
 * Sie kümmert sich um die Navigation zwischen FXML-Dateien, initialisiert die
 * Server Kommunikation, ebenso die KI für den Turniermodus und KiVsPlayermodus und
 * deinitialisiert sie auch.
 * 
 * @author MTGames
 * @Mitglieder Timo Strauch, Tim Holm, Meike Helwig, Marcel Schäfer
 * 
 *
 */

public class MainController {

	/**
	 * Der gameMaster stellt die künstliche Intelligenz dar.
	 */
	public static KIGameMaster gameMaster;

	/**
	 * Die Variable dient der Server Kommunikation mit der KI.
	 */
	public static ServerCommunicatorMonitor serverComm;

	/**
	 * Die Variable speichert den Controller, der grad aktuellen Oberfläche.
	 */
	public static ControllerGameFieldKIvsPlayer controllerGamefield;

	/**
	 * Die Methode initialisiert den Turniermodus.
	 * 
	 * @param gfc
	 */
	public static void initTournamentClasses(ControllerGameFieldTournament gfc) {
		MainController.serverComm = new ServerCommunicatorMonitor();
		MainController.gameMaster = new KIGameMaster(MainController.serverComm);
		MainController.gameMaster.addObserver(MainController.serverComm);
		MainController.gameMaster.addObserver(gfc);
	}

	/**
	 * Die Methode deinitializiert den Turniermodus. Dabei werden alle Threads
	 * getötet.
	 */
	public static void deinitializeTournamentClasses() {
		MainController.serverComm.killAllThreads();
	}

	/**
	 * Die Methode deinitializiert den KIvsPlayermodus. Dabei werden alle
	 * Threads getötet.
	 */
	public static void deinitializePlayerVsKIClasses() {
		MainController.controllerGamefield.killAllThreads();
	}

	/**
	 * Die Methode initialisiert den KiVsPlayermodus.
	 * 
	 * @param gfc
	 */
	public static void initPlayerVsKiClasses(ControllerGameFieldKIvsPlayer gfc) {
		MainController.controllerGamefield = gfc;
		MainController.gameMaster = new KIGameMaster(gfc);
		MainController.gameMaster.addObserver(gfc);
	}

	/**
	 * Die Methode kümmert sich um die Navigation zwischen den Views.
	 * 
	 * @param event
	 * @param view
	 * @throws IOException
	 */
	public static void changeScene(ActionEvent event, String view)
			throws IOException {
		Node node = (Node) event.getSource();
		Stage stage = (Stage) node.getScene().getWindow();
		Parent root = FXMLLoader.load(MainController.class.getResource(view));
		Scene scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}
}
