/**
 * 
 */
package controller;

import model.db.Zug;

/**
 * 
 * Die Klasse Position wird für die verkettete Liste benötigt. Ein Objekt der
 * Klasse Position besitzt eine Objektreferenz, eine Referenz auf das nächste
 * Objekt und das vorherige Objekt in der verketteten Liste, ebenso enthält es
 * einen Zug, eine Satznummer und eine ID.
 * 
 * @author MTGames
 * @Mitglieder Timo Strauch, Tim Holm, Meike Helwig, Marcel Schäfer
 * 
 */
public class Position {
	/**
	 * Objektreferenz, wird für die Methode addLast in der doppelt-verketteten
	 * Liste benötigt.
	 */
	private Object obj;

	/**
	 * Referenz auf das nächste Element, dass auf das Objekt in der Liste folgt.
	 */
	private Position nextElem;

	/**
	 * Referenz auf das vorherige Element, dass vor dem Objekt in der Liste
	 * steht.
	 */
	private Position prevElem;

	/**
	 * Ein Objekt der Klasse Zug. Enthält alle Daten zu dem Zug (Nummer, Spalte,
	 * Werfer).
	 */
	private Zug draw;

	/**
	 * Satznummer, in dem der Zug stattgefunden hat
	 */
	private int set;

	/**
	 * Id des Objektes. Wird bei Realisierung der Simulation auf dem UI der
	 * bereits gespielten Spiele benötigt.
	 */
	private int id;

	/**
	 * Konstruktor
	 * 
	 * @param draw
	 * @param set
	 * @param id
	 */
	public Position(Zug draw, int set, int id) {
		this.draw = draw;
		this.set = set;
		this.id = id;

	}

	/**
	 * Konstruktor wird von der verketteten Liste benutzt
	 * 
	 * @param o
	 */
	public Position(Object o) {
		this.obj = o;
		this.draw = ((Position) o).getDraw();
		this.set = ((Position) o).getSet();
		this.id = ((Position) o).getId();

		nextElem = null;
	}

	/**
	 * Die Methode setzt die Objektreferenz für das nächste Element in der
	 * Liste.
	 * 
	 * @param nextElem
	 */
	public void setNextElem(Position nextElem) {
		this.nextElem = nextElem;
	}

	/**
	 * Die Methode setzt die Objektreferenz für das vorherige Element in der
	 * Liste.
	 * 
	 * @param prevElem
	 */
	public void setPrevElem(Position prevElem) {
		this.prevElem = prevElem;
	}

	/**
	 * Die Methode gibt die Objektreferenz des nächsten Elements der Liste
	 * zurück.
	 * 
	 * @return nextElem
	 */
	public Position getNextElem() {
		return nextElem;
	}

	/**
	 * Die Methode gibt die Objektreferenz des vorherigen Elements zurück.
	 * 
	 * @return prevElem
	 */
	public Position getPrevElem() {
		return this.prevElem;
	}

	/**
	 * Die Methode gibt die Objektreferenz des Objektes zurück.
	 * 
	 * @return obj
	 */
	public Object getObj() {
		return obj;
	}

	/**
	 * Die Methode gibt den Zug zurück.
	 * 
	 * @return draw
	 */
	public Zug getDraw() {

		return draw;
	}

	/**
	 * Die Methode gibt die Spalte zurück, in die der Werfer in diesem Zug
	 * geworfen hat.
	 * 
	 * @return Spalte
	 */
	public int getColumn() {

		return draw.getSpalte();
	}

	/**
	 * Die Methode gibt die Satznummer zurück.
	 * 
	 * @return set
	 */
	public int getSet() {

		return set;
	}

	/**
	 * Die Methode gibt die Zugnummer zurück.
	 * 
	 * @return Zugnummer
	 */
	public int getDrawNr() {

		return draw.getZug_nr();

	}

	/**
	 * Die Methode gibt die Id des Objektes zurück.
	 * 
	 * @return id
	 */
	public int getId() {
		return id;
	}

}
