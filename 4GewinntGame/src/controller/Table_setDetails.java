
package controller;

import javafx.beans.property.SimpleStringProperty;

/**
 * 
 * Die Klasse dient zum Anlegen der Tabelle mit den Satz-Details auf der
 * Gesamtübersicht eines Spiels.
 * 
 * @author MTGames
 * @Mitglieder Timo Strauch, Tim Holm, Meike Helwig, Marcel Schäfer
 * 
 * 
 */
public class Table_setDetails {

	/**
	 * Tabellenspalte Satznummer
	 */
	private SimpleStringProperty setNr_table;

	/**
	 * Tabellenspalte Satzspieler
	 */
	private final SimpleStringProperty startPlayer;

	/**
	 * Tabellenspalte Gewinner
	 */
	private final SimpleStringProperty winner;

	/**
	 * Tabellenspalte Zwischenergebnis
	 */
	private final SimpleStringProperty result;

	/**
	 * Konstruktor der Tabelle
	 * 
	 * @param table_setNr
	 * @param table_startPlayer
	 * @param table_winner
	 * @param table_result
	 */
	public Table_setDetails(String table_setNr, String table_startPlayer,
			String table_winner, String table_result) {
		this.setNr_table = new SimpleStringProperty(table_setNr);
		this.startPlayer = new SimpleStringProperty(table_startPlayer);
		this.winner = new SimpleStringProperty(table_winner);
		this.result = new SimpleStringProperty(table_result);
	}

	/**
	 * Die Methode holt den Wert von der Spalte Satznummer.
	 * 
	 * @return setNr_table.get();
	 */
	public String getSetNr_table() {
		return setNr_table.get();
	}

	/**
	 * Die Methode setzt den Wert in der Spalte Satznummer.
	 * 
	 * @param table_setNr
	 */
	public void setSetNr_table(String table_setNr) {
		setNr_table.set(table_setNr);
	}

	/**
	 * Die Methode holt den Wert von der Spalte Startspieler.
	 * 
	 * @return startPlayer.get();
	 */
	public String getStartPlayer() {
		return startPlayer.get();
	}

	/**
	 * Die Methode setzt den Wert in der Spalte Startspieler.
	 * 
	 * @param startPlayer
	 */
	public void setStartPlayer(String table_startPlayer) {
		startPlayer.set(table_startPlayer);
	}

	/**
	 * Die Methode holt den Wert von der Spalte Gewinner.
	 * 
	 * @return winner.get();
	 */
	public String getWinner() {
		return winner.get();
	}

	/**
	 * Die Methode setzt den Wert in der Spalte Gewinner.
	 * 
	 * @param winner
	 */
	public void setWinner(String table_winner) {
		winner.set(table_winner);
	}

	/**
	 * Die Methode holt den Wert von der Spalte Ergebnis.
	 * 
	 * @return result.get();
	 */
	public String getResult() {
		return result.get();
	}

	/**
	 * Die Methode setzt den Wert in der Spalte Ergebnis.
	 * 
	 * @param result
	 */
	public void setResult(String table_result) {
		result.set(table_result);
	}

}
