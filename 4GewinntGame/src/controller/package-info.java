/**
* 
* Im Paket Controller befinden sich alle Controllerklassen, die sich um die Verbindung zwischen den einzelnen Views und dem Model kümmern.
* Der Kontroller {@link controller.ControllerMenu} kümmert sich um die Steuerung des Hauptmenüs.
* Die Klasse {@link controller.ControllerGameFieldHistory} steuert die Anzeige der bereits gespielten Spiele. 
* Die Klasse {@link controller.ControllerGameFieldKIvsPlayer} kümmert sich um die Steuerung des KivsPlayer-Modus.
* Die Klasse {@link controller.ControllerGameFieldTournament} steuert den Turniermodus. 
* Die Klasse {@link controller.ControllerGameFieldTwoPlayer} kümmert sich um die Steuerung des TwoPlayerModus.
* Die Klasse {@link controller.MainController} beinhaltet Methoden, die von allen Controllern benutzt werden. 
*
* Die Klassen {@link controller.LinkedListDraw} und {@link controller.Position} dienen der Erstellung einer doppelt-verketteten Liste, 
* in die ein bereits gespieltes Spiel abgespeichert wird und zur Simulation benutzt wird.
* 
* Die Klasse {@link controller.Table_setDetails} dient als Hilfsklasse zur Erstellung einer Tabelle auf den Views.
*/
package controller;