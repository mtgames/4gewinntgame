/**
* Das Paket images enthält alle Bilder, die in {@link view} für die Darstellung des User Interfaces verwendet werden.
* Mit Hilfe der {@link view.application.css} Datei LINK werden die Bilder einzelnen UI-Elementen zugeordnet (z.B. Buttons, Panes). 
* Für die Strukturierung wurde folgendes Muster angewandt: 
* 
* Verwendungsort_(UI-Element)_Beschreibung
* 
* Verwendungsorte: 
* -gamefield:		Das allgemeine Spielfeld, dass in den drei verschiedenen Spielmodi(Tournament, KI-Mode, TwoPlayer-Mode) verwenet wird.
* -gameSum:			Zusammenfassungsanzeige, die nach dem Spiel ausgefahren wird. 
* -gameSumHistory:	Anzeige mit Details zum Spiel, wenn ein Spiel in der historischen Ansicht gespeicherter Spiele.
* -history:			Hintergrund und Spielfeld in der historischen Anzeige/ Spielsimulation von gespeicherten Spielen.
* -menu:			Der erste Bildschirm, der aufgerufen wird, wenn das Programm gestartet wird. Hier findet die Spielauswahl statt.
* -slideIn:			Fenster mit Einstellungen bevor das Spiel gestartet werden kann. Hier drunter fallen alle Bilder für die Einstellungsfenster aller drei Spielarten.
*
*/
package images;