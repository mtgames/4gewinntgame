package model.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * 
 * Diese Klasse agiert im Hintergrund als Verbindung zur Datenbank und besitzt nur protected und private Methoden,
 * sodass diese Klasse nur innerhalb des Packetes verwendet werden kann. Diese Klasse kapselt alle Methoden, 
 * die zum Schreiben und Lesen erforderlich sind.
 * 
 * @author MTGames
 * @Mitglieder Timo Strauch, Tim Holm, Meike Helwig, Marcel Schäfer
 *
 */

public class DBConnector {
	/**
	 * speichert die Verbindung zur Datenbank ab
	 */
	private static Connection conn;
	/**
	 * speichert die Instanz dieser Klasse, da es nur eine Instanz dieser Klasse geben soll.
	 */
	private static DBConnector Instanz;
	
	/**
	 * Diese Methode gibt die Instanz dieser Klasse zurück und legt sie, falls noch nicht vorhanden, an.
	 * @return Instanz dieser Klasse
	 * @throws Exception
	 */
	protected static DBConnector getInstanz() throws Exception{
		if(Instanz == null){
			Instanz = new DBConnector();
			Instanz.doServerConn();
			Instanz.initDB();
		}
		return Instanz;
	}
	
	/**
	 * Diese Methode schreibt ein gesamtes Spielobjekt auf die Datenbank. Es muss nur ein Spielobjekt übergeben werden. 
	 * Zurückgegeben wird die ID des angelegten Spielobjektes.
	 * @param spiel
	 * @return ID des hinzugefügten Spieles
	 * @throws SQLException
	 */
	protected int persist (Spiel spiel) throws SQLException{
		int spiel_id = -1;
		
		spiel_id = add_spiel(spiel.getId(), spiel.getSpielerA(), spiel.getSpielerB(), spiel.getWinner(), spiel.getModus(), spiel.getLevel());
		
		for (Satz satz : spiel.saetze) {
			try {
				add_satz(spiel_id, satz.getSatznr() , satz.getSpielerAPunkte(), satz.getSpielerBPunkte(), satz.getStartspieler());
			} catch (SQLException e) { //duplicate entries run in here
			}
			
			for (Zug zug : satz.zuege) {
				try {
					add_zug(spiel_id, satz.getSatznr(), zug.getZug_nr(), zug.getSpalte(), zug.getSpieler());
				} catch (SQLException e) { //duplicate entries run in here
				}
			}
		}
		return spiel_id;
	}
	
	/**
	 * Diese Methode fügt in die Datenbank ein neues Spiel hinzu und gibt die ID des neu angelegten Spieles zurück.
	 * @param spiel_id
	 * @param spielerA
	 * @param spielerB
	 * @param sieger
	 * @param modus
	 * @param level
	 * @return ID des zugefügten Spieles
	 * @throws SQLException
	 */
	private int add_spiel(int spiel_id, String spielerA, String spielerB, String sieger, String modus, String level) throws SQLException{
		if(spiel_id == -1){
			this.execute("INSERT INTO spiel(spielera,spielerb,sieger,modus,level) VALUES('"+spielerA+"','"+spielerB+"','"+sieger+"','"+modus+"','"+level+"')");
			return this.getLatestid("spiel","id");
		}	 	
	 	return spiel_id;
	}

	// Fügt einen Satz der Datenbank hinzu
	/**
	 * Diese Methode fügt einen Satz in die Datenbank zum Spiel hinzu.
	 * @param spiel_id
	 * @param satz_nr
	 * @param spielerapunkte
	 * @param spielerbpunkte
	 * @param startspieler
	 * @throws SQLException
	 */
	private void add_satz(int spiel_id, int satz_nr, int spielerapunkte,int spielerbpunkte, String startspieler) throws SQLException{
		this.execute(
			"INSERT INTO satz(spiel_id, satz_nr, spielerapunkte, spielerbpunkte, startspieler) "
			+ "VALUES('"+spiel_id+"','"+satz_nr+"','"+spielerapunkte+"','"+spielerbpunkte+"','"+startspieler+"')");
	}
	
	// Fügt einen Zug der Datenbank hinzu
	/**
	 * Diese Methode fügt einen Zug zum Spielobjekt und Satzobjekt hinzu.
	 * @param spiel_id
	 * @param satz_nr
	 * @param zug_nr
	 * @param spalte
	 * @param spieler
	 * @throws SQLException
	 */
	private void add_zug(int spiel_id, int satz_nr,int zug_nr, int spalte, String spieler) throws SQLException{
		this.execute(
			"INSERT INTO zug(spiel_id,satz_nr,zug_nr,spalte,spieler) VALUES('"+spiel_id+"','"+satz_nr+"','"+zug_nr+"','"+spalte+"','"+spieler+"')");
	}

	/**
	 * Diese Methode aktualisiert den Gewinner eines bereits persistierten Spieles
	 * @param spiel
	 * @throws SQLException
	 */
	protected void persistNewWinner(Spiel spiel) throws SQLException{
		this.execute("UPDATE spiel set sieger = '"+spiel.getWinner()+"' WHERE id = '"+spiel.getId()+"'");
	}
	
	
	// Liest alle Spiele mit den zugehörigen Sätzen und Zügen aus der Datenbank aus
	// und gibt diese in einer Arraylist zurück
	
	/**
	 * Liefert alle Spiele mit den zugehörigen Sätzen und Zügen der Datenbank zurück.
	 * @return alle Spiele in einer ArrayList<Spiel>
	 */
	protected ArrayList<Spiel> getAllGames(){
		  
		  ResultSet rsSpiel = null;
		  ResultSet rsSatz = null;
		  ResultSet rsZug = null;
		  ArrayList<Spiel> spiele = new ArrayList<Spiel>();
		  try {			  
			  rsSpiel = this.query("SELECT * FROM SPIEL");
		
	          while(rsSpiel.next()) {
	        	  int spiel_id = Integer.parseInt(rsSpiel.getObject(1).toString());
	        	  Spiel spiel = new Spiel(spiel_id, rsSpiel.getObject(2).toString(), rsSpiel.getObject(3).toString(), 
	        			  rsSpiel.getObject(4).toString(),rsSpiel.getObject(5).toString(),rsSpiel.getObject(6).toString());
	        	     
	        	  rsSatz = this.query("SELECT * FROM SATZ WHERE spiel_id = '"+ spiel_id+"'");
	        	  
	        	  while(rsSatz.next()){
	        		  Satz satz = spiel.addSatz(Integer.parseInt(rsSatz.getObject(3).toString()), Integer.parseInt(rsSatz.getObject(4).toString()), rsSatz.getObject(5).toString());
	        		  
	        		  rsZug = this.query("SELECT * FROM ZUG WHERE spiel_id = '"+spiel_id+"' AND satz_nr = '"+satz.getSatznr()+"'");
	        		  
	        		  while(rsZug.next()){
	        			  satz.addZug(rsZug.getObject(5).toString(), Integer.parseInt(rsZug.getObject(4).toString()));
	        		  }
	        		  
	        	  }
	        	  
	        	  spiele.add(spiel);
	        	  
	          }
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		  return spiele;
	  }
	  
	  // führt SQL Befehle aus. In diesem Fall benutzt für Insert und Update
	 /**
	 * Diese Methode führt SQL Befehle aus. In dieser Klasse wird sie für Inserts und Updates verwendet.
	 * @param expression
	 * @throws SQLException
	 */
      private synchronized void execute(String expression) throws SQLException {

          Statement st = null;

          st = conn.createStatement();    // statements

          int i = st.executeUpdate(expression);    // run the query

          if (i == -1) {
              System.out.println("db error : " + expression);
          }

          st.close();
      }    


      // wird verwendet um Selects aus der Datenbank auszulesen
      /**
       * Führt Selects auf der Datenbank aus und liefert die Daten in einem ResultSet Objekt zurück
       * @param expression
       * @return ResultSet (Daten der DB)
       */
      protected synchronized ResultSet query(String expression) {

          Statement st = null;
          ResultSet rs = null;
          try {
              st = conn.createStatement();         
              rs = st.executeQuery(expression);    // run the query
              st.close();  
          } catch (Exception e) {
			System.out.println("Something went wrong executing query");
          }
          
          return rs;
      }
      
      // Gibt die letzte ID die in die Datenbank, Tabelle Spiel, eingefügt wurde
      /**
       * Gibt die letzte ID, die in die Tabelle Spiel eingefügt wurde zurück
       * @param table
       * @param latestfrom
       * @return letzte ID der Spiel Tabelle
       * @throws SQLException
       */
      private synchronized int getLatestid(String table,String latestfrom) throws SQLException{
          Statement st = null;
          ResultSet rs = null;
          st = conn.createStatement(); 
          rs = st.executeQuery("SELECT MAX(ID) FROM SPIEL");  
          rs.next();
          int lastid = Integer.parseInt(rs.getObject(1).toString());
          st.close();    
          return lastid;
      }
      
      /**
       * Initialisiert die Datenbank und schreibt die initiale Struktur
       * @throws SQLException
       */
	  private void initDB() throws SQLException{
			execute("CREATE TABLE IF NOT EXISTS spiel ( id INTEGER IDENTITY, spielera VARCHAR(256),spielerb VARCHAR(256), sieger VARCHAR(256), modus VARCHAR(256), level VARCHAR(256))");
			execute("CREATE TABLE IF NOT EXISTS satz ( spiel_id INTEGER, satz_nr INTEGER, spielerapunkte INTEGER, spielerbpunkte INTEGER, startspieler VARCHAR(256), PRIMARY KEY(spiel_id, satz_nr))");
			execute("CREATE TABLE IF NOT EXISTS zug ( spiel_id INTEGER, satz_nr INTEGER, zug_nr INTEGER, spalte INTEGER, spieler VARCHAR(256), PRIMARY KEY(spiel_id, satz_nr, zug_nr))");
	  }
 
	  /**
	   * Öffnet eine Datenbankverbindung, wenn diese noch nicht gestartet wurde.
	   * @throws Exception
	   */
	  private void doServerConn() throws Exception{
		  if(conn == null){
			  Class.forName("org.hsqldb.jdbcDriver");
			  conn = DriverManager.getConnection("jdbc:hsqldb:dbDaten/history","sa","");
		  }
	  }
	  

}
