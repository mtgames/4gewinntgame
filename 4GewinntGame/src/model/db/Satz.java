package model.db;

import java.util.ArrayList;

/**
 * 
 * Repräsentiert den Satz eines Spieles. Angelegt wird dieser über ein Spielobjekt.addSatz(...)
 * Züge können zum Satz mit der Methode addZug angelegt werden.
 * 
 * @author MTGames
 * @Mitglieder Timo Strauch, Tim Holm, Meike Helwig, Marcel Schäfer
 *
 */

public class Satz {
	/**
	 * Speichert den Namen von Spieler A
	 */
	private int spielerAPunkte;
	
	/**
	 * Speichert der den Namen von Spieler B
	 */
	private int spielerBPunkte;
	
	/**
	 * speichert die Nummer des Satzes im Spiel
	 */
	private int satznr;
	
	/**
	 * Speichert die ID für den nächsten Zug
	 */
	private int nextzugnr = 1;
	
	/**
	 * Speichert den Startspieler
	 */
	private String startspieler;
	
	/**
	 * Speichert alle Züge zu einem Satz
	 */
	public ArrayList<Zug> zuege;
	
	/**
	 * Der Konstruktur für das Satzobjekt ist protected und wird von der Klasse addSatz(...) aufgerufen
	 * @param spielerAPunkte
	 * @param spielerBPunkte
	 * @param startspieler
	 * @param satz_nr
	 */
	protected Satz(int spielerAPunkte, int spielerBPunkte, String startspieler, int satz_nr){
		this.spielerAPunkte = spielerAPunkte;
		this.spielerBPunkte = spielerBPunkte;
		this.startspieler = startspieler;
		this.satznr = satz_nr;
		zuege = new ArrayList<Zug>();
	}
	
	// Fügt einen Zug zum Satz hinzu
	/**
	 * Diese Methode fügt einen Zug zum Satzobjekt hinzu und gibt diesen dann zurück
	 * @param spieler
	 * @param spalte
	 * @return neuer Zug
	 */
	public Zug addZug(String spieler,int spalte){
		Zug neu = new Zug(nextzugnr, spieler, spalte);
		zuege.add(neu);
		nextzugnr++;
		return neu;
	}
	
	// Getter
	/**
	 * Gibt die Punkte des Spielers A zurück des aktuellen Satzes
	 */
	public int getSpielerAPunkte() {
		return spielerAPunkte;
	}

	/**
	 * Gibt die Punkte des Spielers B zurück des aktuellen Satzes
	 */
	public int getSpielerBPunkte() {
		return spielerBPunkte;
	}

	/**
	 * Gibt die Satznummer zurück
	 */
	public int getSatznr() {
		return satznr;
	}

	/**
	 * Gibt den Startspieler des Satzes zurück
	 */
	public String getStartspieler() {
		return startspieler;
	}
	
	/**
	 * Durch diese Methode lässt sich der Startspieler setzen
	 * @param startspieler
	 */
	public void setStartspieler(String startspieler){
		this.startspieler = startspieler;
	}
	
	// Änder die Punkte für einen Satz
	/**
	 * Durch diese Methode lassen sich die Punkte von Spieler A und Spieler B für den Satz setzen
	 * @param spielerAPunkte
	 * @param spielerBPunkte
	 */
	public void changePoints(int spielerAPunkte, int spielerBPunkte){
		this.spielerAPunkte = spielerAPunkte;
		this.spielerBPunkte = spielerBPunkte;
	}

}
