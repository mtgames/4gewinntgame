package model.db;

import java.util.ArrayList;

/**
 * 
 * Die Klasse Spiel stellt die übergeordnete Schnittstelle zur Datenbank dar. 
 * Ein Spiel hat mehrere Sätze und ein Satz hat mehrere Züge.
 * In dieser Klasse können Sätze ausgehend von dem Spielobjekt angelegt werden.
 * 
 * @author MTGames
 * @Mitglieder Timo Strauch, Tim Holm, Meike Helwig, Marcel Schäfer
 *
 */

public class Spiel {
	
	/**
	 * Speichert die Datenbank Instanz
	 */
	private static DBConnector dbInstanz;
	
	/**
	 * Speichert die Nummer für den nächsten Satz
	 */
	private int nextsatznr = 1;
	
	/**
	 * speichert die ID des Spiels
	 * wird erst gesetzt, wenn persist( ) aufgerufen wird
	 * Ist sie nicht gesetzt ist das Spiel noch nicht persistiert
	 */
	private int id = -1;
	
	/**
	 * Speichert den Namen von Spieler A
	 */
	private String spielerA;
	/**
	 * Speichert den Namen von Spieler B
	 */
	private String spielerB;
	
	/**
	 * Speichert den Sieger eines Spiels
	 */
	private String sieger;
	
	/**
	 * Speichert die Schwierigkeit eines Spiels (Leicht, Mittel, Schwer)
	 */
	private String level;
	
	/**
	 * Speichert den Modus eines Spiels
	 */
	private String modus;
	
	/**
	 * Speichert die Saetze eines Spiels in einer ArrayList
	 */
	public ArrayList<Satz> saetze;
	
	
	//
	/**
	 *  protected Konstruktor für DBConnector.
	 *  Wird nur beim Auslesen der Spiele von der Datenbank benötigt
	 * @param spiel_id
	 * @param spielerA
	 * @param spielerB
	 * @param sieger
	 * @param modus
	 * @param level
	 */
	protected Spiel(int spiel_id, String spielerA, String spielerB, String sieger, String modus, String level){
		this.spielerA = spielerA;
		this.spielerB = spielerB;
		this.modus = modus;
		this.level = level;
		this.sieger = sieger;
		this.id = spiel_id;
		saetze = new ArrayList<Satz>();
		initDBifnotExist();
	}
	
	/**
	 * Konstruktor, um das Spiel anzulegen
	 * @param spielerA
	 * @param spielerB
	 * @param modus
	 * @param level
	 */
	public Spiel(String spielerA, String spielerB, String modus, String level){
		this.spielerA = spielerA;
		this.spielerB = spielerB;
		this.modus = modus;
		this.level = level;
		saetze = new ArrayList<Satz>();
		initDBifnotExist();
	}
	
	/**
	 * Mit dieser Methode können Sätze zum Spiel hinzugefügt werden, welche anschließend
	 * in der ArrayList der Methode abgelegt werden.
	 * @param spielerAPunkte
	 * @param spielerBPunkte
	 * @param startspieler
	 * @return Satzobjekt
	 */
	public Satz addSatz(int spielerAPunkte, int spielerBPunkte, String startspieler){
		Satz neu = new Satz(spielerAPunkte, spielerBPunkte, startspieler,nextsatznr);
		saetze.add(neu);
		nextsatznr++;
		return neu;
	}

	/**
	 * Muss aufgerufen werden, damit die Daten eines Spielobjekts persistiert werden
	 * @throws Exception
	 */
	public void persist() throws Exception{
		int sid = dbInstanz.persist(this);
		if(sid != -1){
			id = sid;
		}
	}

	/**
	 * Ermöglicht es den Gewinner von bereits gespeicherten Spielen zu ändern
	 * Gewinner muss vorher über SetWinner geändert werden
	 * @throws Exception
	 */
	public void perstistWinner() throws Exception{
		dbInstanz.persistNewWinner(this);
	}
	
	/**
	 * Gibt alle Spiele, die in der Datenbank stehen zurück
	 * Alle Sätze und Züge sind ebenfalls enthalten
	 * @return ArrayList<Spiel> aller Spiele
	 */
	public static ArrayList<Spiel> getallgames(){
		initDBifnotExist();
		return dbInstanz.getAllGames();
	}	
	
	/**
	 * Initialisiert die DB, wenn sie noch nicht existiert
	 */
	private static void initDBifnotExist(){
		if(dbInstanz == null ){
			try {
				dbInstanz = DBConnector.getInstanz();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	
	/**
	 * @return the modus
	 * Getter & Setter
	 */
	public String getModus() {
		return modus;
	}

	/**
	 * gibt den Schwierigkeitsgrad des Spieles zurück
	 * @return
	 */
	public String getLevel(){
		return level;
	}
	
	/**
	 * Gibt das Endergebnis des Spiels zurück
	 * @return Punkte von A
	 */
	public int getResultPlayerA(){
		int resultA = 0;
		for (Satz satz : saetze) {
			resultA = resultA + satz.getSpielerAPunkte();
		}
		return resultA;
	}
	
	/**
	 * Gibt das Endergebnis des Spiels zurück
	 * @return Punkte von B
	 */
	public int getResultPlayerB(){
		int resultB = 0;
		for (Satz satz : saetze) {
			resultB = resultB + satz.getSpielerBPunkte();
		}
		return resultB;
	}
	
	/**
	 * gibt die ID des spieles zurück
	 * @return Spiel ID
	 */
	public int getId() {
		return id;
	}

	/**
	 * gibt den Spielernamen von A zurück
	 * @return Spieler A
	 */
	public String getSpielerA() {
		return spielerA;
	}
	
	/**
	 * gibt den Spielernamen von B zurück
	 * @return Spieler B
	 */
	public String getSpielerB() {
		return spielerB;
	}
	
	/**
	 * gibt den Sieger des Spiels zurück
	 * @return Gewinner
	 */
	public String getWinner(){
		return sieger;
	}
	
	/**
	 * Über diese Methode lässt sich der Gewinner setzen.
	 * @param winner
	 */
	public void SetWinner(String winner){
		this.sieger = winner;
	}


}
