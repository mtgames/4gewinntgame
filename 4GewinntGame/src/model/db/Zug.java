package model.db;

/**
 * 
 * Repräsentiert den Zug zu einem Satz.
 * Kann angelegt werden über die Methode addZug(...) von Satz
 * 
 * @author MTGames
 * @Mitglieder Timo Strauch, Tim Holm, Meike Helwig, Marcel Schäfer
 * 
 */

public class Zug {
	/**
	 * speichert die Zug Nr des Zuges
	 */
	private int zug_nr;
	/**
	 * speichert den Spieler des Wurfes
	 */
	private String spieler;
	/**
	 * speichert die Spalte des Zuges
	 */
	private int spalte;
	
	/**
	 * gibt die Zugnummer zurück
	 * @return Zugnummer
	 */
	public int getZug_nr() {
		return zug_nr;
	}

	/**
	 * gibt den Spieler des Zuges zurück
	 * @return Spieler
	 */
	public String getSpieler() {
		return spieler;
	}

	/**
	 * gibt die Spalte des Zuges zurück
	 * @return Spalte
	 */
	public int getSpalte() {
		return spalte;
	}

	/**
	 * Privater Konstruktor zum Anlegen eines Zuges für das Auslesen der Datenbank
	 * @param zug_nr
	 * @param spieler
	 * @param spalte
	 */
	protected Zug( int zug_nr, String spieler, int spalte){
		this.zug_nr = zug_nr;
		this.spieler = spieler;
		this.spalte = spalte;
	}
	

}
