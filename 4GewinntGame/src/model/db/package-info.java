/**
* Das Paket model.db bildet die Schnittstelle zur Datenbank. Das Datenbankschema ist der Dokumentation zu entnehmen
* Die Modellierung ist so gewählt, dass ein Spiel mehrere Sätze hat und ein Satz hat mehrere Züge. Damit nicht
* immer direkt mit SQL Befehlen auf die Datenbank zugegriffen werden muss ist das Schema im objektorientierten Ansatz umgesetzt. 
* Das heißt im Packet der Datenbank befinden sich Klassen für Spiel, Satz und Zug, welche die gleichen 
* Zusammenhänge aufweisen wie sie in der Datenbank abgebildet sind. Eine zusätzliche Klassen kümmert 
* sich dabei um die Umsetzung dieser Objekte in die SQL Sprache.
*/
package model.db;