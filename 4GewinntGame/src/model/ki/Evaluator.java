package model.ki;


/**
 * 
 * Die Klasse Evaluator wird für die Bewertung des Spielfeldes genutzt. Hierbei werden nach einem
 * festgelegten Punktesystem (siehe dazu externe Dokumentation <titel>) die einzelnen Threats (siehe dazu Klasse Threat) bewertet und eine 
 * Gesamtbewertung für das Spielfeld berechnet.
 * 
 * @author MTGames
 * @Mitglieder Timo Strauch, Tim Holm, Meike Helwig, Marcel Schäfer
 * 
 *
 */

public class Evaluator {
	
	/**
	 * 8 Punkte für einen vertikalen threat, 10 punkte für einen horizontalen und 9 für einen diagonalen
	 */
	public static final int[] punkteType = {8,10,9};
	
	/**
	 * 4 Fälle:
	 * 1. Startspieler & gerader Threat: 5 Punkte
	 * 2. Startspieler & ungerader Threat: 10 Punkte
	 * 3. nicht-Startspieler & gerader Threat: 10 Punkte
	 * 4. nicht-Startspieler & ungerader Threat: 5 Punkte
	 */
	public static final int[] geradeUngeradeThreatTeam = {5,10,10,5}; 
		
	/**
	 * Gibt Punktzahlen für die Höhe eines Threats auf dem Spielfeld an für den nicht-Startspieler.
	 * Bsp.: Threat des nicht-Startspielers liegt in Zeile 2: 5 Punkte
	 */
	public static final int[] spielfeldHoeheDefender = {1,5,2,3,1,1};
	
	/**
	 * Analog zu spielfeldHoeheDefender für den Startspieler.
	 */
	public static final int[] spielfeldHoeheStarter = {2,1,5,1,2,1};
	
	/**
	 * Anzahl der Steine eines existierenden Threats bilden index für das Array "gefaehrlichkeit"
	 */
	public static final int[] gefaehrlichkeit = {1,4,40,163,850};
	
	/**
	 * StartingPlayer muss bei der Initialisierung der KI-Klassen gesetzt werden
	 * und wird bei der Bewertung der einzelnen Threats benötigt.
	 */
	public static int startingPlayer;
	
	/**
	 * found4er wird gesetzt, wenn eine 4er-Kette gefunden wurde und somit ein Spieler
	 * gewonnen hat.
	 */
	public boolean found4er = false;
	
	
	/**
	 * 
	 * @param gameField
	 * @param threatManager
	 * @return score
	 * 
	 * Die Methode evaluate wird von den Blattknoten des Min-Max-Baums aufgerufen und wertet
	 * das übergebene Spielfeld aus. Dabei findet sowohl eine Ermittlung des gegnerischen Wertes als auch des
	 * eigenen Statt, um anschließend die Differenz aus eigenem und gegnerischem zurückzugeben.
	 */
	public int evaluate(GameField gameField, ThreatManager threatManager){
		int scoreMe = 0;
		int scoreOpp = 0;
		
		for (int i=0; i < threatManager.myThreats.size(); i++){
			Threat t = threatManager.threats.get(threatManager.myThreats.get(i));
			scoreMe += this.evaluateSingleThreat(t);
		}
		for (int i=0; i < threatManager.opponentThreats.size(); i++){
			Threat t = threatManager.threats.get(threatManager.opponentThreats.get(i));
			scoreOpp += this.evaluateSingleThreat(t);
		}
		
		
		return scoreMe - scoreOpp;
		
		
	}
	
	/**
	 * 
	 * @param t
	 * @return scoreThreat
	 * 
	 * In dieser Methode werden die einzelnen Threats ausgewertet und Punkte vergeben.
	 */
	private int evaluateSingleThreat(Threat t){
		int score = 0;
		
		if (t.type == Threat.TYPE_VERTICAL){
			score = this.evaluateVerticalThreat(t);
		}
		else if (t.type == Threat.TYPE_HORIZONTAL){
			if (t.team == Evaluator.startingPlayer){
				score = this.evaluateHorizontalThreat(t, true);
			}
			else{
				score = this.evaluateHorizontalThreat(t, false);
			}
		}
		else if (t.type == Threat.TYPE_DIAGONAL){
			if (t.team == Evaluator.startingPlayer){
				score = this.evaluateDiagonalThreat(t, true);
			}
			else{
				score = this.evaluateDiagonalThreat(t, false);
			}
		}
		
		
		return score;
	}
	
	/**
	 * 
	 * @param t
	 * @param belongsToStarter
	 * @return scoreThreat
	 * 
	 * Wertet einen diagonalen Threat aus. 
	 */
	private int evaluateDiagonalThreat(Threat t, boolean belongsToStarter){
		int punkteType = Evaluator.punkteType[2];
		
		int punkteGeradeUngerade = 0;
		if (t.direction == Threat.DIRECTION_UNGERADE && belongsToStarter){
			punkteGeradeUngerade = Evaluator.geradeUngeradeThreatTeam[2];
		}
		else if (t.direction == Threat.DIRECTION_GERADE && !belongsToStarter){
			punkteGeradeUngerade = Evaluator.geradeUngeradeThreatTeam[1];
		}
		else if (belongsToStarter){
			punkteGeradeUngerade = Evaluator.geradeUngeradeThreatTeam[0];
		}
		else{
			punkteGeradeUngerade = Evaluator.geradeUngeradeThreatTeam[3];
		}
		
		//if there is a direction set which is not "none" we need to find out which row still has an empty field
		int spielfeldhoehe = spielfeldHoeheDefender[0];
		if (t.direction != Threat.DIRECTION_NONE){
			for (int c=0; c < t.position.length; c++){
				if (!(t.position[c][0] == t.occupied[c][0])){
					//found it
					if(belongsToStarter){
						spielfeldhoehe = Evaluator.spielfeldHoeheStarter[t.position[c][1]];
					}
					else{
						spielfeldhoehe = Evaluator.spielfeldHoeheDefender[t.position[c][1]];
					}
				}
			}
		}
		
		int gefaehrlichkeit = Evaluator.gefaehrlichkeit[t.countOccupiedFields];
		if (t.countOccupiedFields == 4){
			this.found4er = true;
		}

		return punkteType+punkteGeradeUngerade+spielfeldhoehe+gefaehrlichkeit;
	}
	
	/**
	 * 
	 * @param t
	 * @param belongsToStarter
	 * @return scoreThreat
	 * 
	 * Wertet einen horizontalen Threat aus.
	 */
	private int evaluateHorizontalThreat(Threat t, boolean belongsToStarter){
		int punkteType = Evaluator.punkteType[1];
		
		int spielfeldhoehe = 0;
		if (belongsToStarter){
			spielfeldhoehe = Evaluator.spielfeldHoeheStarter[t.position[0][1]]; //t.position[0][1] --> Zeile des "ersten" Feldes, das zum Threat gehört (da horizontal --> alle gleiche Zeile)
		}
		else{
			spielfeldhoehe = Evaluator.spielfeldHoeheDefender[t.position[0][1]];
		}
		
		int punkteGeradeUngerade = 0;
		if (t.direction == Threat.DIRECTION_UNGERADE && belongsToStarter){
			punkteGeradeUngerade = Evaluator.geradeUngeradeThreatTeam[2];
		}
		else if (t.direction == Threat.DIRECTION_GERADE && !belongsToStarter){
			punkteGeradeUngerade = Evaluator.geradeUngeradeThreatTeam[1];
		}
		else if (belongsToStarter){
			punkteGeradeUngerade = Evaluator.geradeUngeradeThreatTeam[0];
		}
		else{
			punkteGeradeUngerade = Evaluator.geradeUngeradeThreatTeam[3];
		}
		
		int gefaehrlichkeit = Evaluator.gefaehrlichkeit[t.countOccupiedFields];
		if (t.countOccupiedFields == 4){
			this.found4er = true;
		}
		
		return punkteType+punkteGeradeUngerade+spielfeldhoehe+gefaehrlichkeit;
	}
	
	/**
	 * 
	 * @param t
	 * @return scoreThreat
	 * 
	 * Wertet einen vertikalen Threat aus.
	 */
	private int evaluateVerticalThreat(Threat t){
		int punkteType = Evaluator.punkteType[0];
		int punkteGeradeUngerade = Evaluator.geradeUngeradeThreatTeam[0];
		int spielfeldhoehe = Evaluator.spielfeldHoeheDefender[0];
		int gefaehrlichkeit = Evaluator.gefaehrlichkeit[t.countOccupiedFields];
		if (t.countOccupiedFields == 4){
			this.found4er = true;
		}
		
		return punkteType+punkteGeradeUngerade+spielfeldhoehe+gefaehrlichkeit;
	}
	
}
