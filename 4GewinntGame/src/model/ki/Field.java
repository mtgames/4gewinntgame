package model.ki;

import java.util.ArrayList;

import supportFiles.Constants;

/**
 * 
 * Die Klasse Field stellt eine Repräsentation eines Feldes auf dem gesamten Spielfeld dar. Jedes Feld
 * hat dabei eine eindeutige Position <spalte, reihe> auf dem Spielfeld. Jedes Feld kennt darüber hinaus
 * die Threats, deren Bestandteil es ist. 
 * 
 * @author MTGames
 * @Mitglieder Timo Strauch, Tim Holm, Meike Helwig, Marcel Schäfer
 *
 */

public class Field {
	
	/** 
	 * Mit der Variable team wird der Spieler, der einen Spielstein auf dem Feld platziert hat, 
	 * identifiziert.
	 */
	private int team = -1;
	
	/**
	 * Occupied gibt an, ob das Feld leer ist oder ob es mit einem Spielstein belegt ist.
	 */
	private boolean occupied = false;
	
	/**
	 * position speichert die Position des Feldes auf dem Spielfeld im Format [spalte, reihe]
	 */
	public int[] position = new int[2];
	
	/**
	 * threatsWithField hält eine Liste aller Threats, deren Bestandteil das Feld darstellt.
	 */
	protected ArrayList<Threat> threatsWithField = new ArrayList<Threat>();
	
	
	
	/**
	 * @param pos
	 * Standardkonstruktor mit Übergabe eines positionsarrays
	 */
	public Field(int[] pos){
		this.position = pos;
	}
	
	/**
	 * 
	 * @param f
	 * Konstruktor zur Erzeugung einer Deepcopy von einem Field Objekt
	 */
	public Field(Field f){
		team = f.team;
		occupied = f.occupied;
		for (int i=0; i < position.length; i++){
			position[i] = f.position[i];
		}
	}
	
	/**
	 * 
	 * @param team
	 * occupy() immer dann aufgerufen, wenn ein Feld aufgrund eines Spielsteinwurfes belegt werden soll.
	 * Diese Information wird außerdem an die in threatsWithField gehaltenen Threats weitergegeben.
	 */
	public void occupy(int team){
		this.team = team;
		this.occupied = true;
		for (int i=0; i < threatsWithField.size(); i++){
			threatsWithField.get(i).occupyField(this);
		}
	}
	
	/**
	 * release() gibt eine belegtes Feld wieder frei. Diese Methode wird im produktiven Einsatz nicht genutzt.
	 */
	public void release(){
		this.team = 0;
		this.occupied = false;
	}

	/**
	 * 
	 * @return
	 * getter für team
	 */
	public int getTeam() {
		return team;
	}
	
	/**
	 * 
	 * @return
	 * Die Methode wird bei der Verwendung des Konsolenprogramms (Main-Methode von KIGameMaster) verwendet.
	 */
	public String getTeamPrintable(){
		if (team == -1){
			return " ";
		}
		else if (team == Constants.OPPONENT){
			return "X";
		}
		else{
			return "O";
		}
	}
	
	/**
	 * @return
	 * getter für occupied
	 */
	public boolean isOccupied() {
		return occupied;
	}
	
	/**
	 * gibt Inhalt des Field-Objektes aus.
	 */
	public void print(){
		System.out.println("Field: "+position[0]+", "+position[1]+ " Team: "+team);
	}
}
