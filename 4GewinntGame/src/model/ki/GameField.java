package model.ki;

import java.util.ArrayList;

/**
 * 
 * Die Klasse GameField repräsentiert das Spielfeld bestehend aus 42 Feldern (Klasse Field).
 * 
 * @author MTGames
 * @Mitglieder Timo Strauch, Tim Holm, Meike Helwig, Marcel Schäfer
 * 
 */

public class GameField{
	
	/**
	 * rowCount gibt die Anzahl der Reihen an.
	 */
	private int rowCount = 6;
	
	/**
	 * ColCount gibt die Anzahl der Spalten an.
	 */
	private int colCount = 7;
	
	/**
	 * gameField stellt das eigentliche Spielfeld, bestehend aus 42 Feldern dar.
	 */
	public Field[][] gameField;
	
	/**
	 * colHeight ist ein Array, das die aktuelle Belegung der Spalten angibt.
	 */
	public int[] colHeights = new int[7];
	
	/**
	 * countFullColumns gibt die Anzahl der vollständig gefüllten Spalten an.
	 */
	public int countFullColumns = 0;
	
	/**
	 * occupiedFieldIndeces hält die Positionen der belegten Spielfelder. Die Positionen werden
	 * in der Form [spalte, reihe] angegeben.
	 */
	public ArrayList<Integer[]> occupiedFieldIndeces = new ArrayList<Integer[]>();
	
	/**
	 * 
	 * @param gm
	 * Dieser Konstruktor dient zur Durchführung eines Deep-Copy von einem GameField-Objekt.
	 */
	public GameField(GameField gm){
		rowCount = gm.rowCount;
		colCount = gm.colCount;
		countFullColumns = gm.countFullColumns;
		for (int i=0; i<gm.colHeights.length; i++){
			colHeights[i] = gm.colHeights[i];
		}
		this.gameField = new Field[this.colCount][this.rowCount]; 
		for (int i=0; i < gameField.length; i++){
			for (int z=0; z < gameField[i].length; z++){
				gameField[i][z] = new Field(gm.gameField[i][z]);
			}
		}
		for (int i=0; i < gm.occupiedFieldIndeces.size(); i++){
			Integer[] pos = {gm.occupiedFieldIndeces.get(i)[0].intValue(), gm.occupiedFieldIndeces.get(i)[1].intValue()};
			this.occupiedFieldIndeces.add(pos);
		}
	}
	
	/**
	 * Standardkonstruktor mit Erzeugung eines Spielfelds.
	 */
	public GameField(){
		
		//create the field
		this.gameField = new Field[this.colCount][this.rowCount]; 
		
		for (int i=0; i < gameField.length; i++){
			for (int z=0; z < gameField[i].length; z++){
				int[] pos = {i,z};
				gameField[i][z] = new Field(pos);
			}
		}
		
	}
	
	/**
	 * 
	 * @param col
	 * @param team
	 * 
	 * Die methode throwChip wird immer dann aufgerufen, wenn ein Spielstein in eine Spalte auf 
	 * dem Spielfeld geworfen werden soll. 
	 */
	public void throwChip(int col, int team){
		this.gameField[col][this.colHeights[col]].occupy(team);
		Integer[] intA = {col,this.colHeights[col]};
		this.occupiedFieldIndeces.add(intA);
		this.colHeights[col] += 1;
		if(this.isColFull(col)){
			this.countFullColumns += 1;
		}
	}
	
	/**
	 * 
	 * @param col
	 * @return
	 * 
	 * isColFull prüft, ob die Spalte col bereits voll ist.
	 */
	public boolean isColFull(int col){
		if (this.colHeights[col] == 6){
			return true;
		}
		return false;
	}
	
	/**
	 * Gibt Spielfeld strukturiert auf der Konsole aus (Bei Spiel über die Konsole)
	 */
	public void printGameField(){
		String[] lines = {"","|","","|","","|","","|","","|","","|",""};
		lines[0] = "-----------------------------";
		lines[2] = "-----------------------------";
		lines[4] = "-----------------------------";
		lines[6] = "-----------------------------";
		lines[8] = "-----------------------------";
		lines[10] = "-----------------------------";
		lines[12] = "-----------------------------";
		for (int c=0; c < this.gameField.length; c++){
				lines[1] += " "+this.gameField[c][this.gameField[c].length-1].getTeamPrintable()+" |";
				lines[3] += " "+this.gameField[c][this.gameField[c].length-2].getTeamPrintable()+" |";
				lines[5] += " "+this.gameField[c][this.gameField[c].length-3].getTeamPrintable()+" |";
				lines[7] += " "+this.gameField[c][this.gameField[c].length-4].getTeamPrintable()+" |";
				lines[9] += " "+this.gameField[c][this.gameField[c].length-5].getTeamPrintable()+" |";
				lines[11] += " "+this.gameField[c][this.gameField[c].length-6].getTeamPrintable()+" |";
		}
		
		System.out.println("**************************************************");
		for (int i=0; i < lines.length; i++){
			System.out.println("          "+lines[i]);
		}
		System.out.println("**************************************************");
	}
	
	/**
	 * @param title
	 * Analog zu printGameField(), fügt jedoch einen Titel hinzu
	 */
	public void printGameField(String title){
		String[] lines = {"","|","","|","","|","","|","","|","","|",""};
		lines[0] = "-----------------------------";
		lines[2] = "-----------------------------";
		lines[4] = "-----------------------------";
		lines[6] = "-----------------------------";
		lines[8] = "-----------------------------";
		lines[10] = "-----------------------------";
		lines[12] = "-----------------------------";
		for (int c=0; c < this.gameField.length; c++){
				lines[1] += " "+this.gameField[c][this.gameField[c].length-1].getTeamPrintable()+" |";
				lines[3] += " "+this.gameField[c][this.gameField[c].length-2].getTeamPrintable()+" |";
				lines[5] += " "+this.gameField[c][this.gameField[c].length-3].getTeamPrintable()+" |";
				lines[7] += " "+this.gameField[c][this.gameField[c].length-4].getTeamPrintable()+" |";
				lines[9] += " "+this.gameField[c][this.gameField[c].length-5].getTeamPrintable()+" |";
				lines[11] += " "+this.gameField[c][this.gameField[c].length-6].getTeamPrintable()+" |";
		}
		
		System.out.println("**************************************************");
		System.out.println(title);
		System.out.println(" ");
		for (int i=0; i < lines.length; i++){
			System.out.println("          "+lines[i]);
		}
		System.out.println("**************************************************");
	}
	
	/**
	 * 
	 * @param tm
	 * @return
	 * 
	 * Findet heraus, ob es einen Sieger gibt (d.h. eine 4er Kette von Spielsteinen existiert) oder ob das 
	 * Spielfeld voll ist und gibt diese Information zurück.
	 */
	public String hasGameEnded(ThreatManager tm){
		//check if field is full
		if (this.countFullColumns == 7){
			return "F";
		}
		
		
		//check if there is a winner
		for (int i=0; i < tm.myThreats.size(); i++){
			Threat t = tm.threats.get(tm.myThreats.get(i));
			if (t.countOccupiedFields == 4){
				return "K";
			}
		}
		for (int i=0; i < tm.opponentThreats.size(); i++){
			Threat t = tm.threats.get(tm.opponentThreats.get(i));
			if (t.countOccupiedFields == 4){
				return "O";
			}
		}
		
		return "-";
	}
}
