package model.ki;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Observable;

import supportFiles.Constants;
import supportFiles.GameplayCommunicator;
import supportFiles.GameplayDataPackage;

/**
 * 
 * Diese Klasse stellt der zentrale Zugriffspunkt auf die KI dar.
 * 
 * Es wird mit einer Klasse, die von der abstrakten Klasse GameplayCommunicator erbt, kommuniziert.
 * Diese fungiert als Monitor für den KIGameMaster-Thread (und im Turniermodus für den ServerFileReadWrite-Thread).
 * Solange der gegnerische Spieler am Zug ist, wartet der KIGameMaster-Thread am Monitor. Sobald dieser einen
 * Zug getätigt hat, wird der Thread geweckt und berechnet seinen nächsten Spielzug. Die gewählte Spalte 
 * wird per Event den daran interessierten Klassen mitgeteilt (im Turniermodus um die zur Kommunikation mit
 * dem Server verwendete Textdatei zu schreiben). Dabei kommt das Observer-Pattern zum Einsatz. Für einen genaueren
 * Einblick in die Kommunikation der einzelnen Threads und Klassen siehe auch "Kommunikation Turniermodus.pdf". 
 * 
 * @author MTGames
 * @Mitglieder Timo Strauch, Tim Holm, Meike Helwig, Marcel Schäfer
 * 
 */

public class KIGameMaster extends Observable implements Runnable {
	
	/**
	 * Konstante für Turniermodus
	 */
	public static final int MODE_TOURNAMENT = 10;
	
	/**
	 * Konstante für Spieler vs. KI Modus
	 */
	public static final int MODE_PLAYER_VS_KI = 20;
	
	/**
	 * Konstante für das Stoppen des KIGameMaster-Threads
	 */
	public static final int SHALL_DIE = -1000;
	
	/**
	 * Konstante für einen initialen Wert.
	 */
	public static final int INITIAL = -1;
	
	/**
	 * hält eine Referenz auf die aktuelle interne Repräsentation des Spielfeldes.
	 */
	public GameField gameField;
	
	/**
	 * hält eine Referenz auf den ThreatManager, der die Threats für das aktuelle Spielfeld verwaltet.
	 */
	public ThreatManager threatManager;
	
	/**
	 * hält eine Referenz auf das Predictor Objekt, welches die Schnittstelle zum MinMax-Baum darstellt.
	 */
	public Predictor predictor;
	
	/**
	 * hält eine Referenz auf den GameplayCommunicator. Dabei handelt es sich entweder um einen Controller 
	 * oder um ServerCommunicatorMonitor.
	 */
	public GameplayCommunicator communicator;
	
	/**
	 * mode ist entweder "MODE_TOURNAMENT" oder "MODE_PLAYER_VS_KI"
	 */
	public int mode = INITIAL;
	
	/**
	 * boolean Variable, die vor jedem Durchlauf der Spiel-Schleife geprüft wird.
	 */
	public boolean gameEnded = false;
	
	/**
	 * boolean Variable, die angibt, ob das KIGameMaster Objekt über die Methode initialize initialisiert wurde.
	 */
	public boolean isInitialized = false;
	
	/**
	 * vereinbarte Zugzeit in ms
	 */
	public int zugZeitInMs;
	
	/**
	 * Debug-Variable: Wird kontinuierlich zur Laufzeit berechnet und ausgegeben. Gibt an, wielange die KI
	 * zum Berechnen des nächsten Zuges gebraucht hat.
	 */
	@SuppressWarnings("unused")
	private long timeNeededForCalculation;
	
	
	/**
	 * 
	 * @param c
	 * Konstruktor zur Erzeugung eines Objektes der Klasse KIGameMaster. Hier wird die Verbindung zum 
	 * verwendeten GameplayCommunicator hergestellt.
	 */
	public KIGameMaster(GameplayCommunicator c){
		this.gameField = new GameField();
		this.threatManager = new ThreatManager(this.gameField);
		this.communicator = c;
		this.communicator.gameMaster = this;
		setChanged();
	}
	
	/**
	 * Standardkonstruktor
	 */
	private KIGameMaster(){
		this.gameField = new GameField();
		this.threatManager = new ThreatManager(this.gameField);
	}
	
	
	/**
	 * @param searchDepth
	 * @param startingPlayer
	 * @param mode
	 * 
	 * initialize erzeugt unter Berücksichtigung der Suchtiefe und des Start-Spielers (Constants.STARTING_PLAYER_ME
	 * oder Constants.STARTING_PLAYER_OPPONENT) den Predictor, der alle Berechnungen ausführt.
	 * Wird im Spieler vs. KI Modus verwendet.
	 */
	public void initialize(int searchDepth, int startingPlayer, int mode){
		if (!(searchDepth >= 2 && searchDepth <= 6)){
			searchDepth = 4;
		}
		Evaluator.startingPlayer = startingPlayer;
		this.predictor = new Predictor(searchDepth, this.gameField, this.threatManager, startingPlayer);
		this.mode = mode;
		this.isInitialized = true;
		this.zugZeitInMs = 1000;
	}
	
	/**
	 * @param searchDepth
	 * @param startingPlayer
	 * @param mode
	 * @param zugZeitInMs
	 * 
	 * initialize erzeugt unter Berücksichtigung der Suchtiefe und des Start-Spielers (Field.ME oder Field.OPPONENT)
	 * den Predictor, der alle Berechnungen ausführt. Erhält außerdem Information über die Zugzeit.
	 * Wird im Tournament Modus verwendet.
	 */
	public void initialize(int evalDepth, int startingPlayer, int mode, int zugZeitInMs){
		Evaluator.startingPlayer = startingPlayer;
		
		GameplayDataPackage g = new GameplayDataPackage(startingPlayer, GameplayDataPackage.CONTEXT_UI_TOURNAMENT);
		g.contentInfo = GameplayDataPackage.CONTENT_START_PLAYER;
		g.value = startingPlayer;
		setChanged();
		this.notifyObservers(g);
		
		this.predictor = new Predictor(evalDepth, this.gameField, this.threatManager, startingPlayer);
		this.mode = mode;
		this.isInitialized = true;
		this.zugZeitInMs = zugZeitInMs;
	}
	
	
	/**
	 * @param startingPlayer
	 * 
	 * initialize erzeugt unter Berücksichtung des Start-Spielers (Field.ME oder Field.OPPONENT) den Predictor mit
	 * default-Suchtiefe
	 */
	public void initialize(int startingPlayer){
		Evaluator.startingPlayer = startingPlayer;
		this.predictor = new Predictor(4, this.gameField, this.threatManager, startingPlayer);
		this.isInitialized = true;
	}
	
	/**
	 * kalkuliere die nächste Spalte, in die geworfen werden soll
	 * Aktualisiere den MinMax-Baum und 
	 * teile die Spalte den Observern mit (ViewController oder ServerCommunicator)
	 */
	public int calcNextThrow(){
		long t = System.currentTimeMillis();
		
		this.predictor.evaluate();
		
		int col = this.predictor.root.pathToGoNode.throwColumn;
		
		GameplayDataPackage gdp = new GameplayDataPackage(col, GameplayDataPackage.CONTEXT_UI_SERVER);
		setChanged();
		notifyObservers(gdp);
		
		System.out.println("Time needed for calculation: "+(System.currentTimeMillis()-t)+" Zugzeit: "+this.zugZeitInMs);
		
		this.gameField.throwChip(col, Constants.ME);
		
		this.predictor.moveDownSearchTree(col);

		
		this.predictor.evaluate();
		
		return col;
	}
	
	
	/**
	 * Berücksichtige den gegnerischen Zug und werfe einen gegnerischen Spielstein 
	 * in die gewählte Spalte auf dem Spielfeld.
	 */
	public void setOpponentsThrow(int throwCol){
		this.gameField.throwChip(throwCol, Constants.OPPONENT);

		this.predictor.moveDownSearchTree(throwCol);
		
		GameplayDataPackage gdp = new GameplayDataPackage(throwCol, GameplayDataPackage.CONTEXT_UI_TOURNAMENT);
		setChanged();
		notifyObservers(gdp);
	}
	
	/**
	 * Standard-Thread run Methode. Lässt die KI entweder im Spieler vs Computer Modus laufen
	 * oder im Tournament Modus
	 */
	@Override
	public void run() {
		// TODO Auto-generated method stub
		if (this.mode == KIGameMaster.MODE_TOURNAMENT){
			this.tournamentModeGameLoop();
		}
		else{
			this.manualModeGameLoop();
		}
		
		this.isInitialized = false;
	}
	
	
	/**
	 * implementiert die GameLoop für den Spieler vs. Computer Modus
	 * Unterschieden wird dabei hinsichtlich des Startspielers: Spieler oder Computer
	 */
	private void manualModeGameLoop(){
		this.gameEnded = false;
		int col = -1;
		
		if (Evaluator.startingPlayer == Constants.ME){

			while(!gameEnded){

				col = this.calcNextThrow();
				
				String s = this.gameField.hasGameEnded(this.predictor.threatManager);
				if (s.startsWith("K")){
					this.notifyWithMessage(GameplayDataPackage.MSG_KI_GEWINNT);
					gameEnded = true;
					break;
				}
				else if (s.startsWith("F")){
					this.notifyWithMessage(GameplayDataPackage.MSG_FELD_VOLL);
					gameEnded = true;
					break;
				}
				
				col = this.communicator.getPlayersThrow();
				if(gameEnded){break;}
				
				this.setOpponentsThrow(col);
				s = this.gameField.hasGameEnded(this.predictor.threatManager);
				if (s.startsWith("O")){
					this.notifyWithMessage(GameplayDataPackage.MSG_SPIELER_GEWINNT);
					gameEnded = true;
					break;
				}
				else if (s.startsWith("F")){
					this.notifyWithMessage(GameplayDataPackage.MSG_FELD_VOLL);
					gameEnded = true;
					break;
				}
				
			}
			
		}
		else{
			
			while(!gameEnded){
				col = this.communicator.getPlayersThrow();
				if (gameEnded){break;}
				
				this.setOpponentsThrow(col);
				String s = this.gameField.hasGameEnded(this.predictor.threatManager);
				if (s.startsWith("O")){
					this.notifyWithMessage(GameplayDataPackage.MSG_SPIELER_GEWINNT);
					gameEnded = true;
					break;
				}
				else if (s.startsWith("F")){
					this.notifyWithMessage(GameplayDataPackage.MSG_FELD_VOLL);
					gameEnded = true;
					break;
				}

				col = this.calcNextThrow();
//				this.evaluateTimeNeededForCalculation();
				
				s = this.gameField.hasGameEnded(this.predictor.threatManager);
				if (s.startsWith("K")){
					this.notifyWithMessage(GameplayDataPackage.MSG_KI_GEWINNT);
					gameEnded = true;
					break;
				}
				else if (s.startsWith("F")){
					this.notifyWithMessage(GameplayDataPackage.MSG_FELD_VOLL);
					gameEnded = true;
					break;
				}
				
				
			}
		}
	}
	
	/**
	 * Implementiert die Gameloop für den Turniermodus
	 * Die Unterscheidung zwischen der Reihenfolge, in der die Methoden ausgeführt werden sollen
	 * findet anhand des Startspielers statt.
	 */
	private void tournamentModeGameLoop(){
		this.gameEnded = false;
		int col = -1;
		
		if (Evaluator.startingPlayer == Constants.ME){
			
			while(!gameEnded){
				col = this.calcNextThrow();
				
//				this.evaluateTimeNeededForCalculation();
				
				col = this.communicator.getPlayersThrow();
				if(gameEnded){
					this.evaluateColumn(col);
					break;
				}
				this.setOpponentsThrow(col);

			}
		}
		else{
			
			while(!gameEnded){
				col = this.communicator.getPlayersThrow();
				if(gameEnded){
					this.evaluateColumn(col);
					break;
				}
				this.setOpponentsThrow(col);
				
				col = this.calcNextThrow();
				
//				this.evaluateTimeNeededForCalculation();
			}
		}
	}
	
	/**
	 * evaluiere die Spalte, die vom ServerCommunicator als gegnerischen Zug zurückgegeben wird.
	 * Prüfe dabei auf Konstante werte, die eine besondere Bedeutung haben. 
	 */
	private void evaluateColumn(int col){
		String msg = "";
		if (col == INITIAL){
			//KI hat gewonnen
			msg = GameplayDataPackage.MSG_KI_GEWINNT;
			this.notifyWithMessage(msg);
		}
		else if (col == SHALL_DIE){
			//tue nichts und stirb einfach.
		}
		else{
			this.setOpponentsThrow(col);
			msg = GameplayDataPackage.MSG_SPIELER_GEWINNT;
			this.notifyWithMessage(msg);
		}
	}
	
	/**
	 * INFO: Das Laufwerk, welches für die Kommunikation zwischen dem Server und der KI verwendet wird benötigt zwischen 2s 
	 * und 3s um die Schreib- und Lesevorgänge auf die Kommunikationsdateien zu realisieren. Aus diesem Grund kann 
	 * die Methode "evaluateTimeNeededForCalculation" nicht mehr zum Einsatz kommen.
	 * 
	 * Setzt die Idee um zur Laufzeit die Suchtiefe des MinMax Baums automatisch
	 * anzupassen. Die Erhöhung der Suchtiefe geschieht dabei durch Erhöhen der
	 * Instanzvariable "evalDepth" vom predictor-Objekt. Der Zeitpunkt der Erhöhung hängt von der 
	 * zur Verfügung stehenden Zugzeit ab.
	 */
	@SuppressWarnings("unused")
	private void evaluateTimeNeededForCalculation(){
		if (this.predictor.evalDepth < 6){
			if (zugZeitInMs >= 3000){
				if(this.predictor.startingTeam == Constants.ME){
					this.predictor.evalDepth = 5;
				}
				if (this.predictor.gameField.countFullColumns >= 1){
					this.predictor.evalDepth = 6;
				}
			}
			else if(zugZeitInMs >= 2000){
				if(this.predictor.gameField.occupiedFieldIndeces.size() > 0 && this.predictor.startingTeam == Constants.ME){
					this.predictor.evalDepth = 5;
				}
				
				if (this.predictor.gameField.occupiedFieldIndeces.size() > 13 && this.predictor.gameField.countFullColumns >= 1){
					this.predictor.evalDepth = 6;
				}
				
			}
			else if (zugZeitInMs >= 1000){ 
				if(this.predictor.gameField.occupiedFieldIndeces.size() > 3 && this.predictor.startingTeam == Constants.ME){
					this.predictor.evalDepth = 5;
				}
				
				if (this.predictor.gameField.countFullColumns >= 2){
					this.predictor.evalDepth = 6;
				}
			}

		}
	}
	
	/**
	 * @param msg
	 * Teilt dem UI Informationen über den Spielstatus mit.
	 */
	private void notifyWithMessage(String msg){
		GameplayDataPackage gdp = new GameplayDataPackage(msg, GameplayDataPackage.CONTEXT_UI_SERVER);
		gdp.contentInfo = GameplayDataPackage.CONTENT_MSG;
		setChanged();
		notifyObservers(gdp);
	}
	
	
	/**
	 * 
	 * @param args
	 * Einstieg über die main-Methode der KIGameMaster-Klasse startet ein rudimentäres Konsolenspiel. 
	 */
	public static void main(String[] args){
		KIGameMaster gm = new KIGameMaster();	
		try {
			gm.start2PlayerGameLoopConsole(gm);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
	
	public void start2PlayerGameLoopConsole(KIGameMaster gm) throws IOException{
		String readLine;
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Gib die Spaltenzahl hinter dem > zeichen ein. (Spaltenzahl muss zwischen 1 und 7 liegen!)");
		String player = "Kreuz"; //"Kreis" oder "Kreuz";
		int team;
		int col = -1;
		
		//init KI
		gm.gameField = new GameField();
		gm.threatManager = new ThreatManager(gm.gameField);
		if (player.equals("Kreuz")){
			gm.predictor = new Predictor(4, gm.gameField, gm.threatManager, Constants.ME);
			Evaluator.startingPlayer = Constants.ME;
		}
		else{
			gm.predictor = new Predictor(4, gm.gameField, gm.threatManager, Constants.OPPONENT);
			Evaluator.startingPlayer = Constants.OPPONENT;
		}
		gm.predictor.evaluate();
		
		while(true){
			gm.gameField.printGameField();
			
			if (player.equals("Kreis")){
				System.out.println(player+":>");
				readLine = reader.readLine();

				// get integer value
				col = Integer.parseInt(readLine)-1;
				if (col >= 0 && col <= 6){
					if (gm.gameField.isColFull(col)){
						System.out.println("Spalte ist bereits voll!");
					}
					else{
						if (player.equals("Kreis")){
							team = Constants.ME;
							player = "Kreuz";
						}
						else{
							team = Constants.OPPONENT;
							player = "Kreis";
						}
						gm.gameField.throwChip(col, team);
						String a = gm.gameField.hasGameEnded(gm.threatManager);
						if (a.equals("O")){
							System.out.println("KI verliert!");
							break;
						}
						else if (a.equals("Feld ist voll!")){
							System.out.println("Unentschieden!");
							break;
						}
					}
				}
				else{
					System.out.println("Ungültige Spalte!");
				}
			}
			else{
				//computers turn
				if(col != -1){ //Computer does not start
					gm.predictor.moveDownSearchTree(col);
					gm.predictor.evaluate();

					this.predictor.root.printTeamPathToGo();
				}
				if (player.equals("Kreis")){
					team = Constants.ME;
					player = "Kreuz";
				}
				else{
					team = Constants.OPPONENT;
					player = "Kreis";
				}
				
				col = gm.predictor.root.pathToGoNode.throwColumn;
				
				gm.gameField.throwChip(col, team);
				
				gm.predictor.moveDownSearchTree(col);
				
				gm.predictor.evaluate();
				
				String a = gm.gameField.hasGameEnded(gm.threatManager);
				if (a.equals("K")){
					System.out.println("Du verlierst!");
					break;
				}
				else if (a.equals("Feld ist voll!")){
					System.out.println("Unentschieden!");
					break;
				}
			}
		}
		gm.gameField.printGameField();
	}









}
