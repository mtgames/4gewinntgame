package model.ki;

import java.util.ArrayList;

import supportFiles.Constants;

/**
 * 
 * Die Objekte der Klasse Node bilden die einzelnen Knoten des MinMax-Baums der KI.
 * Dieser Baum hat eine feste Höhe von 7. In der Regel besitzt jeder Knoten einen Grad von 7 
 * (Sind Spalten auf dem Spielfeld bereits vollständig gefüllt, verringert sich der Grad um die
 * Anzahl der belegten Spalten). 
 * 
 * @author MTGames
 * @Mitglieder Timo Strauch, Tim Holm, Meike Helwig, Marcel Schäfer
 * 
 */

public class Node {
	
	/**
	 * Die ArrayList children stellt eine Liste aller Kind-Knoten eines Node-Objektes dar.
	 */
	public ArrayList<Node> children = new ArrayList<Node>();
	
	/**
	 * value hält die von evaluate berechnete Bewertung des Spielfeldes, das zu diesem Knoten gehört. 
	 */
	public int value;
	
	/**
	 * team und throwColumn halten Informationen darüber welcher Spieler in welche Spalte geworfen hat.
	 * Durch Aneinanderreihung dieser Informationen der Knoten lässt sich ein Spielfeld befüllen (siehe dazu auch
	 * die Methode evaluate).
	 */
	public int team; 
	public int throwColumn;
	
	/**
	 * pathToGoNode hält eine Referenz auf den Kind-Knoten, der den zu verfolgenden Spielzug repräsentiert.
	 * Durch Verfolgen aller pathToGoNode-Referenzen lässt sich der für die KI optimale Weg durch den MinMax-Baum
	 * ermitteln.
	 */
	public Node pathToGoNode;
	
	/**
	 * parent hält eine Referenz auf den Vaterknoten.
	 */
	public Node parent;
	
	/**
	 * predictor hält eine Referenz auf das Predictor-Objekt (siehe dazu auch die Klasse Predictor)
	 */
	public Predictor predictor;
	
	/**
	 * is4er gibt an, ob dieser Knoten eine 4er-Spielsteinkette repräsentiert.
	 */
	public boolean is4er = false;
	
	
	/**
	 * 
	 * @param spielzuege
	 * @param nextZugIndex
	 * @return SpielfeldBewertung
	 * 
	 * evaluate wird während der Ermittlung des nächsten Spielzugs auf jeden Knoten des MinMax-Baums aufgerufen.
	 * 
	 * Wenn es sich bei dem Knoten, auf den evaluate aufgerufen wird, um einen Blattknoten handelt, wird
	 * ein Objekt der Klasse Evaluator erzeugt und die Bewertung des durch diesen Knoten repräsentierten
	 * Spielfeldes berechnet und zurückgegeben.
	 * 
	 * Wenn der Knoten <K>, auf den evaluate aufgerufen wird, Kindknoten besitzt, wird der Aufruf von evaluate auf
	 * diese weitergegeben. Diese geben nacheinander die berechneten Spielfeldbewertungen zurück. Hier greift jetzt
	 * die Theorie des MinMax-Baums: Repräsentiert <K> einen Knoten, der einen Spielzug der KI darstellt, wird
	 * das Maximum <max> aus den zurückgegebenen Spielfeldbewertungen der Kind-Knoten gebildet. Anschließend wird
	 * <max> zurückgegeben. Repräsentiert <K> hingegeben einen Knoten, der einen Spielzug des Gegners darstellt, wird
	 * in Analogie das Minimum gebildet und zurückgegeben.
	 */
	public int evaluate(int[][] spielzuege, int nextZugIndex){
		
		int[][] mySpielzuege = new int[spielzuege.length][2];
		for (int i=0; i < spielzuege.length; i++){
			mySpielzuege[i] = spielzuege[i];
		}
	
		if (this.team != -1){//root node
			//meinen Zug an spielzuege anfügen
			int[] z = {this.team, this.throwColumn};
			mySpielzuege[nextZugIndex] = z;
			nextZugIndex++;
		}
		
		if (children.size() == 0 || this.predictor.evalDepth == nextZugIndex){
			
			Evaluator eval = new Evaluator();
			GameField gameField = new GameField(predictor.gameField);
			ThreatManager threatManager = new ThreatManager(predictor.threatManager, gameField);			
			
			//Aufbau des auszuwertenden Spielfeldes durch sukzessives Durchführen der Spielzüge.
			for (int i=0; i < nextZugIndex; i++){
				int[] zug = mySpielzuege[i];
				gameField.throwChip(zug[1], zug[0]);
			}
			
			value = eval.evaluate(gameField, threatManager);
			this.is4er = eval.found4er;
			if(this.is4er){
				this.children.clear();
			}
			
		}
		else{
			int tempVal;
			for (int i=0; i < children.size(); i++){
				tempVal = children.get(i).evaluate(mySpielzuege, nextZugIndex);
				if (children.get(i).team == Constants.ME){
					if (tempVal > value || i==0){
						value = tempVal;
						pathToGoNode = children.get(i);
					}
				}
				else{
					if (tempVal < value || i==0){
						value = tempVal;
						pathToGoNode = children.get(i);
					}
				}
			}
			
		}
		//Wenn ich einen 4er setzen kann, dann soll das auch passieren! Jedoch soll die Priorität
		//4er zu erzeugen in der Spielfeldbewertung nicht erhöht werden.
		if (this.is4er){
			if (parent == predictor.root){
				if (this.team == Constants.ME){
					this.value += 10000;
				}
				else{
					this.value -= 10000;
				}
			}
		}
		
		return value;
		
		
	}
	
	/**
	 * 
	 * @param spielzuege
	 * @param nextZugIndex
	 * Debug-Methode zum Ausgeben sämtlicher Spielfelder aller Knoten.
	 */
	public void printGameFieldHierarchy(int[][] spielzuege, int nextZugIndex){
		if (this.team != -1){//root node
			//meinen Zug an spielzuege anfügen
			int[] z = {this.team, this.throwColumn};
			spielzuege[nextZugIndex] = z;
			nextZugIndex++;
		}

		GameField gameField = new GameField(predictor.gameField);
		@SuppressWarnings("unused")
		ThreatManager threatManager = new ThreatManager(predictor.threatManager, gameField);
		
		for (int i=0; i < nextZugIndex; i++){
			int[] zug = spielzuege[i];
			gameField.throwChip(zug[1], zug[0]);
		}
		
		gameField.printGameField();
		
		for (int i=0; i < children.size(); i++){
			children.get(i).printGameFieldHierarchy(spielzuege, nextZugIndex);
		}
	}
	
	/**
	 * 
	 * @param spielzuege
	 * @param nextZugIndex
	 * Debug-Methode zum Ausgeben der Spielfeldhierarchie, die sich ergibt, wenn man den pathToGo-Knoten folgt.
	 * Diese referenzieren auf jenen Knoten, der den optimalen Folgezug repräsentiert.
	 */
	public void printGameFieldPathToGo(int[][] spielzuege, int nextZugIndex){
		if (this.team != -1){//root node
			//meinen Zug an spielzuege anfügen
			int[] z = {this.team, this.throwColumn};
			spielzuege[nextZugIndex] = z;
			nextZugIndex++;
		}

		GameField gameField = new GameField(predictor.gameField);
		@SuppressWarnings("unused")
		ThreatManager threatManager = new ThreatManager(predictor.threatManager, gameField);
		
		for (int i=0; i < nextZugIndex; i++){
			int[] zug = spielzuege[i];
			gameField.throwChip(zug[1], zug[0]);
		}
		
		System.out.println("Value: "+value);
		gameField.printGameField("Number of Children: "+children.size()+" is 4er? -> "+this.is4er);
		
		if (this.pathToGoNode != null){
			this.pathToGoNode.printGameFieldPathToGo(spielzuege, nextZugIndex);
		}
	}
	
	/**
	 * Debug-Methode: Gibt das Spielfeld des Knotens auf der Konsole uas
	 */
	public void printGameField(){
		//build up the stack of throws
		ArrayList<Node> parents = new ArrayList<Node>();
		ArrayList<Integer[]> spielzuege = new ArrayList<Integer[]>();
		Node p = this;
		while((p = p.parent) != null){
			parents.add(p);
		}
		
		for (int i=parents.size()-1; i >= 0; i--){
			Integer[] t = {parents.get(i).team, parents.get(i).throwColumn};
			spielzuege.add(t);
		}
		

		GameField gameField = new GameField(predictor.gameField);
		@SuppressWarnings("unused")
		ThreatManager threatManager = new ThreatManager(predictor.threatManager, gameField);
		
		
		for (int i=0; i < spielzuege.size(); i++){
			Integer[] zug = spielzuege.get(i);
			gameField.throwChip(zug[1].intValue(), zug[0].intValue());
		}
		
		gameField.printGameField();
		
	}
	
	/**
	 * Debug-Methode: Gibt alle team-Werte für alle Knoten aus.
	 */
	public void printTeam(){
		System.out.println(this.team);
		for (int i=0; i < this.children.size(); i++){
			this.children.get(i).printTeam();
		}
	}
	
	/**
	 * Debug-Methode: Gibt alle team-Werte der Knoten aus, die den optimalen Pfad durch den MinMax-Baum
	 * repräsentieren.
	 */
	public void printTeamPathToGo(){
		System.out.println(this.team);
		if(this.pathToGoNode != null){
			this.pathToGoNode.printTeamPathToGo();
		}
	}
	
	
	/**
	 * 
	 * @param colHeights
	 * extend wird immer dann aufgerufen, wenn der die Höhe des MinMax-Baums um 1 erhöht werden soll.
	 * Dabei erzeugt jeder Blattknoten, der keine 4er Spielsteinkette repräsentiert,
	 * einen neuen Kind-Knoten für jede Spalte, die noch nicht vollständig
	 * gefüllt ist. Jedem Kind-Knoten werden dabei Informationen zu "team" und "throwColumn" gegeben. 
	 */
	public void extend(int[] colHeights){

		int[] myColHeights = new int[7];
		for (int i=0; i < colHeights.length; i++){myColHeights[i] = colHeights[i];}
		if (team != -1){
			myColHeights[throwColumn]++;
		}
		
		if (children.size() > 0){
			for (int i=0; i < children.size(); i++){
				children.get(i).extend(myColHeights);
			}
		}
		else if (!this.is4er){
			for (int i=0; i < 7; i++){
				if (!(myColHeights[i] > 5)){ //bei 5 ist die spalte voll (index)
					
					Node n = createNewNode(i);
					children.add(n);
				}
			}
		}
		
	}
	
	/**
	 * 
	 * @param throwCol
	 * @return
	 * 
	 * Erzeugt einen neuen Knoten während extend ausgeführt wird.
	 */
	private Node createNewNode(int throwCol){
		Node node = new Node();
		
		//set team
		if (team == Constants.ME){
			node.team = Constants.OPPONENT;
		}
		else if (team == Constants.OPPONENT){
			node.team = Constants.ME;
		}
		else{ //Zweig wird nur ausgeführt, wenn der Wurzelknoten createNewNode aufruft.
			node.team = this.predictor.startingTeam;
		}
		
		//set throwCol 
		node.throwColumn = throwCol;
		
		//set Parent
		node.parent = this;
		
		//set predictor
		node.predictor = predictor;
		
		return node;
		
	}
}
