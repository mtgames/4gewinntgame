package model.ki;


//verwaltet die Nodes!

/**
 * 
 * Die Klasse Predictor stellt den Zugriffspunkt auf den MinMax-Baum dar, der zur Ermittlung des nächsten 
 * Spielzugs eingesetzt wird. 
 * 
 * @author MTGames
 * @Mitglieder Timo Strauch, Tim Holm, Meike Helwig, Marcel Schäfer
 * 
 */

public class Predictor {
	/**
	 * root stellt die Referenz auf den Wurzelknoten des MinMax-Baums dar.
	 */
	public Node root;
	
	/**
	 * searchDepth gibt die Höhe des MinMax-Baums an.
	 */
	public int searchDepth;
	
	/**
	 * startingTeam gibt den Startspieler an (Constants.ME oder Constants.OPPONENT)
	 */
	public int startingTeam;
	
	/**
	 * evalDepth gibt an bis zu welchem Niveau der MinMax-Baum evaluiert werden soll.
	 * Wird zur dynamischen Erhöhung der Suchtiefe verwendet, ohne Rechnerzeit durch
	 * Erweiterung des MinMax-Baums zu benötigen.
	 */
	public int evalDepth;
	
	/**
	 * hält eine Referenz auf das aktuelle Spielfeld.
	 */
	public GameField gameField;
	
	/**
	 * hält eine Referenz auf den ThreatManager.
	 */
	public ThreatManager threatManager;
	
	/**
	 * 
	 * @param evalDepth
	 * @param gameField
	 * @param threatManager
	 * @param team
	 * Standardkonstruktor zur Erzeugung eines Objektes der Klasse Predictor.
	 */
	public Predictor(int evalDepth, GameField gameField, ThreatManager threatManager, int team){
		this.evalDepth = evalDepth;
		this.searchDepth = 6;
		this.startingTeam = team;
		this.gameField = gameField;
		this.threatManager = threatManager;
		
		this.buildTree(gameField, threatManager);
	}
	
	/**
	 * 
	 * @return throwColumn
	 * 
	 * getThrowColumn gibt die Spalte zurück, in die als nächstes durch die KI geworfen werden soll.
	 */
	public int getThrowColumn(){
		return this.root.pathToGoNode.throwColumn;
	}
	
	/**
	 * 
	 * @param gameField
	 * @param threatManager
	 * 
	 * buildTree baut den initialen MinMax-Baum auf.
	 */
	private void buildTree(GameField gameField, ThreatManager threatManager){
		root = new Node();
		root.team = -1;
		root.predictor = this;
		
		for (int i=0; i < searchDepth; i++){
			root.extend(this.gameField.colHeights);
		}
	}
	
	/**
	 * evaluate stellt der Einstiegspunkt zum Aufruf von evaluate auf den gesamten MinMax-Baum dar.
	 */
	public void evaluate(){
		int[][] spielzuege = new int[searchDepth+1][2];
		root.evaluate(spielzuege, 0);
	}
	
	/**
	 * Debug-Methode: Gibt die Spielfelder aller Knoten des MinMax-Baums aus.
	 */
	public void printGameFieldHierarchy(){
		int[][] spielzuege = new int[searchDepth+1][2];
		root.printGameFieldHierarchy(spielzuege, 0);
	}
	
	/**
	 * Debug-Methode: Gibt die Spielfelder der Knoten aus, die den optimalen Pfad durch den MinMax-Baum
	 * repräsentieren.
	 */
	public void printGameFieldPathToGo(){
		int[][] spielzuege = new int[searchDepth+1][2];
		root.printGameFieldPathToGo(spielzuege, 0);
	}
	
	/**
	 *
	 * @param throwCol
	 * 
	 * Nachdem der Gegner oder die KI einen zug getätigt hat, wird die root-Referenz auf
	 * den Kind-Knoten gesetzt, der den gewählten Spielzug repräsentiert. Besitzt der root-Knoten
	 * zuvor eine Ordnung von <x> verringert sich dadurch zunächst die Anzahl der Knoten im Baum um <x> 
	 * und die Höhe um 1.
	 * Anschließend wird extend auf den MinMax-Baum aufgerufen, um die Höhe des Baums wiederherzustellen.
	 */
	public void moveDownSearchTree(int throwCol){
		for (int i=0; i < root.children.size(); i++){
			Node child = root.children.get(i);
			if (child.throwColumn == throwCol){
				root = child;
				break;
			}
		}
		
		root.parent = null;
		root.team = -1;
		root.throwColumn = -1;
		
		root.extend(this.gameField.colHeights);
		
	}
}
