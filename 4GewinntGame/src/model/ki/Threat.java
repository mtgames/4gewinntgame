package model.ki;

import supportFiles.Constants;

/**
 * 
 * Die Klasse Threat dient zur Abbildung der potentiellen 4er-Kombinationen, die auf dem Spielfeld möglich sind.
 * Ein Threat hat dabei folgende Klassifikationen:
 * 	1. Zustand: "Solved", "Potential Threat" und "Existing Threat". "Solved" bedeutet, dass mit dem Threat niemand mehr
 * 				gewinnen kann, "Potential Threat" beschreibt Threats, bei denen noch keines der 4 Felder von einem Spielstein
 * 				eines Teams belegt wurde und "Existing Threat" einen Threat, der durch mindestens einen Spielstein eines Teams
 * 				belegt ist und noch zu einem 4er ausgebaut werden kann.
 * 	2. Typ:		"Vertical", "Horizontal" und "Diagonal". Der Typ beschreibt die Ausrichtung des Threats. Diese Information ist 
 * 				bei der Bewertung des Spielfeldes in der Klasse Evaluator von Bedeutung.
 * 	3. Lage:	"Gerade", "Ungerade" und "None". Die Lage ("Direction") gibt an, ob es sich beim Threat um einen geraden
 * 				oder ungeraden Threat handelt. Dabei gelten folgende Zusammenhänge: 
 * 					1. Horizontale Threats in geraden Zeilen sind "Gerade" Threats
 * 					2. Horizontale Threats in ungeraden Zeilen sind "Ungerade" Threats
 * 					3. Vertikale Threats sind weder "Gerade" noch "Ungerade" Threats: "None"
 * 					4. Diagonale Threats bekommen die Lage-Parameter "Gerade"/"Ungerade" erst gesetzt, wenn 3 der 4 Spielfelder,
 * 						die zu einem Threat gehören belegt sind und der Zustand "Existing Threat" ist. Die Zeile des noch
 * 						freien Spielfeldes bestimmt den Lageparameter in Analogie zu 1. und 2.
 * 
 * @author MTGames
 * @Mitglieder Timo Strauch, Tim Holm, Meike Helwig, Marcel Schäfer
 * 
 */
public class Threat {
	public static final int STATE_SOLVED = 0;
	public static final int STATE_POTENTIAL_THREAT = 1;
	public static final int STATE_EXISTING_THREAT = 2;
	
	public static final int TYPE_VERTICAL = 0;
	public static final int TYPE_HORIZONTAL = 1;
	public static final int TYPE_DIAGONAL = 2;
	
	public static final int DIRECTION_GERADE = 0;
	public static final int DIRECTION_UNGERADE = 1;
	public static final int DIRECTION_NONE = -1;
	
	/**
	 * id des Threats
	 */
	public int id;
	
	/**
	 * Zustand des Threats. Siehe Kommentar zur Klasse Threat.
	 */
	public int state = Threat.STATE_POTENTIAL_THREAT;
	
	/**
	 * Gibt an wer mit diesem Threat gewinnen könnte. Constants.ME oder Constants.OPPONENT.
	 */
	public int team = -1; //initial
	
	/**
	 * Typ des Threats. Siehe Kommentar zur Klasse Threat.
	 */
	public int type = -1; //initial
	
	/**
	 * Richtung des Threats. Siehe Kommentar zur Klasse Threat.
	 */
	public int direction = DIRECTION_NONE; //initial
	
	/**
	 * Referenz auf den verwaltenden ThreatManager
	 */
	public ThreatManager threatManager;
	
	/**
	 * position beinhaltet 4 arrays, wobei eines die Position eines Feldes auf dem Spielfeld in der form [spalte, zeile] 
	 * angibt.
	 */
	public int[][] position = new int[4][2];
	
	/**
	 * occupied beinhaltet Positionsarrays von Feldern, die durch einen Spielstein besetzt sind. 
	 */
	public int[][] occupied = new int[4][2];
	
	/**
	 * gibt die Anzahl der belegten Felder an. Kann maximal 4 erreichen.
	 */
	public int countOccupiedFields = 0;
	
	/**
	 * Standardkonstruktor
	 */
	public Threat(){
		
	}
	/**
	 * 
	 * @param t
	 * @param gf
	 * 
	 * Dieser Konstruktor dient zur Erstellung einer Deep-Copy von einem Threat-Objekt.
	 */
	public Threat(Threat t, GameField gf){
		state = t.state;
		team = t.team;
		type = t.type;
		id = t.id;
		direction = t.direction;
		countOccupiedFields = t.countOccupiedFields;
		
		for (int i=0; i < position.length; i++){
			position[i][0] = t.position[i][0];
			position[i][1] = t.position[i][1];
			Field f = gf.gameField[position[i][0]][position[i][1]];
			f.threatsWithField.add(this);
		}
		for (int i=0; i < occupied.length; i++){
			occupied[i][0] = t.occupied[i][0];
			occupied[i][1] = t.occupied[i][1];
		}
	}
	
	/**
	 * 
	 * @param f
	 * 
	 * Wird von einem Field-Objekt aufgerufen, sobald dieses durch einen Spielstein belegt wird. Das Field-Objekt
	 * ruft anschließend "occupyField" auf alle seine assoziierten Threats auf.
	 */
	public void occupyField(Field f){
		if (team == -1){
			team = f.getTeam();
			this.state = Threat.STATE_EXISTING_THREAT;
			if (team == Constants.ME){
				threatManager.myThreats.add(new Integer(id));
			}
			else{
				threatManager.opponentThreats.add(new Integer(id));
			}
			threatManager.potentialThreats.remove(new Integer(id));
		}
		else if (team != f.getTeam()){
			this.state = Threat.STATE_SOLVED; 
			if (team == Constants.ME){
				threatManager.myThreats.remove(new Integer(id));
			}
			else{
				threatManager.opponentThreats.remove(new Integer(id));
			}
		}
		//this.occupied[this.occupied.length-1] = f;
		this.occupied[countOccupiedFields][0] = f.position[0];
		this.occupied[countOccupiedFields][1] = f.position[1];
		countOccupiedFields++;
		
		if (countOccupiedFields == 3 && type == Threat.TYPE_DIAGONAL){
			if ((f.position[1]+1) % 2 == 1){ //ungerade zeile (+1 weil indices gespeichert werden)
				this.direction = Threat.DIRECTION_UNGERADE;
			}
			else{
				this.direction = Threat.DIRECTION_GERADE;
			}
		}
	}
	
	/**
	 * Debug-Methode: Dient zur Ausgabe der Informationen eines Threat-Objektes.
	 */
	public void print(){
		System.out.println("Threat: ");
		for (int i=0; i < position.length; i++){System.out.println("Feld: Spalte: "+position[i][0]+" Reihe: "+position[i][1]);}
	}
	
	
}
