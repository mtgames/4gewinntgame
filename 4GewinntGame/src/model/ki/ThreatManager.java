package model.ki;

import java.util.ArrayList;

/**
 * 
 * Die Klasse ThreatManager dient zur Verwaltung und Erzeugung der 69 potentiellen Threats. 
 * 
 * @author MTGames
 * @Mitglieder Timo Strauch, Tim Holm, Meike Helwig, Marcel Schäfer
 * 
 */

public class ThreatManager {
	/**
	 * threats hält Referenzen auf alle Threat-Objekte
	 */
	public ArrayList<Threat> threats = new ArrayList<Threat>();
	
	/**
	 * myThreats hält Referenzen auf existierende Threats (siehe Klasse Threat) der KI
	 */
	public ArrayList<Integer> myThreats = new ArrayList<Integer>(); //state:EXISTING_THREAT
	
	/**
	 * opponentThreats hält Referenzen auf existierende Threats (siehe Klasse Threat) des Gegners
	 */
	public ArrayList<Integer> opponentThreats = new ArrayList<Integer>(); //state:EXISTING_THREAT
	
	/**
	 * potentialThreats hält potentielle Threats (siehe Klasse Threat)
	 */
	public ArrayList<Integer> potentialThreats = new ArrayList<Integer>();
	
	/**
	 * gameField hält eine Referenz auf das aktuelle Spielfeld-Objekt. (Siehe Klasse GameField)
	 */
	public GameField gameField;
	
	
	
	
	
	/**
	 * 
	 * @param gameField
	 * Standardkonstruktor
	 */
	public ThreatManager(GameField gameField){
		this.gameField = gameField;
		this.initThreatList();
	}
	
	/**
	 * 
	 * @param tm
	 * @param gf
	 * 
	 * Konstruktor, der zur Erzeugung einer Deep-Copy von einem bestehenden ThreatManager-Objekt verwendet wird.
	 */
	public ThreatManager(ThreatManager tm, GameField gf){
		this.gameField = gf;
		
		for (int i=0; i < tm.threats.size(); i++){
			Threat t = new Threat(tm.threats.get(i), this.gameField);
			t.threatManager = this;
			threats.add(t);
		}
		
		for (int i=0; i < tm.myThreats.size(); i++){
			myThreats.add(tm.myThreats.get(i).intValue());
		}

		for (int i=0; i < tm.opponentThreats.size(); i++){
			opponentThreats.add(tm.opponentThreats.get(i).intValue());
		}

		for (int i=0; i < tm.potentialThreats.size(); i++){
			potentialThreats.add(tm.potentialThreats.get(i).intValue());
		}
	}
	
	
	/**
	 * initThreatList erzeugt die 69 Threats für das Spielfeld. Dabei werden zuerst die verikalen, dann die horizontalen
	 * und anschließend die diagonalen Threats erzeugt und die Verbindung mit den assoziierten Field-Objekten hergestellt.
	 */
	public void initThreatList(){
		Field[][] gameField = this.gameField.gameField;
		
		int id = 0;
		//--------------------------vertical threats
		for (int c=0; c < gameField.length; c++){ //gameField.length = 7

			for (int r=0; r < 3; r++){ //drei reihen nach oben: 7*3 = 21 vertikale threats
				Threat t = new Threat();
				t.id = id;
				t.threatManager = this;
				potentialThreats.add(id);
				t.type = Threat.TYPE_VERTICAL;
				int[] cols = {c,c,c,c};
				int[] rows = {r,r+1,r+2,r+3};
				this.initThreatFieldConnection(t, cols, rows, gameField);
				threats.add(t);
				id++;
			}
		}

		//--------------------------horizontal threats
		for (int c=0; c < 4; c++){

			for (int r=0; r < gameField[c].length; r++){ //4 * 6 = 24 horizontale threats
				Threat t = new Threat();
				t.id = id;
				t.threatManager = this;
				potentialThreats.add(id);
				t.type = Threat.TYPE_HORIZONTAL;
				if ((r+1) % 2 == 1){
					t.direction = Threat.DIRECTION_UNGERADE;
				}
				else{
					t.direction = Threat.DIRECTION_GERADE;
				}
				threats.add(t);

				int[] cols = {c,c+1,c+2,c+3};
				int[] rows = {r,r,r,r};
				this.initThreatFieldConnection(t, cols, rows, gameField);
				
				id++;
			}
		}

		//--------------------------upward diagonal threats
		int col = 0;
		int row = 0;
		int[] cols = new int[4];
		int[] rows = new int[4];
		cols[0]=col; cols[1]=col+1;cols[2]=col+2;cols[3]=col+3; 
		rows[0]=row; rows[1]=row+1;rows[2]=row+2;rows[3]=row+3; 

		//1 out of 12
		Threat t = new Threat();
		t.id = id;
		t.threatManager = this;
		potentialThreats.add(id);
		id++;
		threats.add(t);
		t.type = Threat.TYPE_DIAGONAL;
		this.initThreatFieldConnection(t, cols, rows, gameField);

		//2 out of 12
		t = new Threat();
		t.id = id;
		t.threatManager = this;
		potentialThreats.add(id);
		id++;
		threats.add(t);
		t.type = Threat.TYPE_DIAGONAL;
		for (int i=0; i < cols.length; i++){cols[i]++; rows[i]++;}
		this.initThreatFieldConnection(t, cols, rows, gameField);

		//3 out of 12
		t = new Threat();
		t.id = id;
		t.threatManager = this;
		potentialThreats.add(id);
		id++;
		threats.add(t);
		t.type = Threat.TYPE_DIAGONAL;
		for (int i=0; i < cols.length; i++){cols[i]++; rows[i]++;}
		this.initThreatFieldConnection(t, cols, rows, gameField);

		//4 out of 12
		row = 1;
		col = 0;
		cols[0]=col; cols[1]=col+1;cols[2]=col+2;cols[3]=col+3; 
		rows[0]=row; rows[1]=row+1;rows[2]=row+2;rows[3]=row+3; 

		t = new Threat();
		threats.add(t);
		t.threatManager = this;
		t.id = id;
		potentialThreats.add(id);
		id++;
		t.type = Threat.TYPE_DIAGONAL;
		this.initThreatFieldConnection(t, cols, rows, gameField);

		//5 out of 12
		t = new Threat();
		threats.add(t);
		t.threatManager = this;
		potentialThreats.add(id);
		t.id = id;
		id++;
		t.type = Threat.TYPE_DIAGONAL;
		for (int i=0; i < cols.length; i++){cols[i]++; rows[i]++;}
		this.initThreatFieldConnection(t, cols, rows, gameField);

		//6 out of 12
		row = 2;
		col = 0;
		cols[0]=col; cols[1]=col+1;cols[2]=col+2;cols[3]=col+3; 
		rows[0]=row; rows[1]=row+1;rows[2]=row+2;rows[3]=row+3;

		t = new Threat();
		threats.add(t);
		t.threatManager = this;
		potentialThreats.add(id);
		t.id = id;
		id++;
		t.type = Threat.TYPE_DIAGONAL;
		this.initThreatFieldConnection(t, cols, rows, gameField);

		//7 out of 12
		row = 0;
		col = 1;
		cols[0]=col; cols[1]=col+1;cols[2]=col+2;cols[3]=col+3; 
		rows[0]=row; rows[1]=row+1;rows[2]=row+2;rows[3]=row+3;

		t = new Threat();
		threats.add(t);
		t.threatManager = this;
		potentialThreats.add(id);
		t.id = id;
		id++;
		t.type = Threat.TYPE_DIAGONAL;
		this.initThreatFieldConnection(t, cols, rows, gameField);

		//8 out of 12
		t = new Threat();
		threats.add(t);
		t.threatManager = this;
		potentialThreats.add(id);
		t.id = id;
		id++;
		t.type = Threat.TYPE_DIAGONAL;
		for (int i=0; i < cols.length; i++){cols[i]++; rows[i]++;}
		this.initThreatFieldConnection(t, cols, rows, gameField);
		
		//9 out of 12
		t = new Threat();
		threats.add(t);
		t.threatManager = this;
		potentialThreats.add(id);
		t.id = id;
		id++;
		t.type = Threat.TYPE_DIAGONAL;
		for (int i=0; i < cols.length; i++){cols[i]++; rows[i]++;}
		this.initThreatFieldConnection(t, cols, rows, gameField);

		//10 out of 12
		row = 0;
		col = 2;
		cols[0]=col; cols[1]=col+1;cols[2]=col+2;cols[3]=col+3; 
		rows[0]=row; rows[1]=row+1;rows[2]=row+2;rows[3]=row+3;

		t = new Threat();
		threats.add(t);
		t.threatManager = this;
		potentialThreats.add(id);
		t.id = id;
		id++;
		t.type = Threat.TYPE_DIAGONAL;
		this.initThreatFieldConnection(t, cols, rows, gameField);

		//11 out of 12
		t = new Threat();
		threats.add(t);
		t.threatManager = this;
		potentialThreats.add(id);
		t.id = id;
		id++;
		t.type = Threat.TYPE_DIAGONAL;
		for (int i=0; i < cols.length; i++){cols[i]++; rows[i]++;}
		this.initThreatFieldConnection(t, cols, rows, gameField);

		//12 out of 12
		row = 0;
		col = 3;
		cols[0]=col; cols[1]=col+1;cols[2]=col+2;cols[3]=col+3; 
		rows[0]=row; rows[1]=row+1;rows[2]=row+2;rows[3]=row+3;

		t = new Threat();
		threats.add(t);
		t.threatManager = this;
		potentialThreats.add(id);
		t.id = id;
		id++;
		t.type = Threat.TYPE_DIAGONAL;
		this.initThreatFieldConnection(t, cols, rows, gameField);

		//------------------------------downward diagonal 

		//1 out of 12
		row = 3;
		col = 0;
		cols[0]=col; cols[1]=col+1;cols[2]=col+2;cols[3]=col+3; 
		rows[0]=row; rows[1]=row-1;rows[2]=row-2;rows[3]=row-3; 

		t = new Threat();
		threats.add(t);
		t.threatManager = this;
		potentialThreats.add(id);
		t.id = id;
		id++;
		t.type = Threat.TYPE_DIAGONAL;
		this.initThreatFieldConnection(t, cols, rows, gameField);

		//2 out of 12
		row = 4;
		col = 0;
		cols[0]=col; cols[1]=col+1;cols[2]=col+2;cols[3]=col+3; 
		rows[0]=row; rows[1]=row-1;rows[2]=row-2;rows[3]=row-3; 

		t = new Threat();
		threats.add(t);
		t.threatManager = this;
		t.id = id;
		potentialThreats.add(id);
		id++;
		t.type = Threat.TYPE_DIAGONAL;
		this.initThreatFieldConnection(t, cols, rows, gameField);

		//3 out of 12
		t = new Threat();
		threats.add(t);
		t.threatManager = this;
		potentialThreats.add(id);
		t.id = id;
		id++;
		t.type = Threat.TYPE_DIAGONAL;
		for (int i=0; i < cols.length; i++){cols[i]++; rows[i]--;}
		this.initThreatFieldConnection(t, cols, rows, gameField);

		//4 out of 12
		row = 5;
		col = 0;
		cols[0]=col; cols[1]=col+1;cols[2]=col+2;cols[3]=col+3; 
		rows[0]=row; rows[1]=row-1;rows[2]=row-2;rows[3]=row-3; 

		t = new Threat();
		threats.add(t);
		t.threatManager = this;
		potentialThreats.add(id);
		t.id = id;
		id++;
		t.type = Threat.TYPE_DIAGONAL;
		this.initThreatFieldConnection(t, cols, rows, gameField);

		//5 out of 12
		t = new Threat();
		threats.add(t);
		t.threatManager = this;
		potentialThreats.add(id);
		t.id = id;
		id++;
		t.type = Threat.TYPE_DIAGONAL;
		for (int i=0; i < cols.length; i++){cols[i]++; rows[i]--;}
		this.initThreatFieldConnection(t, cols, rows, gameField);

		//6 out of 12
		t = new Threat();
		threats.add(t);
		t.threatManager = this;
		potentialThreats.add(id);
		t.id = id;
		id++;
		t.type = Threat.TYPE_DIAGONAL;
		for (int i=0; i < cols.length; i++){cols[i]++; rows[i]--;}
		this.initThreatFieldConnection(t, cols, rows, gameField);

		//7 out of 12
		row = 5;
		col = 1;
		cols[0]=col; cols[1]=col+1;cols[2]=col+2;cols[3]=col+3; 
		rows[0]=row; rows[1]=row-1;rows[2]=row-2;rows[3]=row-3; 

		t = new Threat();
		threats.add(t);
		t.threatManager = this;
		potentialThreats.add(id);
		t.id = id;
		id++;
		t.type = Threat.TYPE_DIAGONAL;
		this.initThreatFieldConnection(t, cols, rows, gameField);

		//8 out of 12
		t = new Threat();
		threats.add(t);
		t.threatManager = this;
		potentialThreats.add(id);
		t.id = id;
		id++;
		t.type = Threat.TYPE_DIAGONAL;
		for (int i=0; i < cols.length; i++){cols[i]++; rows[i]--;}
		this.initThreatFieldConnection(t, cols, rows, gameField);

		//9 out of 12
		t = new Threat();
		threats.add(t);
		t.threatManager = this;
		potentialThreats.add(id);
		t.id = id;
		id++;
		t.type = Threat.TYPE_DIAGONAL;
		for (int i=0; i < cols.length; i++){cols[i]++; rows[i]--;}
		this.initThreatFieldConnection(t, cols, rows, gameField);

		//10 out of 12
		row = 5;
		col = 2;
		cols[0]=col; cols[1]=col+1;cols[2]=col+2;cols[3]=col+3; 
		rows[0]=row; rows[1]=row-1;rows[2]=row-2;rows[3]=row-3; 

		t = new Threat();
		threats.add(t);
		t.threatManager = this;
		potentialThreats.add(id);
		t.id = id;
		id++;
		t.type = Threat.TYPE_DIAGONAL;
		this.initThreatFieldConnection(t, cols, rows, gameField);

		//11 out of 12
		t = new Threat();
		threats.add(t);
		t.threatManager = this;
		potentialThreats.add(id);
		t.id = id;
		id++;
		t.type = Threat.TYPE_DIAGONAL;
		for (int i=0; i < cols.length; i++){cols[i]++; rows[i]--;}
		this.initThreatFieldConnection(t, cols, rows, gameField);

		//12 out of 12
		row = 5;
		col = 3;
		cols[0]=col; cols[1]=col+1;cols[2]=col+2;cols[3]=col+3; 
		rows[0]=row; rows[1]=row-1;rows[2]=row-2;rows[3]=row-3; 

		t = new Threat();
		threats.add(t);
		t.threatManager = this;
		potentialThreats.add(id);
		t.id = id;
		id++;
		t.type = Threat.TYPE_DIAGONAL;
		this.initThreatFieldConnection(t, cols, rows, gameField);
	}
	
	/**
	 * 
	 * @param t
	 * @param cols
	 * @param rows
	 * @param gameField
	 * 
	 * Die Methode initThreatFieldConnection stellt die gegenseitige Verbindung zwischen Threats und den Feld-Objekten
	 * her, die dem Threat angehören.
	 */
	private void initThreatFieldConnection(Threat t, int[] cols, int[] rows, Field[][] gameField){
		gameField[cols[0]][rows[0]].threatsWithField.add(t);
		gameField[cols[1]][rows[1]].threatsWithField.add(t);
		gameField[cols[2]][rows[2]].threatsWithField.add(t);
		gameField[cols[3]][rows[3]].threatsWithField.add(t);
		
		t.position[0][0] = cols[0]; t.position[0][1] = rows[0];
		t.position[1][0] = cols[1]; t.position[1][1] = rows[1];
		t.position[2][0] = cols[2]; t.position[2][1] = rows[2];
		t.position[3][0] = cols[3]; t.position[3][1] = rows[3];
		
	}
}
