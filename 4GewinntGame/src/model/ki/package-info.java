/**
* Das Paket model.ki enthält alle Klassen, die zur Berechnung eines Spielzugs durch die KI benötigt werden.
* Für eine detaillierte Erklärung des KI-Konzeptes inklusiver der Begrifflichkeiten siehe <a href="http://www.example.com/myfile.pdf#page=4">KI-Konzept</a>. 
* Den Zugriffspunkt auf das Paket bildet die Klasse {@link model.ki.KIGameMaster}, welche das {@link java.lang.Runnable} Interface implementiert 
* und zur Laufzeit die Spiel-Schleife ausführt. Für die interne Repräsentation des Spielfeldes und den einzelnen
* Feldern auf diesem sind die Klassen {@link model.ki.GameField} und {@link model.ki.Field} zuständig. Die Klasse {@link model.ki.ThreatManager} verwaltet die 
* Objekte der Klasse {@link model.ki.Threat}, die die möglichen 4-er Kombinationen aus Spielsteinen auf dem Spielfeld repräsentieren. 
* Die Klasse {@link model.ki.Predictor} verwaltet die interne Baumstruktur, die entsprechend des <a href="http://de.wikipedia.org/wiki/Minimax-Algorithmus">Minimax-Algorithmus</a> den Rahmen für 
* die Berechnung von Spielzügen bietet. Die Baumstruktur besteht aus Knoten, welche durch Objekte der Klasse {@link model.ki.Node}
* abgebildet werden. Die Klasse {@link model.ki.Evaluator} bildet gemeinsam mit der Baumstruktur das Herzstück der KI. 
* Hier werden Spielfelder bewertet, wobei die resultierenden Punktzahlen ausschlaggebend für die Wahl eines 
* Spielzuges durch die KI sind. 
*/
package model.ki;