package model.server;

import java.util.Observable;

import model.ki.KIGameMaster;
import supportFiles.Constants;
import supportFiles.GameplayCommunicator;
import supportFiles.GameplayDataPackage;

/**
 * 
 * Die Klasse ServerCommunicatorMonitor erbt von der abstrakten Klasse GameplayCommunicator und dient als Monitor im Sinne
 * des Thread-Konzeptes während ein Turnier gespielt wird. Sowohl der KIGameMaster-Thread als auch der ServerFileReadWrite-Thread
 * warten in den entsprechenden synchronized Methoden darauf mit Informationen versorgt zu werden und den jeweils anderen
 * aufzuwecken (notify). In dieser Klasse wird Inhalt der XML-Datei, die vom Server bereitgestellt wird ausgewertet und
 * entsprechende Aktionen, wie das Beenden oder Starten eines Spiels eingeleitet.
 * 
 * @author MTGames
 * @Mitglieder Timo Strauch, Tim Holm, Meike Helwig, Marcel Schäfer
 * 
 */

public class ServerCommunicatorMonitor extends GameplayCommunicator{
	
	/**
	 * hält eine Referenz auf das verwendete Objekt der Klasse ServerFileReadWrite zur Kommunikation mit dem Server.
	 */
	public ServerFileReadWrite readWrite;
	
	/**
	 * hält eine Referenz auf das Thread-Objekt, in dem die Runnable readWrite läuft.
	 */
	public Thread readWriteThread;
	
	/**
	 * hält eine Referenz auf das Thread-Objekt, in dem die Runnable des KIGameMaster läuft.
	 */
	public Thread gameMasterThread;
	
	/**
	 * Gibt die vereinbarte Zugzeit in ms an.
	 */
	public int zugZeitInMs;
	
	/**
	 * Main-Methode für Testzwecke.
	 */
//	public static void main(String[] args){
//		ServerCommunicatorMonitor scm = new ServerCommunicatorMonitor();
//		scm.gameMaster = new KIGameMaster(scm);
//		scm.gameMaster.addObserver(scm);
//		
//		scm.start(Constants.SPIELERO, "/Users/Timo/Desktop/", 4000);
//	}
	
	/**
	 * initialisiert den LeseSchreib-Thread und lässt ihn losarbeiten.
	 */
	public void start(int spieler, String pathToDir, int zugZeit){
		this.zugZeitInMs = zugZeit*1000;
		this.readWrite = new ServerFileReadWrite(spieler, pathToDir, this, this.zugZeitInMs);
		this.readWriteThread = new Thread(this.readWrite);
		this.readWriteThread.start();
	}
	
	/**
	 * wird vom LeseSchreib-Thread aufgerufen, nachdem der Server eine XML-Datei zur Verfügung gestellt hat und diese
	 * ausgewertet wurde.
	 */
	public void setXMLContent(XMLContent xml){
		this.interpretateXMLContent(xml);
	}
	
	/**
	 * @param xml
	 * 
	 * interpretiert den Inhalt des XML-Dokumentes, welches bereits vom LeseSchreib-Thread geparst wurde und im Objekt
	 * xml der Klasse XMLContent zur Verfügung steht.
	 */
	private void interpretateXMLContent(XMLContent xml){
		if (xml.freigabe == false){
			//Spiel ist beendet --> Entweder Spielfeld voll oder einer gewinnt
			//unabhängig davon steht in xml.sieger ein sieger drin --> die Berechnung
			//kann jetzt gestoppt werden
			this.endSatz();
			this.setPlayersThrow(xml.gegnerzug);
		}
		else if(xml.gegnerzug == -1){
			this.initGameMasterAndStart(Constants.ME);
		}
		else{
			if (!this.gameMaster.isInitialized){
				this.initGameMasterAndStart(Constants.OPPONENT);
			}
			this.setPlayersThrow(xml.gegnerzug);
		}
	}
	
	/**
	 * 
	 * @param startingPlayer
	 * 
	 * initialisiert das Objekt gameMaster der Klasse KIGameMaster mit den Informationen aus dem XML-Dokument
	 * und startet den KI-Thread.
	 */
	private void initGameMasterAndStart(int startingPlayer){
		if (this.zugZeitInMs <= 1000){
			this.gameMaster.initialize(1, startingPlayer, KIGameMaster.MODE_TOURNAMENT, this.zugZeitInMs);
		}
		else if (this.zugZeitInMs <= 3000){
			this.gameMaster.initialize(2, startingPlayer, KIGameMaster.MODE_TOURNAMENT, this.zugZeitInMs);
		}
		else if (this.zugZeitInMs > 3000){
			this.gameMaster.initialize(4, startingPlayer, KIGameMaster.MODE_TOURNAMENT, this.zugZeitInMs);
		}
//		this.gameMaster.initialize(2, startingPlayer, KIGameMaster.MODE_TOURNAMENT, this.zugZeitInMs);
//		this.gameMaster.initialize(4, startingPlayer, KIGameMaster.MODE_TOURNAMENT, this.zugZeitInMs);
		
		this.gameMasterThread = new Thread(this.gameMaster);
		this.gameMasterThread.start();
	}
	
	/**
	 * beendet den aktuellen Satz. Die Threats besitzen in ihrer run-Methode eine while-Schleife, die im Schleifenkopf
	 * auf den Wert der hier gesetzten Variable prüft.
	 */
	public synchronized void endSatz(){
		this.readWrite.keepOnLooking = false;
		this.gameMaster.gameEnded = true;
	}
	
	/**
	 * Erhält vom GameMaster ein Datenobjekt, dem die errechnete Wurfspalte entnommen werden kann
	 * Als Folge muss der LeseSchreib-Thread geweckt werden, der die Kommunikation mit dem Server realisiert
	 * und die berechnete Spalte aufs Netzlaufwerk schreibt.
	 */
	@Override
	public synchronized void update(Observable o, Object arg) {
		// TODO Auto-generated method stub
		GameplayDataPackage gdp = (GameplayDataPackage) arg;
		if (gdp.context == GameplayDataPackage.CONTEXT_SERVER || gdp.context == GameplayDataPackage.CONTEXT_UI_SERVER){
			if (gdp.contentInfo == GameplayDataPackage.CONTENT_THROWINFO){
				this.KIsThrowColumn = gdp.value;
				notifyAll();
			}
		}
	}
	
	/**
	 * Wird vom LeseSchreib-Thread aufgerufen, um sich die berechnete Wurfspalte der KI
	 * zu holen und die Text-Datei zu erstellen.
	 */
	public synchronized int getKIsThrow(){
//		System.out.println("getKIsThrow - start");
		while(this.KIsThrowColumn == INITIAL && this.readWrite.keepOnLooking){
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		int temp = this.KIsThrowColumn;
		this.KIsThrowColumn = INITIAL;
		
		return temp;
	}
	
	/**
	 * Wird vom GameMaster aufgerufen, um sich den gegnerischen Zug abzuholen.
	 * Falls keine gegnerische Wurfspalte verfügbar ist, wartet der GameMaster-Thread.
	 */
	@Override
	public synchronized int getPlayersThrow() {
		while(this.playersThrowColumn == INITIAL && (!this.gameMaster.gameEnded)){
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		int temp = this.playersThrowColumn;
		this.playersThrowColumn = INITIAL;
		
		return temp;
	}
	
	/**
	 * wird vom aufgerufen, um die gegnerische Wurfspalte zu setzen.
	 */
	@Override
	public synchronized void setPlayersThrow(int throwColumn) {
		// TODO Auto-generated method stub
//		System.out.println("setPlayersThrow");
		this.playersThrowColumn = throwColumn;
		notifyAll();
	}
	
	/**
	 * wird vom ControllerGameField aufgerufen, um den KI-Thread und den ServerFileReadWrite
	 * Thread zu beenden.
	 */
	public void killAllThreads(){
		this.endSatz();
		this.setPlayersThrow(KIGameMaster.SHALL_DIE);
	}

}
