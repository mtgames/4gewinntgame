package model.server;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import supportFiles.Constants;

/**
 * 
 * Die Klasse ServerFileReadWrite implementiert den LeseSchreib-Thread zur Kommunikation mit dem Server.
 * Damit sowohl auf Mac OS als auch auf Windows gespielt werden kann, müssen die Pfade entsprechend angepasst
 * werden. Zu diesem Zweck werden diverse Konstanten definiert.
 * 
 * @author MTGames
 * @Mitglieder Timo Strauch, Tim Holm, Meike Helwig, Marcel Schäfer
 * 
 */

public class ServerFileReadWrite implements Runnable{

	/**
	 * 
	 */
	public static final String WindowsServer2SpielerO = "server2spielero";
	public static final String WindowsServer2SpielerX = "server2spielerx";
	public static final String MacServer2SpielerO = "\\server2spielero";
	public static final String MacServer2SpielerX = "\\server2spielerx";
	public static final String WindowsSpielerO2Server = "spielero2server";
	public static final String WindowsSpielerX2Server = "spielerx2server";
	public static final String MacSpielerO2Server = "\\spielero2server";
	public static final String MacSpielerX2Server = "\\spielerx2server";

	/**
	 * Kontaktpfad für die Dateien, die zur Kommunikation mit dem Server verwendet werden.
	 */
	public String pathToDirectory = "";
	
	/**
	 * sichert den zu verwendenden Pfad für die server2spieler Datei (betriebssystemabhängig)
	 */
	public String server2spieler = ""; 
	
	/**
	 * sichert den zu verwendenden Pfad für die spieler2server Datei (betriebssystemabhängig)
	 */
	public String spieler2server = ""; 
	
	/**
	 * vereinbarte Zugzeit in ms
	 */
	public int zugZeit; 

	/**
	 * boolean Variable, die vor jedem Durchlauf der Arbeitsschleife in der run-Methode geprüft wird.
	 */
	public boolean keepOnLooking = true;
	
	/**
	 * gibt an ob die Pfad für Mac verwendet werden sollen.
	 */
	public boolean useMacPaths = false;
	
	/**
	 * hält eine Referenz auf das ServerCommunicatorMonitor-Objekt, welches den LeseSchreib-Thread instanziert.
	 */
	public ServerCommunicatorMonitor serverComm;
	
	/**
	 * Main-Methode für Testzwecke
	 */
	//	public static void main(String[] args){
	//		ServerCommunicatorMonitor server = new ServerCommunicatorMonitor();
	//		ServerFileReadWrite s = new ServerFileReadWrite(Constants.SPIELERO, "/Users/Timo/Desktop/", server, 4000);
	//		s.writeKIsFile(5);
	////		Thread t = new Thread(s);
	////		t.start();
	//	}

	/**
	 * 
	 * @param spieler: SPIELERO oder SPIELERX
	 * @param pathToDir: Kontaktpfad zu den Spieldateien
	 * @param s: Referenz auf das ServerCommunicatorMonitor Objekt
	 * @param zugZeit: Zeit, die für einen Spielzug verfügbar ist in ms.
	 * 
	 * Konstruktor zur Erzeugung eines Objekts der Klasse ServerFileReadWrite. 
	 */
	public ServerFileReadWrite(int spieler, String pathToDir, ServerCommunicatorMonitor s, int zugZeit) {
		this.serverComm = s;
		this.pathToDirectory = pathToDir;
		this.zugZeit = zugZeit;

		if (System.getProperty("os.name").startsWith("Mac")){
			this.useMacPaths = true;
		}

		if(spieler == Constants.SPIELERO){
			if(this.useMacPaths){
				this.server2spieler = ServerFileReadWrite.MacServer2SpielerO;
				this.spieler2server = ServerFileReadWrite.MacSpielerO2Server;
			}
			else{
				this.server2spieler = ServerFileReadWrite.WindowsServer2SpielerO;
				this.spieler2server = ServerFileReadWrite.WindowsSpielerO2Server;
			}
		}
		else{
			if(this.useMacPaths){
				this.server2spieler = ServerFileReadWrite.MacServer2SpielerX;
				this.spieler2server = ServerFileReadWrite.MacSpielerX2Server;
			}
			else{
				this.server2spieler = ServerFileReadWrite.WindowsServer2SpielerX;
				this.spieler2server = ServerFileReadWrite.WindowsSpielerX2Server;
			}
		}
	}


	/**
	 * die run-Methode wird beim Starten eines Threats mit einem Objekt der Klasse ServerFileReadWrite als Runnable
	 * ausgeführt. Hier werden die Server-Dateien gelesen und Textdateien mit dem berechneten Spielzug geschrieben.
	 */
	@Override
	public void run() {
		
		while(keepOnLooking){
			boolean b = this.evaluateOpponentsFile();

			if(!keepOnLooking){break;}

			if(b){
				this.writeKIsFile();
			}
			else{
				try {
					Thread.sleep(300);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
		
	}

	/**
	 * Schaut im Kontaktverzeichnis nach der gegnerischen XML-Datei.
	 * Wenn sie gefunden wird, wird sie geparst und der gegnerische
	 * Zug an ServerCommunicatorMonitor per setXMLContent mitgeteilt.
	 */
	public boolean evaluateOpponentsFile(){

		try {
			File f = new File(this.pathToDirectory+this.server2spieler+".xml");
			if(!f.exists()){
				return false;
			}

			//get the factory
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			
			//Using factory get an instance of document builder
			DocumentBuilder db = dbf.newDocumentBuilder();

			Document dom = db.parse(f);
			XMLContent c = this.parseDocument(dom);
			this.serverComm.setXMLContent(c);
//			f.delete();
			
			return true;

		}catch(ParserConfigurationException pce) {
			pce.printStackTrace();
			return false;
		}catch(SAXException se) {
			se.printStackTrace();
			return false;
		}catch(FileNotFoundException ioe) {
			//			ioe.printStackTrace();
			return false;
		}catch(IOException ioe) {
			ioe.printStackTrace();
			return false;
		}
	}

	/**
	 * erzeugt eine Textdatei mit der gewählten Spalte und speichert
	 * sie im Kontaktverzeichnis.
	 */
	public void writeKIsFile(){
		int throwCol = this.serverComm.getKIsThrow();

		try {
			FileWriter fw = new FileWriter(this.pathToDirectory+this.spieler2server+".txt");
			fw.write(throwCol+"");
			fw.close();
			File f = new File(this.pathToDirectory+this.server2spieler+".xml");
			f.delete();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/*
	 * --------------------Methoden, die für das Parsen des XML Dokuments verwendet werden
	 */

	/**
	 * 
	 * @param dom
	 * @return XMLContent
	 * 
	 * Erzeugt ein Objekt der Klasse XMLContent mit dem Inhalt des vom Server gelieferten XML-Dokuments.
	 */
	private XMLContent parseDocument(Document dom){
		//get the root element
		Element docEle = dom.getDocumentElement();

		XMLContent c = this.getXMLContent(docEle);
		return c;
	}

	/**
	 * 
	 * @param el
	 * @return XMLContent
	 * 
	 * Setzt die Attribute eines Objekt der Klasse XMLContent mit den Informationen aus dem XML-Dokument.
	 */
	private XMLContent getXMLContent(Element el){
		XMLContent c = new XMLContent();
		c.freigabe = this.getBooleanValue(el, "freigabe");
		c.satzstatus = this.getTextValue(el, "satzstatus");
		c.gegnerzug = this.getIntValue(el, "gegnerzug");
		c.sieger = this.getTextValue(el, "sieger");

		return c;
	}

	/**
	 * 
	 * @param el
	 * @param tagName
	 * @return textVal
	 * 
	 * Wird aufgerufen, wenn aus dem XML-Dokument ein String geparst werden soll.
	 */
	private String getTextValue(Element el, String tagName) {
		String textVal = null;
		NodeList nl = el.getElementsByTagName(tagName);
		if(nl != null && nl.getLength() > 0) {
			Element ele = (Element)nl.item(0);
			textVal = ele.getFirstChild().getNodeValue();
		}

		return textVal;
	}

	/**
	 * 
	 * @param el
	 * @param tagName
	 * @return integerVal
	 * 
	 * Wird aufgerufen, wenn aus dem XML-Dokument ein Integer geparst werden soll.
	 */
	private int getIntValue(Element el, String tagName) {
		//in production application you would catch the exception
		return Integer.parseInt(this.getTextValue(el,tagName));
	}

	/**
	 * 
	 * @param el
	 * @param tagName
	 * @return boolVal
	 * 
	 * Wird aufgerufen, wenn aus dem XML-Dokument ein Boolean geparst werden soll.
	 */
	private boolean getBooleanValue(Element el, String tagName){
		return Boolean.parseBoolean(this.getTextValue(el, tagName));
	}




}
