package model.server;

/**
 * 
 * Die Klasse XMLContent wird zur Abbildung des Inhalts des XML-Dokuments verwendet, welches vom Spielserver bereitgestellt 
 * wird.
 * 
 * @author MTGames
 * @Mitglieder Timo Strauch, Tim Holm, Meike Helwig, Marcel Schäfer
 * 
 */

public class XMLContent {
	
	/**
	 * XML-Datei-Inhalt des Tags "freigabe"
	 */
	public boolean freigabe;
	
	/**
	 * XML-Datei-Inhalt des Tags "satzstatus"
	 */
	public String satzstatus;
	
	/**
	 * XML-Datei-Inhalt des Tags "gegnerzug"
	 */
	public int gegnerzug;
	
	/**
	 * XML-Datei-Inhalt des Tags "sieger"
	 */
	public String sieger;
	
	/**
	 * Ausgabemethode
	 */
	public String toString(){
		String s = "Freigabe: "+freigabe+"\n";
		s += "Satzstatus: "+satzstatus+"\n";
		s += "Gegnerzug: "+gegnerzug+"\n";
		s += "Sieger: "+sieger+"\n";
		
		return s;
	}
}
