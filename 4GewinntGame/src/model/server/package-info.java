/**
* Das Paket model.server beinhaltet die Klassen, die die Kommunikation mit dem durch den Dozenten
* bereitgestellten Server realisieren. Die Klasse {@link model.server.ServerFileReadWrite} implementiert das {@link java.lang.Runnable} Interface 
* und wird zur Laufzeit  von der Klasse {@link model.server.ServerCommunicatorMonitor} als Thread gestartet. 
* Der Thread ist für das Lesen und die Bereitstellung der Dateien zuständig, die für die Kommunikation 
* mit dem Server benötigt werden. Zur besseren Verarbeitung des Inhalts der vom Server bereitgestellten 
* XML-Datei dient die Klasse {@link model.server.XMLContent}. Der {@link model.server.ServerCommunicatorMonitor} wertet den Inhalt des XML-Objektes 
* aus und leitet entsprechende Aktionen ein, wie beispielsweise das Beenden eines Spielsatzes.
*/
package model.server;