/**
 * 
 */
package supportFiles;

/**
 * 
 * Diese Klasse stellt eine zentrale Sammlung aller Konstanten dar, die im gesamten
 * Projekt genutzt werden.
 * 
 * @author MTGames
 * @Mitglieder Timo Strauch, Tim Holm, Meike Helwig, Marcel Schäfer
 * 
 *
 */
public class Constants {
	
	public static final int OPPONENT = 0;
	public static final int ME = 1;
	public static final int INITIAL = -1;
	public static final int SPIELERX = 10;
	public static final int SPIELERO = 20;
	
}
