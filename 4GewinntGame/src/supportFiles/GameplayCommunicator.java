package supportFiles;

import java.util.Observer;

import controller.ControllerGameFieldKIvsPlayer;
import model.ki.KIGameMaster;
import model.server.ServerCommunicatorMonitor;

/**
 * 
 * Die abstrakte Klasse dient zur Vereinheitlichung der Kommunikation zwischen {@link KIGameMaster}
 * und {@link ControllerGameFieldKIvsPlayer} bzw {@link KIGameMaster} und {@link ServerCommunicatorMonitor}. 
 * 
 * Sobald der Spieler eine Spalte gewählt hat, in die er werfen möchte, wird
 * setPlayersThrow(int throwColumn) mit der gewählten Spalte aufgerufen. Darin wird
 * die Instanzvariable playersThrowColumn mit der entsprechenden Spalte gesetzt und 
 * notifyAll() aufgerufen. Als Folge wird der KIGameMaster-Thread, der in getPlayersThrow
 * wartet, aufgeweckt und prüft ob playersThrowColumn != GameplayCommunicator.INITIAL ist.
 * Ist dem so, MUSS playersThrowColumn in einer lokalen variable gesichert werden und anschließend
 * der Wert GameplayCommunicator.INITIAL zugewiesen werden. die lokale Variable wird per return
 * zurückgegeben.
 * Darüber hinaus muss der {@link ControllerGameFieldKIvsPlayer} die Methode update(Observable obj, Object arg) implementieren.
 * Diese wird vom GameMaster aufgerufen, sobald er :
 * 1. eine Spalte berechnet, 
 * 2. Der Gegner eine Spalte gewählt hat (Turnier Modus), 
 * 3. Es einen Gewinner gibt / Das Spielfeld voll ist (Manueller Spieler vs. KI Modus)
 * hat.
 * Der update Methode wird ein Objekt der Klasse {@link GameplayDataPackage} mitgegeben, welches alle relevanten
 * Informationen enthält.
 * 
 * @author MTGames
 * @Mitglieder Timo Strauch, Tim Holm, Meike Helwig, Marcel Schäfer
 * 
 */

public abstract class GameplayCommunicator implements Observer {
	public static int INITIAL = -1;
	
	/**
	 * playersThrowColumn hält den Spaltenindex für den gegnerischen Zug (Gegner der KI)
	 */
	public int playersThrowColumn = GameplayCommunicator.INITIAL; //initial
	
	/**
	 * kIsThrowColumn hält den Spaltenindex für den Zug der KI. 
	 */
	public int KIsThrowColumn = GameplayCommunicator.INITIAL; //initial
	
	/**
	 * gameMaster hält eine Referenz auf das KIGameMaster-Objekt.
	 */
	public KIGameMaster gameMaster;
	
	/**
	 * @return gegnerSpalte
	 * 
	 * Wird von der KI aufgerufen, um den Spaltenindex des gegnerischen Zuges zu erhalten. (playersThrowColumn)
	 */
	public abstract int getPlayersThrow();
	
	/**
	 * @param throwColumn
	 * 
	 * Wird von der Klasse, die die Züge des Gegners der KI setzt aufgerufen, um den Spaltenindex
	 * des gegnerischen Zugs zu setzen. (playersThrowColumn)
	 */
	public abstract void setPlayersThrow(int throwColumn);
	
}
