package supportFiles;

/**
 * 
 * Diese Klasse wird für die Übermittlung von Daten zwischen Model -> Controller und Model -> ServerCommunicator
 * verwendet.
 * 
 * Variable column:
 * Wird das GameplayDataPackage verwendet um eine Spalte, in die geworfen werden soll, mitzuteilen, hält die
 * Variable Column den Spaltenindex. Ansonsten ist column = INITIAL.
 * 
 * Variable msg:
 * Wird nur verwendet, wenn GameMaster dem GamefieldController eine Nachricht in der Form: "Spielfeld voll" oder
 * "Spieler A gewinnt" mitteilen möchte. 
 * 
 * Variable context:
 * Prüfung der context-Variable lässt den Empfänger des Packets entscheiden, ob die enthaltenen Informationen für
 * ihn relevant sind. (Beispiel: Wurf des gegnerischen(Gegner der KI) Spielsteins ist für das UI nur dann von Interesse,
 * wenn es im Turniermodus betrieben wird.)
 * 
 * Variable content:
 * Prüfung der content-Variable lässt den Empfänger ermitteln, um welche Art Inhalt es sich handelt. (der Wert der value-
 * Variable hat entsprechend eine andere Bedeutung)
 * 
 * @author MTGames
 * @Mitglieder Timo Strauch, Tim Holm, Meike Helwig, Marcel Schäfer
 * 
 */

public class GameplayDataPackage {
	public static final int CONTEXT_UI_PLAYER_VS_KI = 10;
	public static final int CONTEXT_UI_TOURNAMENT = 15;
	public static final int CONTEXT_SERVER = 20;
	public static final int CONTEXT_UI_SERVER = 25;
	
	public static final int CONTENT_MSG = 30;
	public static final int CONTENT_THROWINFO = 40;
	public static final int CONTENT_START_PLAYER = 50;
	
	public static final String MSG_KI_GEWINNT = "KI gewinnt";
	public static final String MSG_SPIELER_GEWINNT = "Spieler gewinnt";
	public static final String MSG_FELD_VOLL = "Feld voll";
	
	public static final int INITIAL = -1;
	
	public int value;
	public String msg;
	public int context = INITIAL;
	public int contentInfo = INITIAL;
	
	
	public GameplayDataPackage(int value, int context){
		this.value = value;
		this.msg = "";
		this.context = context;
		this.contentInfo = GameplayDataPackage.CONTENT_THROWINFO;
	}
	
	public GameplayDataPackage(String msg, int context){
		this.value = GameplayDataPackage.INITIAL;
		this.msg = msg;
		this.context = context;
		this.contentInfo = GameplayDataPackage.CONTENT_MSG;
	}
	
	/**
	 * Getter und Setter für einen erleichterten Zugriff auf die Daten
	 */
	public void setThrowColumn(int c){
		this.value = c;
	}
	
	public void setThrowRow(int r){
		this.value = r;
	}
	
	public int getThrowColumn(){
		return this.value;
	}
	
	public int getThrowRow(){
		return this.value;
	}
}
