/**
* Das Paket supportFiles beinhaltet Klassen, die an diversen Stellen innerhalb des Projektes benötigt werden. 
* Dazu zählt die Klasse {@link supportFiles.Constants}, welche einen zentralisierten Zugriff auf bestimmte Kontanten liefert, 
* auf die an diversen Stellen zugegriffen wird. Die Klasse {@link supportFiles.GameplayDataPackage} dient als Datenpaket, 
* welches beispielsweise zwischen Model und Controller versendet wird und die Kommunikation vereinheitlicht. 
* Die abstrakte Klasse {@link supportFiles.GameplayCommunicator} definiert Variablen und Methoden, die von den Klassen 
* implementiert werden müssen, die als Monitor im Sinne das Thread-Konzeptes auftreten. 
* Bei den implementierenden Klassen handelt es sich um {@link model.server.ServerCommunicatorMonitor}, {@link controller.ControllerGameFieldKIvsPlayer}, 
* {@link controller.ControllerGameFieldTournament} und {@link controller.ControllerGameFieldTwoPlayer}. Für eine schematische Darstellung der 
* Kommunikation zwischen Thread und Monitor siehe auch <a href="http://www.tagesschau.de/">Kommunikation Turniermodus</a>. 
*/
package supportFiles;

//<A HREF="http://www.example.com/myfile.pdf#page=4">