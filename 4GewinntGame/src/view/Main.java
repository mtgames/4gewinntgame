/**
 * 
 */
package view;

import controller.MainController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * 
 * Die Klasse ist der Einstiegspunkt des Programms, dabei wird der erste View
 * aufgerufen. Wenn der User das Programm beendet, werden alle Threads beendet.
 * 
 * @author MTGames
 * @Mitglieder Timo Strauch, Tim Holm, Meike Helwig, Marcel Schäfer
 *
 */
public class Main extends Application {

	public static Stage primaryStage;

	/**
	 * Die Methode initialisiert den erste View. Die erste View ist das
	 * Hauptmenü.
	 */
	@Override
	public void start(final Stage primaryStage) {
		try {
			Parent root = FXMLLoader.load(getClass().getResource(
					"/view/Menu.fxml"));
			Scene scene = new Scene(root);

			primaryStage.setScene(scene);
			primaryStage.show();

			Main.primaryStage = primaryStage;
			primaryStage.setResizable(false);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Die Methode wird aufgerufen, wenn der User das Programm beendet. Dabei
	 * werden alle Threads getötet.
	 */
	public void stop() {
		try {
			MainController.deinitializeTournamentClasses();
		} catch (Exception e) {}

		try {
			MainController.deinitializePlayerVsKIClasses();
		} catch (Exception e) {}

	}

	public static void main(String[] args) {
		launch(args);
		
	}
}