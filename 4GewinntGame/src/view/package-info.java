/**
* Das Paket view enthält die entsprechenden User Interface Fenster. Mit Hilfe der fxml-Dateien werden die 
* verschiedenen Fenster darsgestellt.
* 
* In der fxml-Datei {@link view.HistoryGamefield.fxml} findet sich das User Interface der historische Spielanzeige
* von gespeicherten Spielen sowie der Simulation dieser. 
* In der fxml-Datei {@link view.KIGamefield.fxml} findet sich das User Interface für den Spielmodus gegen den Computer.
* In der fxml-Datei {@link view.TournamentGamefield.fxml} findet sich das User Interface für den Turnierspielmodus.
* In der fxml-Datei {@link view.TwoPlayerGamefield.fxml} findet sich das User Interface für den 2-Spieler Modus.
* In der fxml-Datei {@link view.Menu.fxml} findet sich das User Interface für den Einstiegsbildschirm und die Auswahl der Spielart.
* 
* Ebenfalls befindet sich die CSS Datei in diesem Paket. Sie wird zum Style der einzelnen UI Elemente aller fxml Dateien
* verwendet. Die entsprechenden Bilder finden sich in dem Paket {@link images}.
* 
* Die Klasse {@link view.Main} ist die initiale Klasse zum Starten des Programms. Hier wird das UI erstmals aufgebaut.
* Dort wird die fxml-Datei {@link view.Menu.fxml} verlinkt, die den Startbildschirm des Programms erstellt. 
*/

package view;